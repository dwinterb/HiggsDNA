from higgs_dna.selections.lumi_selections import select_lumis
from higgs_dna.utils.dumping_utils import (
    dump_ak_array,
    ditau_ak_array,
    get_object_systematics,
)

from higgs_dna.selections.object_selections import delta_r_match_mask, delta_r_mask, delta_r_seed_mask
from higgs_dna.selections.ditau.lepton_selections import select_muons, select_electrons, select_taus
from higgs_dna.selections.ditau.jet_selections import select_jets, select_jets_eta_dependent, jetvetomap
from higgs_dna.systematics.ditau import object_systematics as available_object_systematics
from higgs_dna.systematics.ditau import object_corrections as available_object_corrections
from higgs_dna.systematics.ditau import weight_systematics as available_weight_systematics
from higgs_dna.systematics.ditau import weight_corrections as available_weight_corrections
from higgs_dna.tools.ditau.add_gen_info import add_gen_information, add_genVertex_information
from higgs_dna.tools.ditau.add_genweights_info import add_lhe_weights, add_tauSpinner_weights
from higgs_dna.utils.misc_utils import identify_dataset
from higgs_dna.utils.add_ditau_mass import add_svfit, add_fastmtt, replace_failed_FastMTT
from higgs_dna.utils.stitching_utils import samples_to_stitch,stitching, stitching_NLO
from higgs_dna.tools.ditau.add_pair_info import add_pair_information
from higgs_dna.tools.ditau.add_decayProduct_info import add_decayProduct_information
from higgs_dna.tools.ditau.add_vertex_info import add_vertex_information
from higgs_dna.tools.ditau.shift_met import tauEnergyScale_shift, jetEnergyResolution_shift
from higgs_dna.tools.ditau.recoil_corrector import apply_RecoilCorrections
from higgs_dna.tools.ditau.add_pair_info import add_jet_information, add_met_information
from higgs_dna.tools.htautau_cp.ip_corrector import apply_IP_Corrections
from higgs_dna.tools.htautau_cp.acoplanarity_producer import acoplanarity_producer
from higgs_dna.tools.htautau_cp.add_ipCalibration_info import add_ipCalibration_information
from higgs_dna.tools.ditau.add_pair_info import add_generic_information
from higgs_dna.tools.ditau.fake_factors import apply_fake_factors

import functools
import operator
import os
from typing import Any, Dict, List, Optional
import awkward
import numpy
import vector
from coffea import processor
from coffea.analysis_tools import Weights
from prettytable import PrettyTable

import logging
import warnings
# Suppress warnings associated with missing branches in the schema (Caused by the skims dropping this information)
warnings.filterwarnings("ignore", message="Missing cross-reference target for Electron_photonIdx => Photon")
warnings.filterwarnings("ignore", message="Missing cross-reference index for Electron_photonIdx => Photon")
warnings.filterwarnings("ignore", message="Missing cross-reference index for Photon_electronIdx => Electron")
warnings.filterwarnings("ignore", message="Missing cross-reference index for Photon_genPartIdx => GenPart")
warnings.filterwarnings("ignore", message="Missing cross-reference index for Photon_jetIdx => Jet")

logger = logging.getLogger(__name__)
vector.register_awkward()


class HtautauBaseProcessor(processor.ProcessorABC):
    def __init__(
        self,
        metaconditions: Dict[str, Any],
        run_effective: bool,
        systematics: Optional[List[Any]],
        corrections: Optional[List[Any]],
        corrections_data: Optional[List[Any]],
        apply_trigger: bool,
        output_location: Optional[str],
        taggers: Optional[List[Any]],
        trigger_group: str,
        analysis: str,
        year: str,
        channel: str,
    ) -> None:
        self.meta = metaconditions
        self.run_effective = run_effective
        self.systematics = systematics if systematics is not None else {}
        self.corrections = corrections if corrections is not None else {}
        self.corrections_data = corrections_data if corrections_data is not None else {}
        self.apply_trigger = apply_trigger
        self.output_location = output_location
        self.trigger_group = trigger_group
        self.analysis = analysis
        self.year = year
        self.channel = channel

        self.postfixes = {"obj_1": "1", "obj_2": "2"}

    def process_extra(self, events: awkward.Array) -> awkward.Array:
        raise NotImplementedError

    def apply_met_filters(self, events: awkward.Array) -> awkward.Array:
        """
        Apply MET filters to the events and return the filtered events.

        Args:
            events (awkward.Array): An Awkward Array containing the events data.

        Returns:
            awkward.Array: The filtered events after applying MET filters.

        This function applies MET filters to the events based on a list of filters specified in the metaconditions.
        Note: The MET filters are applied using the logical AND operation on the individual filter flags.
        """

        logger.debug("Applying MET filters")

        met_filters = self.meta["MetFilters"][self.data_kind]
        filtered = functools.reduce(
            operator.and_,
            (events.Flag[metfilter.split("_")[-1]] for metfilter in met_filters),
        )
        return events[filtered]

    def get_trigger_OR_mask(self, objects : awkward.Array, trig_objects : awkward.Array, selections : str, dR : float) -> awkward.Array:
        """
        Get the trigger OR mask for objects based on a list of trigger selections and delta R matching.

        This function applies a set of trigger selections on the 'trig_objects' and returns a mask that
        represents the logical OR (disjunction) of those selections. It also performs delta R matching
        between the selected trigger objects and 'objects', returning the matched objects.

        Parameters:
            objects (awkward.Array): The array of objects for delta R matching.
            trig_objects (awkward.Array): The array of trigger objects for applying selections.
            selections (str): A string containing trigger selections separated by ':' for logical OR.
                              Individual selections are separated by ','. Supported comparisons are '==', '=', '>=',
                              '>', '<=', and '<'. Bitwise AND operation is supported with '&'.
                              Example: 'id==13,pt>30,filterBits&3'
            dR (float): The delta R threshold for matching objects.

        Returns:
            awkward.Array: An array with boolean values indicating if the objects passed the delta R matching and
                           trigger OR selection. The array contains 'True' for objects that have matching trigger
                           objects according to the specified selections and delta R threshold, and 'False' otherwise.

        Note:
            If no trigger selections are specified (or '1' or 'None' is provided), the function returns an array
            that is 'True' for non-None values (i.e., there is an offline object) and 'False' for None values.
        """

        # if no trigger selections were specified then the object is taken to have passed the selection
        allowed_operations = set(['==', '=', '>=', '>', '<=', '<', '&'])
        selections_check = selections.strip().lower()  # Convert to lowercase and remove leading/trailing spaces
        if selections_check == '1' or selections_check == 'none' or not any(op in selections for op in allowed_operations):
            # return an array that is false for none value and true for non-None values(i.e there is an offline object)
            return ~awkward.is_none(objects)

        masks_for_OR = []
        for sels in selections.split(':'):
            sels_vec = sels.split(',')
            AND_mask = trig_objects.filterBits > 0
            for sel in sels_vec:
                sel_no_spaces = sel.replace(' ','')
                if '==' in sel_no_spaces:
                    AND_mask = AND_mask & (trig_objects[sel_no_spaces.split('==')[0]] == float(sel_no_spaces.split('==')[1]))
                elif '=' in sel_no_spaces:
                    AND_mask = AND_mask & (trig_objects[sel_no_spaces.split('=')[0]] == float(sel_no_spaces.split('=')[1]))
                elif '>=' in sel_no_spaces:
                    AND_mask = AND_mask & (trig_objects[sel_no_spaces.split('>=')[0]] >= float(sel_no_spaces.split('>=')[1]))
                elif '>' in sel_no_spaces:
                    AND_mask = AND_mask & (trig_objects[sel_no_spaces.split('>')[0]] > float(sel_no_spaces.split('>')[1]))
                elif '<=' in sel_no_spaces:
                    AND_mask = AND_mask & (trig_objects[sel_no_spaces.split('<=')[0]] <= float(sel_no_spaces.split('<=')[1]))
                elif '<' in sel_no_spaces:
                    AND_mask = AND_mask & (trig_objects[sel_no_spaces.split('<')[0]] > float(sel_no_spaces.split('<')[1]))
                elif '&' in sel_no_spaces:
                    AND_mask = AND_mask & ((trig_objects[sel_no_spaces.split('&')[0]] & int(sel_no_spaces.split('&')[1])) != 0)
            masks_for_OR.append(AND_mask)

        # Combine all masks with logical OR operation
        mask = awkward.any(masks_for_OR,axis=0)

        # passing trigger objects are selected by requiring them to pass the OR selection AND their is a corresponding offline object
        passing_trig_objects = trig_objects[mask & ~awkward.is_none(objects)]

        # Apply delta R matching and get the matched objects
        matched_objects = passing_trig_objects[delta_r_match_mask(passing_trig_objects, objects, dR)]

        # return an array that is false if there are no matched trigger objects or true if there are matches
        return awkward.num(matched_objects) > 0

    def apply_triggers(self, trig_objects: awkward.Array, pairs: awkward.Array, outname : str, leg1_selections: str = None, leg2_selections : str = None, leg3_selections : str = None, leg4_selections : str = None) -> awkward.Array:
        """
        Apply triggers to candidate pairs in the provided 'pairs' array based on trigger matching criteria.

        Parameters:
            trig_objects (awkward.Array): Awkward Array containing the trigger objects.
            pairs (awkward.Array): Awkward Array containing the candidate pairs.
            outname (str): Name of the output column that will be added to the 'pairs' array to indicate trigger matches.
            leg1_selections (str): Trigger matching criteria for the first leg (obj_1) of candidate pairs.
            leg2_selections (str): Trigger matching criteria for the second leg (obj_2) of candidate pairs.

        Returns:
            awkward.Array: Awkward Array containing the candidate pairs with an additional bool column indicating trigger matches.
        """

        # get trigger matched for first leg (obj_1)
        if leg1_selections:
            leg1_matches = self.get_trigger_OR_mask(pairs['obj_1'],trig_objects,leg1_selections,0.5)
        else:
            leg1_matches = True
        # get trigger matched for second leg (obj_2)
        if leg2_selections:
            leg2_matches = self.get_trigger_OR_mask(pairs['obj_2'],trig_objects,leg2_selections,0.5)
        else:
            leg2_matches = True
        # get trigger matched for third leg (leading jet)
        if leg3_selections:
            leg3_matches = self.get_trigger_OR_mask(pairs['jet_1'],trig_objects,leg3_selections,0.5)
            # default phi and eta of jets that don't exist should be -9999 so dR matching for trigger objects should
            # never pass anyway. But to be extra careful we set the matches to False if the leading jet does not exist (n_jets<1)
            leg3_matches = awkward.where(pairs["n_jets"] >= 1, leg3_matches, False)
        else:
            leg3_matches = True
        # get trigger matched for fourth leg (subleading jet)
        if leg4_selections:
            leg4_matches = self.get_trigger_OR_mask(pairs['jet_2'],trig_objects,leg4_selections,0.5)
            # default phi and eta of jets that don't exist should be -9999 so dR matching for trigger objects should
            # never pass anyway. But to be extra careful we set the matches to False if the subleading jet does not exist (n_jets<2)
            leg4_matches = awkward.where(pairs["n_jets"] >= 2, leg4_matches, False)
        else:
            leg4_matches = True

        # now add a bool as a variable to indicate if the trigger passed or not (i.e both legs pass)
        pairs[outname] = leg1_matches & leg2_matches & leg3_matches & leg4_matches

        return pairs

    def rename_and_drop(self, array: awkward.Array, channel: str, data_kind: str, correction_names: list, individual_trg_weights: list, systematic_name_extensions: list) -> awkward.Array:
        """
        Renames and drops columns in the provided `awkward.Array` based on a predefined mapping.

        Args:
            array (awkward.Array): The input array containing columns to be renamed and dropped.
            channel (str): A string that I defines the ditau channel ('mm','ee','et','mt','em','tt') which determines which quantities are stored

        Returns:
            awkward.Array: The modified array with columns renamed and dropped based on the mapping.

        Example:
            array = ...  # Initialize your awkward.Array
            modified_array = rename_and_drop(array,'tt')

        Note:
            - The provided `array` should be an `awkward.Array`.
            - The function uses the `awkward.zip` function to rename and drop columns based on the mapping.
            - Columns with `None` values in the mapping will retain their original names.
            - Columns not present in the mapping will be dropped from the resulting array.
        """
        map_dict = {}

        metadata = {
            "event": None, "run": None, "lumi": None,
            "original_index_1": None, "original_index_2": None,
        }
        kinematics = {
            "charge_1": None, "charge_2": None, "q_1" : None, "q_2" : None,
            "pt_1": None, "eta_1": None, "phi_1": None, "mass_1": None,
            "pt_2": None, "eta_2": None, "phi_2": None, "mass_2": None,
            "os": None, "dR" : None, "dphi" : None,
            "pt_tt" : None, "pt_vis" : None, "phi_vis": None, "eta_vis": None,
            "mt_1" : None, "mt_2" : None, "mt_lep" : None, "mt_tot" : None,
            "mass" : "m_vis",
        }
        met = {
            "met_pt" : None, "met_phi" : None,
            "met_covXX" : None, "met_covXY" : None, "met_covYY" : None,
            "met_dphi_1" : None, "met_dphi_2" : None,
        }
        triggers = {
            "trg_singlemuon" : None, "trg_singlemuon_2" : None,
            "trg_singleelectron" : None, "trg_singleelectron_2" : None,
            "trg_et_cross": None,
            "trg_mt_cross": None,
            "trg_doubletau" : None, "trg_doubletauandjet": None, "trg_doubletauandjet_2": None,
        }
        tauIDs = {
            # DeepTauv2p5
            "idDeepTau2018v2p5VSjet_1" : None, "idDeepTau2018v2p5VSmu_1" : None, "idDeepTau2018v2p5VSe_1" : None,
            "idDeepTau2018v2p5VSjet_2" : None, "idDeepTau2018v2p5VSmu_2" : None, "idDeepTau2018v2p5VSe_2" : None,
            # DeepTauv2p5 noDA
            "idDeepTau2018v2p5noDAVSjet_1" : None, "idDeepTau2018v2p5noDAVSmu_1" : None, "idDeepTau2018v2p5noDAVSe_1" : None,
            "idDeepTau2018v2p5noDAVSjet_2" : None, "idDeepTau2018v2p5noDAVSmu_2" : None, "idDeepTau2018v2p5noDAVSe_2" : None,
            # DeepTauv2p5 raw scores
            "rawDeepTau2018v2p5VSjet_1" : None, "rawDeepTau2018v2p5VSmu_1" : None, "rawDeepTau2018v2p5VSe_1" : None,
            "rawDeepTau2018v2p5VSjet_2" : None, "rawDeepTau2018v2p5VSmu_2" : None, "rawDeepTau2018v2p5VSe_2" : None,
            # DeepTauv2p5 noDA raw scores
            "rawDeepTau2018v2p5noDAVSjet_1" : None, "rawDeepTau2018v2p5noDAVSmu_1" : None, "rawDeepTau2018v2p5noDAVSe_1" : None,
            "rawDeepTau2018v2p5noDAVSjet_2" : None, "rawDeepTau2018v2p5noDAVSmu_2" : None, "rawDeepTau2018v2p5noDAVSe_2" : None,
            # Pnet raw scores
            "rawPNetVSjet_1": None, "rawPNetVSmu_1": None, "rawPNetVSe_1": None,
            "rawPNetVSjet_2": None, "rawPNetVSmu_2": None, "rawPNetVSe_2": None,
        }
        decayModes = {
            "decayMode_1" : None, "decayMode_2" : None,
            "decayModePNet_1" : None, "decayModePNet_2" : None,
            # Pnet DM probabilities
            "probDM0PNet_1": None,"probDM1PNet_1": None,"probDM2PNet_1": None,"probDM10PNet_1": None,"probDM11PNet_1": None,
            "probDM0PNet_2": None,"probDM1PNet_2": None,"probDM2PNet_2": None,"probDM10PNet_2": None,"probDM11PNet_2": None,
        }
        jets = {
            "n_jets" : None, "n_prebjets" : None, "n_bjets" : None,
            "mjj" : None,
            "jdeta" : None, "sjdphi" : None, "dijetpt" : None,
            "jpt_1" : None, "jeta_1": None, "jphi_1": None,
            "jpt_2" : None, "jeta_2": None, "jphi_2": None,
            # seeding jets
            "seeding_n_jets": None,
            "seeding_mjj": None,
            "seeding_jdeta": None, "seeding_sjdphi": None, "seeding_dijetpt": None,
            "seeding_jpt_1": None, "seeding_jeta_1": None, "seeding_jphi_1": None,
            "seeding_jpt_2": None, "seeding_jeta_2": None, "seeding_jphi_2": None,
        }
        cp_aco = {
            "aco_e_pi": None, "aco_e_rho": None, "aco_e_a1": None, "aco_e_a1_SVFIT": None, "aco_e_a1_FASTMTT_NoMassConstraint": None, "aco_e_a1_FASTMTT_MassConstraint": None,
            "aco_mu_pi": None, "aco_mu_rho": None, "aco_mu_a1": None, "aco_mu_a1_SVFIT": None, "aco_mu_a1_FASTMTT_NoMassConstraint": None, "aco_mu_a1_FASTMTT_MassConstraint": None,
            "aco_pi_pi": None, "aco_pi_rho": None, "aco_pi_a1": None,
            "aco_rho_pi": None, "aco_rho_rho": None, "aco_rho_a1": None,
            "aco_a1_pi": None, "aco_a1_rho": None, "aco_a1_a1": None,
            "aco_pi_a1_SVFIT": None, "aco_rho_a1_SVFIT": None, "aco_a1_pi_SVFIT": None, "aco_a1_rho_SVFIT": None,
            "aco_pi_a1_FASTMTT_NoMassConstraint": None, "aco_rho_a1_FASTMTT_NoMassConstraint": None, "aco_a1_pi_FASTMTT_NoMassConstraint": None, "aco_a1_rho_FASTMTT_NoMassConstraint": None,
            "aco_pi_a1_FASTMTT_MassConstraint": None, "aco_rho_a1_FASTMTT_MassConstraint": None, "aco_a1_pi_FASTMTT_MassConstraint": None, "aco_a1_rho_FASTMTT_MassConstraint": None,
            # alpha angles for validation of CP method
            "alphaAngle_e_pi_1": None, "alphaAngle_e_pi_2": None,
            "alphaAngle_mu_pi_1": None, "alphaAngle_mu_pi_2": None,
        }
        tracking = {
            "PV_x": None, "PV_y": None, "PV_z": None,
            "PVBS_x": None, "PVBS_y": None, "PVBS_z": None,
            "ip_x_1": None, "ip_y_1": None, "ip_z_1": None,
            "ip_x_2": None, "ip_y_2": None, "ip_z_2": None,
            "ip_LengthSig_1": None, "ip_LengthSig_2": None,
            "hasRefitSV_1": None, "hasRefitSV_2": None,
            "sv_x_1": None, "sv_y_1": None, "sv_z_1": None,
            "sv_x_2": None, "sv_y_2": None, "sv_z_2": None,
        }
        decayProducts = {
            "pi_pt_1": None, "pi_eta_1": None, "pi_phi_1": None, "pi_mass_1": None, "pi_charge_1": None, "pi_pdgId_1": None, "pi_Energy_1": None,
            "pi2_pt_1": None, "pi2_eta_1": None, "pi2_phi_1": None, "pi2_mass_1": None, "pi2_charge_1": None, "pi2_pdgId_1": None, "pi2_Energy_1": None,
            "pi3_pt_1": None, "pi3_eta_1": None, "pi3_phi_1": None, "pi3_mass_1": None, "pi3_charge_1": None, "pi3_pdgId_1": None, "pi3_Energy_1": None,
            "pi0_pt_1": None, "pi0_eta_1": None, "pi0_phi_1": None, "pi0_mass_1": None, "pi0_charge_1": None, "pi0_pdgId_1": None, "pi0_Energy_1": None,
            "pi_pt_2": None, "pi_eta_2": None, "pi_phi_2": None, "pi_mass_2": None, "pi_charge_2": None, "pi_pdgId_2": None, "pi_Energy_2": None,
            "pi2_pt_2": None, "pi2_eta_2": None, "pi2_phi_2": None, "pi2_mass_2": None, "pi2_charge_2": None, "pi2_pdgId_2": None, "pi2_Energy_2": None,
            "pi3_pt_2": None, "pi3_eta_2": None, "pi3_phi_2": None, "pi3_mass_2": None, "pi3_charge_2": None, "pi3_pdgId_2": None, "pi3_Energy_2": None,
            "pi0_pt_2": None, "pi0_eta_2": None, "pi0_phi_2": None, "pi0_mass_2": None, "pi0_charge_2": None, "pi0_pdgId_2": None, "pi0_Energy_2": None,
        }
        svfit_fastmtt = {
            "FastMTT_mass": None, "FastMTT_pt": None, "FastMTT_pt_1": None, "FastMTT_pt_2": None,
            "FastMTT_mass_constraint": None, "FastMTT_pt_constraint": None, "FastMTT_pt_1_constraint": None, "FastMTT_pt_2_constraint": None,
            "svfit_Mass": None, "svfit_Mass_err": None, "svfit_tau1_pt": None, "svfit_tau2_pt": None,
        }
        weights = {
            "weight": None, "genWeight": None,
            "LHEReweightingWeight_SM": None, "LHEReweightingWeight_PS": None, "LHEReweightingWeight_MM": None,
            "nLHEjets": None, "npNLOjets": None, 'LHE_Vpt': None,
            "wt_cp_sm": None, "wt_cp_ps": None, "wt_cp_mm": None,
            "w_DoubleTauJetTrigger": None, "w_DoubleTauTrigger": None,
            "w_FakeFactor": None, "w_FakeFactor_cmb": None,
            "w_FakeFactor_up": None, "w_FakeFactor_down": None, "w_FakeFactor_cmb_up": None, "w_FakeFactor_cmb_down": None,
        }
        generic_information = {
            "pion_E_split_1": None, "pion_E_split_2": None,
        }

        mc_specific = {}
        if data_kind == "mc":
            mc_specific = {
                "gen_boson_pT": None, "gen_boson_mass": None, "gen_boson_eta": None, "gen_boson_phi": None,
                "gen_taunus_pT": None, "gen_taunus_phi": None,
                "genPartFlav_1": None, "genPartFlav_2": None,
                "genPart_pt_1": None, "genPart_eta_1": None, "genPart_phi_1": None, "genPart_mass_1": None, "genPart_pdgId_1": None, "genPart_statusFlags_1": None,
                "genPart_pt_2": None, "genPart_eta_2": None, "genPart_phi_2": None, "genPart_mass_2": None, "genPart_pdgId_2": None, "genPart_statusFlags_2": None,
                "genVisTau_pt_1": None, "genVisTau_eta_1": None, "genVisTau_phi_1": None, "genVisTau_mass_1": None,
                "genVisTau_pt_2": None, "genVisTau_eta_2": None, "genVisTau_phi_2": None, "genVisTau_mass_2": None,
                "gen_decayMode_1": None, "gen_decayMode_2": None,
                "genIP_1_x": None, "genIP_1_y": None, "genIP_1_z": None,
                "genIP_2_x": None, "genIP_2_y": None, "genIP_2_z": None,
                'GenVsReco_PVBS_dxy': None, 'GenVsReco_PVBS_dz': None, 'GenVsReco_PV_dxy': None, 'GenVsReco_PV_dz': None,
                'w_DY_soup': None, 'w_WJ_soup': None, 'w_DY_NLO_soup': None,
            }

            mc_specific.update({f"w_{correction}": None for correction in correction_names if correction in available_weight_corrections})
            mc_specific.update({f"w_{individual_trg_weight}": None for individual_trg_weight in individual_trg_weights})
            mc_specific.update({f"w_{systematic_name_extension}": None for systematic_name_extension in systematic_name_extensions})

        map_dict = {**metadata, **kinematics, **met, **triggers, **tauIDs, **decayModes, **jets, **cp_aco, **tracking, **decayProducts, **svfit_fastmtt, **weights, **generic_information, **mc_specific}

        if channel == "ee":
            map_dict["miniPFRelIso_all_1"] = "iso_1"
            map_dict["miniPFRelIso_all_2"] = "iso_2"
        if channel == "mm":
            map_dict["pfRelIso04_all_1"] = "iso_1"
            map_dict["pfRelIso04_all_2"] = "iso_2"
        if channel == "em":
            map_dict["pfRelIso04_all_1"] = "iso_1"
            map_dict["miniPFRelIso_all_2"] = "iso_2"
        if channel in ["et"]:
            map_dict["miniPFRelIso_all_1"] = "iso_1"
        if channel in ["mt"]:
            map_dict["pfRelIso04_all_1"] = "iso_1"

        # Drop columns not in the map and rename columns based on the map
        array_renamed = awkward.zip(
            {new_name if not (new_name == '' or new_name is None) else old_name: array[old_name]
             for old_name, new_name in map_dict.items() if old_name in array.fields}
        )

        return array_renamed

    def process(self, events: awkward.Array) -> Dict[Any, Any]:

        analysis = self.meta.get("Analysis", "Default")
        logger.info(f"Analysis Configuration: {analysis}")

        generic_table = PrettyTable()
        generic_table.field_names = ["Workflow Step", "Number of Events"]
        generic_table.add_row(["Initial", len(events)])

        # here we start recording possible coffea accumulators
        # most likely histograms, could be counters, arrays, ...
        histos_etc = {}

        dataset_name = events.metadata["dataset"]
        logger.info(f"Processing dataset: {dataset_name}")

        # data or monte carlo?
        self.data_kind = "mc" if hasattr(events, "GenPart") else "data"

        if self.data_kind == "data":
            data_era_identifier = identify_dataset(dataset_name, self.year)
            logger.info(f"[{self.year}, {dataset_name}] Data era identifier: {data_era_identifier}")

        # Store effective number of events (before selection)
        if self.data_kind == "mc":
            pos = awkward.count_nonzero(events.genWeight > 0)
            neg = awkward.count_nonzero(events.genWeight < 0)
            n_eff = pos - neg
        else:
            n_eff = len(events)

        if self.run_effective:
            if self.data_kind == "mc":
                # Define the output path and file name
                out_path = f"{self.output_location}/{self.year}/{dataset_name}/"
                if not os.path.exists(out_path):
                    os.makedirs(out_path)

                fname = (
                    events.behavior["__events_factory__"]._partition_key.replace(
                        "/", "_"
                    )
                    + ".parquet"
                )

                run_info_file = f"{out_path}/{fname.split('.')[0]}.txt"

                with open(run_info_file, "w") as f:
                    f.write(f"n_eff = {n_eff}\n")
                    f.write(f"n_pos = {pos}\n")
                    f.write(f"n_neg = {neg}\n")

            return histos_etc

        # metadata array to append to higgsdna output
        metadata = {}

        # lumi mask
        if self.data_kind == "data":
            try:
                lumimask = select_lumis(self.year, events, logger)
                events = events[lumimask]
                logger.info(f"[ lumimask ] Applied lumi mask for {self.year} on {dataset_name}")
            except:
                logger.info(
                    f"[ lumimask ] Skip now! Unable to find year info of {dataset_name}"
                )

        generic_table.add_row(["Lumi Mask", len(events)])

        # apply jetvetomap: only retain events that without any jets in the EE leakage region
        events = jetvetomap(
            events, logger, dataset_name, year=self.year
        )

        generic_table.add_row(["JetVetoMap", len(events)])

        logger.debug(f"Processing channel: {self.channel}")

        # read which systematics and corrections to process
        try:
            correction_names = self.corrections
        except KeyError:
            correction_names = []
        try:
            systematics = self.systematics
        except KeyError:
            systematics = []
        try:
            correction_names_data = self.corrections_data
        except KeyError:
            correction_names_data = []

        electrons_dct = {}
        muons_dct = {}
        taus_dct = {}
        jets_dct = {}
        met_dct = {}

        # apply met filters
        events = self.apply_met_filters(events)
        generic_table.add_row(["MET Filters", len(events)], divider=True)

        # Applying object level corrections to Data
        if self.data_kind == "data":
            for correction_name in correction_names_data:
                if correction_name in available_object_corrections.keys():
                    logger.info(f"Applying correction {correction_name} to dataset {dataset_name}")
                    varying_function = available_object_corrections[correction_name]
                    events = varying_function(events=events,year=self.year, era=data_era_identifier)

        # Applying object level corrections to simulation
        if self.data_kind == "mc":
            for correction_name in correction_names:
                if correction_name in available_object_corrections.keys() and correction_name != "Electron_Scale":
                    logger.info(f"Applying correction {correction_name} to dataset {dataset_name}")
                    varying_function = available_object_corrections[correction_name]
                    events = varying_function(events=events,year=self.year,meta=self.meta,channel=self.channel)

        original_electrons = events.Electron
        original_muons = events.Muon
        original_taus = events.Tau
        original_jets = events.Jet
        original_met = events[self.meta['MET_Type']]

        electrons_dct["nominal"] = original_electrons
        muons_dct["nominal"] = original_muons
        taus_dct["nominal"] = original_taus
        met_dct["nominal"] = original_met

        # Applying systematic variations
        variations_combined = []
        if self.data_kind == "mc":
            for systematic_name in systematics:
                if systematic_name in available_object_systematics.keys():
                    systematic_dct = available_object_systematics[systematic_name]
                    if systematic_dct["object"] == "Tau" or systematic_dct["object"] == "Electron":
                        logger.info(f"Applying systematic {systematic_name} to collections")
                        if not systematic_dct["args"]["coffea"]:
                            varying_function = systematic_dct["args"]["varying_function"]
                            if systematic_dct["object"] == "Tau":
                                taus_variation_dct = varying_function(events=events,year=self.year, meta=self.meta, channel=self.channel)
                                variations_added = []
                                for key in taus_variation_dct:
                                    taus_dct[systematic_name + '_' + key] = taus_variation_dct[key]
                                    variation_to_add = systematic_name + '_' + key.removesuffix("_down").removesuffix("_up")
                                    if variation_to_add not in variations_added:
                                        variations_added.append(variation_to_add)
                                        variations_combined.append([variation_to_add])
                            elif systematic_dct["object"] == "Electron":
                                electrons_up,electrons_down = varying_function(events=events,year=self.year, meta=self.meta)
                                electrons_dct[systematic_name + "_down"] = electrons_down
                                electrons_dct[systematic_name + "_up"] = electrons_up
                                variations_combined.append([systematic_name])

        jerc_syst_list, jets_dct = get_object_systematics(original_jets, ["pt", "mass"])

        variations_combined.append(original_electrons.systematics.fields)
        variations_combined.append(jerc_syst_list)
        variations_flattened = sum(variations_combined, [])
        variations = [item + suffix for item in variations_flattened for suffix in ['_down', '_up']]
        variations.append('nominal')
        logger.info(f"[systematics variations] {variations}")

        for variation in variations:
            logger.info("-" * 50)
            logger.info(f"Variation: {variation}")
            logger.info("-" * 50)

            electrons,muons,taus = electrons_dct["nominal"],muons_dct["nominal"],taus_dct["nominal"]
            met_, jets_ = met_dct["nominal"], jets_dct["nominal"]
            if variation == "nominal":
                pass
            elif variation in [*electrons_dct]:
                electrons = electrons_dct[variation]
            elif variation in [*muons_dct]:
                muons = muons_dct[variation]
            elif variation in [*taus_dct]:
                taus = taus_dct[variation]
            elif variation in [*jets_dct]:
                jets_ = jets_dct[variation]
            elif variation in [*met_dct]:
                met_ = met_dct[variation]

            electrons = awkward.with_field(electrons, awkward.local_index(electrons), 'original_index')
            muons = awkward.with_field(muons, awkward.local_index(muons), 'original_index')
            taus = awkward.with_field(taus, awkward.local_index(taus), 'original_index')

            # object preselection
            # first we select muons using loose pt and isolation cuts so that they can be used in all channels and eventually as veto objects
            logger.debug("Applying muon preselection")
            muons = select_muons(muons, 10., 2.4)

            # then we do the same for electrons
            logger.debug("Applying electron preselection")
            electrons = select_electrons(electrons, 10., 2.5, self.meta['Ele_ID'])
            # and finally for taus
            logger.debug("Applying tau preselection")
            taus = select_taus(taus, 18., 2.3, self.meta['Tau_ID'])

            # we sort the objects here to ensure the pairs will later be ordered preferring the most isolated particles, if objects have the same isolation then the highest pT one is preferred.
            # To ensure the correct sorting we first sort based on the least important quantity (pT), then after this we sort by the isolation

            # first sort all objects in each event descending in pt
            muons = muons[awkward.argsort(muons.pt, ascending=False)]
            electrons = electrons[awkward.argsort(electrons.pt, ascending=False)]
            taus = taus[awkward.argsort(taus.pt, ascending=False)]

            # then sort all electrons and muon by isolation (smallest isolation first)
            muons = muons[awkward.argsort(muons.pfRelIso04_all, ascending=True)]
            electrons = electrons[awkward.argsort(electrons.miniPFRelIso_all, ascending=True)]

            # also sort taus based on the RAW ID score for vs jet ID, in this case high scores correspond to 'more isolated'
            taus = taus[awkward.argsort(taus[f"raw{self.meta['Tau_ID']}VSjet"], ascending=False)]

            # process ditau pairs for the specified channel
            ch = self.channel

            pairs = {}
            print("")
            logger.info(f"Producing ditau pairs for {ch}")
            # first we make same-type pairs
            if ch == "mm":
                mm_pairs = awkward.combinations(muons, 2, fields=["obj_1", "obj_2"])
                pairs['mm'] = mm_pairs
            elif ch == 'ee':
                ee_pairs = awkward.combinations(electrons, 2, fields=["obj_1", "obj_2"])
                pairs['ee'] = ee_pairs
            elif ch == 'tt':
                tt_pairs = awkward.combinations(taus, 2, fields=["obj_1", "obj_2"])
                pairs['tt'] = tt_pairs

            # As preference was given to isolated particles during the sorting, for same-type pairs we need to make sure that obj_1 is always the one with the highest pT:
            if ch in ['ee','mm','tt']:
                # Check if obj_1 has greater pt than obj_2 in each pair
                mask = pairs[ch]["obj_1"].pt > pairs[ch]["obj_2"].pt

                # Switch obj_1 and obj_2 if necessary
                switched_pairs = awkward.with_field(
                    pairs[ch],
                    awkward.where(mask, pairs[ch]["obj_1"], pairs[ch]["obj_2"]),
                    "obj_1"
                )
                switched_pairs = awkward.with_field(
                    switched_pairs,
                    awkward.where(mask, pairs[ch]["obj_2"], pairs[ch]["obj_1"]),
                    "obj_2"
                )
                pairs[ch] = switched_pairs

            # combinations of mixed pairs we follow the usual HTT conventions and lighter leptons are preferred for obj_1
            # Note when using the cartesian function the pairs will be ordered preferentially by the objects passed as the first argument
            # We want to order the pairs prefering muons > electrons > taus so we must pass the arguments in the same order below

            elif ch == "em":
                em_pairs = awkward.cartesian({"muons": muons,"electrons": electrons})
                em_pairs = awkward.zip({"obj_1": em_pairs["electrons"], "obj_2":em_pairs["muons"]})
                pairs['em'] = em_pairs

            elif ch == "et":
                et_pairs = awkward.cartesian({"electrons": electrons, "taus": taus})
                et_pairs = awkward.zip({"obj_1": et_pairs["electrons"], "obj_2":et_pairs["taus"]})
                pairs['et'] = et_pairs

            elif ch == "mt":
                mt_pairs = awkward.cartesian({"muons": muons, "taus": taus})
                mt_pairs = awkward.zip({"obj_1": mt_pairs["muons"], "obj_2":mt_pairs["taus"]})
                pairs['mt'] = mt_pairs

            p = pairs[ch]
            n_pairs = awkward.count_nonzero(awkward.num(p) > 0)
            if variation == "nominal":
                generic_table.add_row([f"Pairs [{ch}]", n_pairs])

            logger.debug("Applying channel specific selections")

            # dR cut
            dR_cut = p["obj_1"].delta_r(p["obj_2"]) > 0.5

            # pT and eta cuts
            pt_cut = (p["obj_1"].pt > self.meta['Selections'][ch]['pt_1']) & (p["obj_2"].pt > self.meta['Selections'][ch]['pt_2'])
            eta_cut = (abs(p["obj_1"].eta) < abs(self.meta['Selections'][ch]['eta_1'])) & (abs(p["obj_2"].eta) < abs(self.meta['Selections'][ch]['eta_2']))

            # extra lepton vetos
            lepton_veto_cut = ((awkward.num(electrons, axis=1) <= self.meta['Selections'][ch]['max_n_electrons'])) & ((awkward.num(muons, axis=1) <= self.meta['Selections'][ch]['max_n_muons']))

            all_cuts = dR_cut & pt_cut & eta_cut & lepton_veto_cut

            # apply masks to remove pairs that fail any of the cuts
            p = p[all_cuts]

            # finally we keep only the first pair (which is the highest ranked one)
            p = awkward.firsts(p)

            logger.debug(f"Dropping events without a selected {ch} pair")
            # drop events without a preselected pair
            selection_mask = ~awkward.is_none(p)
            pair = p[selection_mask]

            if variation == "nominal":
                generic_table.add_row([f"Events passing dR cut [{ch}]", awkward.sum(awkward.any(dR_cut, axis=1))])
                generic_table.add_row([f"Events passing pT cut [{ch}]", awkward.sum(awkward.any(pt_cut, axis=1))])
                generic_table.add_row([f"Events passing eta cut [{ch}]", awkward.sum(awkward.any(eta_cut, axis=1))])
                generic_table.add_row([f"Events passing lepton veto [{ch}]", n_pairs - awkward.sum(awkward.any(dR_cut & pt_cut & eta_cut, axis=1))])
                generic_table.add_row([f"Events with valid pairs [{ch}]", awkward.count_nonzero(~awkward.is_none(pair['obj_1']) & ~awkward.is_none(pair['obj_2']))])
            pair["event"] = events.event[selection_mask]
            pair["run"] = events.run[selection_mask]
            pair["lumi"] = events.luminosityBlock[selection_mask]
            pair, use_svfit = add_svfit(pair, self.year, ch, dataset_name, variation)
            pair, use_fastmtt_NoMassConstraint = add_fastmtt(pair, self.year, ch, dataset_name, variation, mass_constraint=False)
            pair, use_fastmtt_MassConstraint = add_fastmtt(pair, self.year, ch, dataset_name, variation, mass_constraint=True)
            pair = replace_failed_FastMTT(pair, use_fastmtt_NoMassConstraint and use_fastmtt_MassConstraint)
            pair["weight"] = numpy.ones(len(pair))

            if self.data_kind == "mc" and len(pair) > 0:
                pair['weight'] = pair['weight'] * numpy.sign(events.genWeight[selection_mask])
                pair = add_gen_information(pair, events[selection_mask], ch)
                pair = add_genVertex_information(pair, events[selection_mask])
                pair = add_lhe_weights(pair, events[selection_mask], dataset_name)
                pair = add_tauSpinner_weights(pair, events[selection_mask])
                logger.debug("Applying stitching weights")
                if self.year in ["Run3_2022", "Run3_2022EE", "Run3_2023", "Run3_2023BPix"]:
                    pair["w_DY_soup"] = numpy.ones(len(pair))
                    pair["w_WJ_soup"] = numpy.ones(len(pair))
                    pair["w_DY_NLO_soup"] = numpy.ones(len(pair))
                    do_stitch, process_id = samples_to_stitch(dataset_name)
                    if do_stitch:
                        if process_id == "DY" or process_id == "WJ":
                            pair[f"w_{process_id}_soup"] = stitching(self.year,events[selection_mask],process_id)
                        elif process_id == "DY_NLO":
                            pair[f"w_{process_id}_soup"] = stitching_NLO(self.year,events[selection_mask],process_id)
                        pair["weight"] = pair["weight"] * pair[f"w_{process_id}_soup"]
            else:
                logger.debug("Skipping the addition of gen info to pairs as there are no surviving pairs")

            # Add pair quantities e.g visible-mass, pT etc
            pair = add_pair_information(pair)
            pair = add_vertex_information(pair, events[selection_mask])
            pair = add_decayProduct_information(pair, events[selection_mask], ch)

            if self.data_kind == "mc" and len(pair) > 0 and analysis == "CP":
                pair = add_ipCalibration_information(pair, events[selection_mask])
                pair = apply_IP_Corrections(pair, ch, self.year)

            if analysis == "CP":
                pair = acoplanarity_producer(pair, ch, use_svfit, use_fastmtt_NoMassConstraint, use_fastmtt_MassConstraint)

            # Now we pick up the MET and jets, and we apply the same mask that we used to select only our good pairs
            met = met_[selection_mask]
            if "Tau_EnergyScale" in correction_names and "Tau_EnergyScale" in available_object_corrections and self.data_kind == "mc":
                met = tauEnergyScale_shift(ch, met, pair)

            jets = jets_[selection_mask]
            if "MET_Recoil" not in correction_names and any("Jet_Energy" in s for s in correction_names):
                met = jetEnergyResolution_shift(jets, met, variation)

            # apply eta dependent pT cut for 2022 and 2023 data
            if self.year in ["Run3_2022", "Run3_2022EE", "Run3_2023", "Run3_2023BPix"]:
                selected_jets = select_jets_eta_dependent(jets,[20.,50.,30.],[2.5,3.0,4.7], "v14")
            else:
                selected_jets = select_jets(jets,30.,4.7, "v14")

            # select b-jet candidates that pass pT and eta cuts for bjets
            selected_prebjets = select_jets(jets,self.meta['BTAG_ID']['pt'],self.meta['BTAG_ID']['eta'], "v14")
            # require b-jet candidates to pass cut on btagging discriminator
            selected_bjets = selected_prebjets[selected_prebjets[self.meta['BTAG_ID']['name']] > self.meta['BTAG_ID']['cut']]

            jets = selected_jets
            prebjets = selected_prebjets
            bjets = selected_bjets

            # Add jet quantities here
            # First we filter the jets that are matched in dR to our selected tau candidates
            dr_jet_obj1_cut = delta_r_mask(jets, pair['obj_1'], 0.5)
            dr_jet_obj2_cut = delta_r_mask(jets, pair['obj_2'], 0.5)
            filtered_jets = jets[dr_jet_obj1_cut & dr_jet_obj2_cut]
            # And then we compute and add our jet quantities
            pair = add_jet_information(pair,filtered_jets,"")

            # Filter jets seeding the taus
            dr_jet_obj1_seed = delta_r_seed_mask(jets, pair['obj_1'], 0.5)
            dr_jet_obj2_seed = delta_r_seed_mask(jets, pair['obj_2'], 0.5)
            filtered_jets = jets[dr_jet_obj1_seed & dr_jet_obj2_seed]
            pair = add_jet_information(pair,filtered_jets, "seeding_")

            # also add number of (pre) b-tagged jets
            # as for other jets we filter the (pre) bjets that are matched in dR to our selected tau candidates
            dr_bjet_obj1_cut = delta_r_mask(prebjets, pair['obj_1'], 0.5)
            dr_bjet_obj2_cut = delta_r_mask(prebjets, pair['obj_2'], 0.5)
            filtered_prebjets = prebjets[dr_bjet_obj1_cut & dr_bjet_obj2_cut]
            # for now we don't use a dedicated function for bjet quantities as we only store n_bjets but this
            # could be added if we eventually want to store more information
            pair["n_prebjets"] = awkward.num(filtered_prebjets)

            # also add number of b-tagged jets
            # as for other jets we filter the bjets that are matched in dR to our selected tau candidates
            dr_bjet_obj1_cut = delta_r_mask(bjets, pair['obj_1'], 0.5)
            dr_bjet_obj2_cut = delta_r_mask(bjets, pair['obj_2'], 0.5)
            filtered_bjets = bjets[dr_bjet_obj1_cut & dr_bjet_obj2_cut]
            # for now we don't use a dedicated function for bjet quantities as we only store n_bjets but this
            # could be added if we eventually want to store more information
            pair["n_bjets"] = awkward.num(filtered_bjets)

            # add number of jets to be used for recoil corrections (simpler selection)
            selected_jets_recoil = select_jets_eta_dependent(jets,[30.,50.],[2.5,4.7], "v14")
            dr_jet_obj1_seed_recoil = delta_r_seed_mask(selected_jets_recoil, pair['obj_1'], 0.5)
            dr_jet_obj2_seed_recoil = delta_r_seed_mask(selected_jets_recoil, pair['obj_2'], 0.5)
            filtered_jets_recoil = selected_jets_recoil[dr_jet_obj1_seed_recoil & dr_jet_obj2_seed_recoil]
            pair["n_jets_recoil"] = awkward.num(filtered_jets_recoil)

            if "MET_Recoil" in correction_names:
                met = apply_RecoilCorrections(met, pair, self.year, dataset_name)
            # Add MET quantities e.g MET, transverse masses etc
            pair = add_met_information(pair,met)

            pair = add_generic_information(pair)

            # Apply Fake Factors
            if ch == 'tt':
                # Fake factors for individual eras
                pair['w_FakeFactor'] = apply_fake_factors(pair, self.year, 'nominal', combined=False)
                pair['w_FakeFactor_up'] = apply_fake_factors(pair, self.year, 'up', combined=False)
                pair['w_FakeFactor_down'] = apply_fake_factors(pair, self.year, 'down', combined=False)
                # Combined fake factors
                pair['w_FakeFactor_cmb'] = apply_fake_factors(pair, self.year, 'nominal', combined=True)
                pair['w_FakeFactor_cmb_up'] = apply_fake_factors(pair, self.year, 'up', combined=True)
                pair['w_FakeFactor_cmb_down'] = apply_fake_factors(pair, self.year, 'down', combined=True)

            # Get trigger objects
            trig_objects = events.TrigObj[selection_mask]
            # Apply trigger matching which adds booleons to the array indicating whether the pair legs were matched to particular trigger objects
            if 'Triggers' in self.meta and ch in self.meta['Triggers']:
                logger.debug("Determining trigger decisions")
                triggers = self.meta['Triggers'][ch]
                # loop over all specified triggers and store a booleon for each one indicating which events passed the trigger
                for trig_name, trig_sels in triggers.items():
                    leg1_selections = trig_sels['leg1'] if 'leg1' in trig_sels else None
                    leg2_selections = trig_sels['leg2'] if 'leg2' in trig_sels else None
                    leg3_selections = trig_sels['leg3'] if 'leg3' in trig_sels else None
                    leg4_selections = trig_sels['leg4'] if 'leg4' in trig_sels else None
                    pair = self.apply_triggers(trig_objects, pair, trig_name, leg1_selections, leg2_selections, leg3_selections, leg4_selections)
                    if variation == "nominal":
                        generic_table.add_row([f"{trig_name} [{ch}]", f"{awkward.sum(pair[trig_name])} -- {len(pair)}"])
            else:
                logger.debug("Not requiring any triggers as none were specified in the metaconditions")

            # workflow specific processing
            events, process_extra = self.process_extra(events)
            histos_etc.update(process_extra)

            if variation == "nominal":
                generic_table.add_row(["", ""], divider=True)

            # continue if there is no surviving events
            if len(pair) == 0:
                logger.debug(f"No surviving {ch} channel events in this run, no output will be written!")
                continue

            individual_trg_weights = []
            systematic_name_extensions = []
            if self.data_kind == "mc":
                for correction_name in correction_names:
                    if correction_name in available_weight_corrections:
                        if correction_name == "Trigger":
                            event_weights = Weights(size=len(events[selection_mask]), storeIndividual=True)
                        else:
                            event_weights = Weights(size=len(events[selection_mask]))
                        logger.info(
                            f"Adding correction {correction_name} to weight collection of dataset {dataset_name}"
                        )
                        varying_function = available_weight_corrections[correction_name]
                        event_weights = varying_function(
                            events=events[selection_mask],
                            weights=event_weights,
                            pair=pair,
                            channel=ch,
                            dataset_name=dataset_name,
                            year=self.year,
                            meta=self.meta,
                        )
                        if correction_name == "Trigger":
                            pair["w_" + correction_name] = event_weights.partial_weight(include=["Trigger"])
                            pair["weight"] = pair["weight"] * pair["w_" + correction_name]

                            # being a bit sneaky here, using the systematics variation names to get the trigger weights
                            for modifier in event_weights.variations:
                                trg_weight_name = modifier.split("Up")[0].split("Down")[0]
                                if trg_weight_name not in individual_trg_weights:
                                    individual_trg_weights.append(trg_weight_name)
                                    pair[f"w_{trg_weight_name}"] = event_weights.partial_weight(include=[trg_weight_name])
                        else:
                            pair["w_" + correction_name] = event_weights.weight()
                            pair["weight"] = pair["weight"] * event_weights.weight()

                # systematic variations of event weights go to nominal output dataframe:
                if variation == "nominal":
                    for systematic_name in systematics:
                        if systematic_name in available_weight_systematics:
                            logger.info(
                                f"Adding systematic {systematic_name} to weight collection of dataset {dataset_name}"
                            )
                            event_weights = Weights(size=len(events[selection_mask]))
                            varying_function = available_weight_systematics[
                                systematic_name
                            ]
                            event_weights = varying_function(
                                events=events[selection_mask],
                                weights=event_weights,
                                pair=pair,
                                channel=ch,
                                dataset_name=dataset_name,
                                year=self.year,
                                meta=self.meta,
                            )
                            for modifier in event_weights.variations:
                                if systematic_name in ["Tau_ID", "Tau_e_FakeRate", "Tau_mu_FakeRate"]:
                                    pair[f"w_{modifier}"] = event_weights.weight(modifier)
                                    systematic_name_extensions.append(modifier)
                                elif systematic_name == "Trigger":
                                    if modifier in ["TriggerUp", "TriggerDown"]:
                                        pair[f"w_{systematic_name}_{modifier.split(systematic_name)[1]}"] = event_weights.weight(modifier)
                                        systematic_name_extensions.append(systematic_name + "_" + modifier.split(systematic_name)[1])
                                else:
                                    pair[f"w_{systematic_name}_{modifier.split(systematic_name)[1]}"] = event_weights.weight(modifier)
                                    systematic_name_extensions.append(systematic_name + "_" + modifier.split(systematic_name)[1])

            if self.output_location is not None:
                akarr = ditau_ak_array(self, pair)
                akarr = self.rename_and_drop(akarr, ch, self.data_kind, correction_names, individual_trg_weights, systematic_name_extensions)
                fname = (
                    events.behavior["__events_factory__"]._partition_key.replace(
                        "/", "_"
                    )
                    + ".parquet"
                )
                subdirs = [ch]
                if "dataset" in events.metadata:
                    subdirs.append(events.metadata["dataset"])
                subdirs.append(variation)
                dump_ak_array(self, akarr, fname, self.output_location + "/" + self.year, metadata, subdirs)

        logger.info(generic_table)

        return histos_etc

    def postprocess(self, accumulant: Dict[Any, Any]) -> Any:
        raise NotImplementedError


class DiTauProcessor(HtautauBaseProcessor):
    def __init__(
        self,
        metaconditions: Dict[str, Any],
        run_effective: bool = False,
        systematics: Optional[List[Any]] = None,
        corrections: Optional[List[Any]] = None,
        corrections_data: Optional[List[Any]] = None,
        apply_trigger: bool = False,
        output_location: Optional[str] = None,
        taggers: Optional[List[Any]] = None,
        year: str = '',
        channel: str = '',
        analysis: str = '',
    ) -> None:
        super().__init__(
            metaconditions,
            run_effective=run_effective,
            systematics=systematics,
            corrections=corrections,
            corrections_data=corrections_data,
            apply_trigger=apply_trigger,
            output_location=output_location,
            taggers=taggers,
            year=year,
            channel=channel,
            trigger_group=".*SingleMu.*",
            analysis="mainAnalysis",
        )
        self.trigger_group = ".*SingleMu.*"
        self.analysis = "mainAnalysis"

    def process_extra(self, events: awkward.Array) -> awkward.Array:
        return events, {}

    def postprocess(self, accumulant: Dict[Any, Any]) -> Any:
        pass
