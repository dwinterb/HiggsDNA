import os
import awkward
import pandas as pd
import logging
import numpy as np

logger = logging.getLogger(__name__)


def add_svfit(pair: awkward.Array, year: str, channel: str, dataset: str, variation: str) -> awkward.Array:
    logger.debug(f"Adding SVFit variables to pair for channel {channel}")
    svfit_filepath = f"/vols/cms/ks1021/offline/HiggsDNA/IC/outputs/SVFIT/{year}/{channel}/{dataset}/{variation}/svfit.parquet"
    if not os.path.exists(svfit_filepath):
        logger.info(f"SVFit file not found for year {year}, channel {channel}, variation {variation}. Skipping...")
        return pair, False

    svfit_df = pd.read_parquet(svfit_filepath)
    svfit_df.drop_duplicates(subset=['run', 'lumi', 'event'], keep='first', inplace=True)

    event = pair['event']
    lumi = pair['lumi']
    run = pair['run']

    df = pd.DataFrame({"run": run, "lumi": lumi, "event": event})

    merged_df = pd.merge(
        df,
        svfit_df[['run', 'lumi', 'event', 'svfitMass', 'svfitMass_err', 'tau1_pt_svfit', 'tau2_pt_svfit']],
        on=['run', 'lumi', 'event'],
        how='left'
    )

    merged_df.fillna(-1, inplace=True)

    svfitMass = awkward.Array(merged_df['svfitMass'].values)
    svfitMass_err = awkward.Array(merged_df['svfitMass_err'].values)
    tau1_pt_svfit = awkward.Array(merged_df['tau1_pt_svfit'].values)
    tau2_pt_svfit = awkward.Array(merged_df['tau2_pt_svfit'].values)

    pair['svfit_Mass'] = svfitMass
    pair['svfit_Mass_err'] = svfitMass_err
    pair['svfit_tau1_pt'] = tau1_pt_svfit
    pair['svfit_tau2_pt'] = tau2_pt_svfit

    return pair, True


def add_fastmtt(pair: awkward.Array, year: str, channel: str, dataset: str, variation: str, mass_constraint: bool) -> awkward.Array:
    logger.debug(f"Adding FastMTT variables (Mass Constraint: {mass_constraint}) to pair for channel {channel}")

    if mass_constraint:
        ext = "_Window_123.0_127.0"
    else:
        ext = ""
    if channel == "tt":
        fastmtt_filepath = f"/vols/cms/ks1021/offline/HiggsDNA/master/HiggsDNA/output/test_0103_IP/{year}/{channel}/{dataset}/{variation}/fastmtt{ext}.parquet"
    else:
        fastmtt_filepath = f"/vols/cms/lcr119/offline/HiggsCP/HiggsDNA/output/FastMTT_updatedjets/{year}/{channel}/{dataset}/{variation}/fastmtt{ext}.parquet"
    if not os.path.exists(fastmtt_filepath):
        logger.info(f"FastMTT file not found for year {year}, channel {channel}, variation {variation}. Skipping...")
        return pair, False

    fastmtt_df = pd.read_parquet(fastmtt_filepath)
    fastmtt_df.drop_duplicates(subset=["run", "lumi", "event"], keep='first', inplace=True)

    event = pair['event']
    lumi = pair['lumi']
    run = pair['run']

    df = pd.DataFrame({"run": run, "lumi": lumi, "event": event})

    merged_df = pd.merge(
        df,
        fastmtt_df[['run', 'lumi', 'event', 'FastMTT_mass', 'FastMTT_pt', 'FastMTT_pt_1', 'FastMTT_pt_2']],
        on=['run', 'lumi', 'event'],
        how='left'
    )

    merged_df.fillna(-1, inplace=True)

    FastMTT_mass = awkward.Array(merged_df['FastMTT_mass'].values)
    FastMTT_pt = awkward.Array(merged_df['FastMTT_pt'].values)
    FastMTT_pt_1 = awkward.Array(merged_df['FastMTT_pt_1'].values)
    FastMTT_pt_2 = awkward.Array(merged_df['FastMTT_pt_2'].values)

    if mass_constraint:
        extension = "_constraint"
    else:
        extension = ""

    pair[f'FastMTT_mass{extension}'] = FastMTT_mass
    pair[f'FastMTT_pt{extension}'] = FastMTT_pt
    pair[f'FastMTT_pt_1{extension}'] = FastMTT_pt_1
    pair[f'FastMTT_pt_2{extension}'] = FastMTT_pt_2

    return pair, True


def replace_failed_FastMTT(pair: awkward.Array, replace: bool) -> awkward.Array:
    if replace:
        logger.debug("Replacing faulty FastMTT values")
        FastMTT_mass = pair['FastMTT_mass']
        FastMTT_mass_constraint = pair['FastMTT_mass_constraint']

        mask = (FastMTT_mass_constraint < 123.0) | (FastMTT_mass_constraint > 127.0)

        pair['FastMTT_mass_constraint'] = np.where(mask, FastMTT_mass, FastMTT_mass_constraint)
        pair['FastMTT_pt_constraint'] = np.where(mask, pair['FastMTT_pt'], pair['FastMTT_pt_constraint'])
        pair['FastMTT_pt_1_constraint'] = np.where(mask, pair['FastMTT_pt_1'], pair['FastMTT_pt_1_constraint'])
        pair['FastMTT_pt_2_constraint'] = np.where(mask, pair['FastMTT_pt_2'], pair['FastMTT_pt_2_constraint'])

    return pair
