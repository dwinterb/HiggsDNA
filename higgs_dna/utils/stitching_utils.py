import numpy as np
import yaml
import os


def samples_to_stitch(sample):
    samples = [
        'DYto2L_M_50_madgraphMLM',
        'DYto2L_M_50_madgraphMLM_ext1',
        'DYto2L_M_50_1J_madgraphMLM',
        'DYto2L_M_50_2J_madgraphMLM',
        'DYto2L_M_50_3J_madgraphMLM',
        'DYto2L_M_50_4J_madgraphMLM',
        'WtoLNu_madgraphMLM',
        'WtoLNu_madgraphMLM_ext1',
        'WtoLNu_1J_madgraphMLM',
        'WtoLNu_2J_madgraphMLM',
        'WtoLNu_3J_madgraphMLM',
        'WtoLNu_4J_madgraphMLM',
        'DYto2L_M_50_amcatnloFXFX',
        'DYto2L_M_50_amcatnloFXFX_ext1',
        'DYto2L_M_50_0J_amcatnloFXFX',
        'DYto2L_M_50_1J_amcatnloFXFX',
        'DYto2L_M_50_2J_amcatnloFXFX',
        'DYto2L_M_50_PTLL_40to100_1J_amcatnloFXFX',
        'DYto2L_M_50_PTLL_100to200_1J_amcatnloFXFX',
        'DYto2L_M_50_PTLL_200to400_1J_amcatnloFXFX',
        'DYto2L_M_50_PTLL_400to600_1J_amcatnloFXFX',
        'DYto2L_M_50_PTLL_600_1J_amcatnloFXFX',
        'DYto2L_M_50_PTLL_40to100_2J_amcatnloFXFX',
        'DYto2L_M_50_PTLL_100to200_2J_amcatnloFXFX',
        'DYto2L_M_50_PTLL_200to400_2J_amcatnloFXFX',
        'DYto2L_M_50_PTLL_400to600_2J_amcatnloFXFX',
        'DYto2L_M_50_PTLL_600_2J_amcatnloFXFX'
    ]
    if sample in samples:
        if "DY" in sample and "madgraphMLM" in sample:
            return (True, "DY")
        elif "DY" in sample and "amcatnloFXFX" in sample:
            return (True, "DY_NLO")
        elif "W" in sample and "madgraphMLM" in sample:
            return (True, "WJ")
    return (False, None)


def stitching(year,events,process):
    nLHEjets = events.LHE.Njets
    # load in stitching.yml file to read information from it
    path_file = os.path.join(os.getcwd(), f'scripts/ditau/config/{year}/Stitching.yaml')
    with open(path_file) as f:
        stitching = yaml.safe_load(f)

    # get the stitching information for the given year
    stitching = stitching[process]

    inclusive_xs = stitching['Inclusive']['Cross_section']
    inclusive_eff_evts = stitching['Inclusive']['Effective_Events']

    numerator_1 = (stitching['1J']['Cross_section'] / inclusive_xs) * inclusive_eff_evts
    numerator_2 = (stitching['2J']['Cross_section'] / inclusive_xs) * inclusive_eff_evts
    numerator_3 = (stitching['3J']['Cross_section'] / inclusive_xs) * inclusive_eff_evts
    numerator_4 = (stitching['4J']['Cross_section'] / inclusive_xs) * inclusive_eff_evts

    denominator_1 = numerator_1 + stitching['1J']['Effective_Events']
    denominator_2 = numerator_2 + stitching['2J']['Effective_Events']
    denominator_3 = numerator_3 + stitching['3J']['Effective_Events']
    denominator_4 = numerator_4 + stitching['4J']['Effective_Events']

    # get the weights for the stitching
    weight_1 = numerator_1 / denominator_1
    weight_2 = numerator_2 / denominator_2
    weight_3 = numerator_3 / denominator_3
    weight_4 = numerator_4 / denominator_4

    stitching_weights = np.ones(len(nLHEjets))
    stitching_weights = np.where(
        nLHEjets == 1, weight_1,
        np.where(
            nLHEjets == 2, weight_2,
            np.where(
                nLHEjets == 3, weight_3,
                np.where(nLHEjets == 4, weight_4, stitching_weights)
            )
        )
    )

    return stitching_weights


def stitching_NLO(year,events,process):
    npNLO = events.LHE.NpNLO
    Vpt = events.LHE.Vpt

    # load in stitching.yml file to read information from it
    path_file = os.path.join(os.getcwd(), f'scripts/ditau/config/{year}/Stitching.yaml')
    with open(path_file) as f:
        stitching = yaml.safe_load(f)

    # get the stitching information for the given year
    stitching = stitching[process]

    # inclusive cross section and effective events
    inclusive_xs = stitching['Inclusive']['Cross_section']
    inclusive_eff_evts = stitching['Inclusive']['Effective_Events']

    # determine xs to be used for where pTLL<40 (no datasets to measure on)
    binned_1J_0to40_xs = stitching['1J']['Cross_section'] - (stitching['1J_40to100']['Cross_section']
                                                             + stitching['1J_100to200']['Cross_section']
                                                             + stitching['1J_200to400']['Cross_section']
                                                             + stitching['1J_400to600']['Cross_section']
                                                             + stitching['1J_600toInf']['Cross_section'])
    binned_2J_0to40_xs = stitching['1J']['Cross_section'] - (stitching['2J_40to100']['Cross_section']
                                                             + stitching['2J_100to200']['Cross_section']
                                                             + stitching['2J_200to400']['Cross_section']
                                                             + stitching['2J_400to600']['Cross_section']
                                                             + stitching['2J_600toInf']['Cross_section'])

    # stitch 0J events (no pT binned samples)
    numerator_0J = (stitching['0J']['Cross_section'] / inclusive_xs) * inclusive_eff_evts
    denominator_0J = numerator_0J + stitching['0J']['Effective_Events']

    # stitch 1J events (pT inclusive and binned samples)
    numerator_1J_0_40 = (binned_1J_0to40_xs / inclusive_xs) * inclusive_eff_evts
    numerator_1J_40_100 = (stitching['1J_40to100']['Cross_section'] / inclusive_xs) * inclusive_eff_evts
    numerator_1J_100_200 = (stitching['1J_100to200']['Cross_section'] / inclusive_xs) * inclusive_eff_evts
    numerator_1J_200_400 = (stitching['1J_200to400']['Cross_section'] / inclusive_xs) * inclusive_eff_evts
    numerator_1J_400_600 = (stitching['1J_400to600']['Cross_section'] / inclusive_xs) * inclusive_eff_evts
    numerator_1J_600 = (stitching['1J_600toInf']['Cross_section'] / inclusive_xs) * inclusive_eff_evts
    denominator_1J_0_40 = numerator_1J_0_40 + (binned_1J_0to40_xs / stitching['1J']['Cross_section']) * stitching['1J']['Effective_Events']
    denominator_1J_40_100 = numerator_1J_40_100 + (stitching['1J_40to100']['Cross_section'] / stitching['1J']['Cross_section']) * stitching['1J']['Effective_Events'] + stitching['1J_40to100']['Effective_Events']
    denominator_1J_100_200 = numerator_1J_100_200 + (stitching['1J_100to200']['Cross_section'] / stitching['1J']['Cross_section']) * stitching['1J']['Effective_Events'] + stitching['1J_100to200']['Effective_Events']
    denominator_1J_200_400 = numerator_1J_200_400 + (stitching['1J_200to400']['Cross_section'] / stitching['1J']['Cross_section']) * stitching['1J']['Effective_Events'] + stitching['1J_200to400']['Effective_Events']
    denominator_1J_400_600 = numerator_1J_400_600 + (stitching['1J_400to600']['Cross_section'] / stitching['1J']['Cross_section']) * stitching['1J']['Effective_Events'] + stitching['1J_400to600']['Effective_Events']
    denominator_1J_600 = numerator_1J_600 + (stitching['1J_600toInf']['Cross_section'] / stitching['1J']['Cross_section']) * stitching['1J']['Effective_Events'] + stitching['1J_600toInf']['Effective_Events']

    # stitch 2J events (pT inclusive and binned samples)
    numerator_2J_0_40 = (binned_2J_0to40_xs / inclusive_xs) * inclusive_eff_evts
    numerator_2J_40_100 = (stitching['2J_40to100']['Cross_section'] / inclusive_xs) * inclusive_eff_evts
    numerator_2J_100_200 = (stitching['2J_100to200']['Cross_section'] / inclusive_xs) * inclusive_eff_evts
    numerator_2J_200_400 = (stitching['2J_200to400']['Cross_section'] / inclusive_xs) * inclusive_eff_evts
    numerator_2J_400_600 = (stitching['2J_400to600']['Cross_section'] / inclusive_xs) * inclusive_eff_evts
    numerator_2J_600 = (stitching['2J_600toInf']['Cross_section'] / inclusive_xs) * inclusive_eff_evts
    denominator_2J_0_40 = numerator_2J_0_40 + (binned_2J_0to40_xs / stitching['2J']['Cross_section']) * stitching['2J']['Effective_Events']
    denominator_2J_40_100 = numerator_2J_40_100 + (stitching['2J_40to100']['Cross_section'] / stitching['2J']['Cross_section']) * stitching['2J']['Effective_Events'] + stitching['2J_40to100']['Effective_Events']
    denominator_2J_100_200 = numerator_2J_100_200 + (stitching['2J_100to200']['Cross_section'] / stitching['2J']['Cross_section']) * stitching['2J']['Effective_Events'] + stitching['2J_100to200']['Effective_Events']
    denominator_2J_200_400 = numerator_2J_200_400 + (stitching['2J_200to400']['Cross_section'] / stitching['2J']['Cross_section']) * stitching['2J']['Effective_Events'] + stitching['2J_200to400']['Effective_Events']
    denominator_2J_400_600 = numerator_2J_400_600 + (stitching['2J_400to600']['Cross_section'] / stitching['2J']['Cross_section']) * stitching['2J']['Effective_Events'] + stitching['2J_400to600']['Effective_Events']
    denominator_2J_600 = numerator_2J_600 + (stitching['2J_600toInf']['Cross_section'] / stitching['2J']['Cross_section']) * stitching['2J']['Effective_Events'] + stitching['2J_600toInf']['Effective_Events']

    # get the weights for the stitching
    weight_0J = numerator_0J / denominator_0J
    weight_1J_0_40 = numerator_1J_0_40 / denominator_1J_0_40
    weight_1J_40_100 = numerator_1J_40_100 / denominator_1J_40_100
    weight_1J_100_200 = numerator_1J_100_200 / denominator_1J_100_200
    weight_1J_200_400 = numerator_1J_200_400 / denominator_1J_200_400
    weight_1J_400_600 = numerator_1J_400_600 / denominator_1J_400_600
    weight_1J_600 = numerator_1J_600 / denominator_1J_600
    weight_2J_0_40 = numerator_2J_0_40 / denominator_2J_0_40
    weight_2J_40_100 = numerator_2J_40_100 / denominator_2J_40_100
    weight_2J_100_200 = numerator_2J_100_200 / denominator_2J_100_200
    weight_2J_200_400 = numerator_2J_200_400 / denominator_2J_200_400
    weight_2J_400_600 = numerator_2J_400_600 / denominator_2J_400_600
    weight_2J_600 = numerator_2J_600 / denominator_2J_600

    # apply weights where appropriate
    stitching_weights = np.ones(len(npNLO))
    stitching_weights = np.where(
        npNLO == 0, weight_0J,
        np.where((npNLO == 1) & (0 <= Vpt) & (Vpt < 40), weight_1J_0_40,
                 np.where((npNLO == 1) & (40 <= Vpt) & (Vpt < 100), weight_1J_40_100,
                          np.where((npNLO == 1) & (100 <= Vpt) & (Vpt < 200), weight_1J_100_200,
                                   np.where((npNLO == 1) & (200 <= Vpt) & (Vpt < 400), weight_1J_200_400,
                                            np.where((npNLO == 1) & (400 <= Vpt) & (Vpt < 600), weight_1J_400_600,
                                                     np.where((npNLO == 1) & (600 <= Vpt), weight_1J_600,
                                                              np.where((npNLO == 2) & (0 <= Vpt) & (Vpt < 40), weight_2J_0_40,
                                                                       np.where((npNLO == 2) & (40 <= Vpt) & (Vpt < 100), weight_2J_40_100,
                                                                                np.where((npNLO == 2) & (100 <= Vpt) & (Vpt < 200), weight_2J_100_200,
                                                                                         np.where((npNLO == 2) & (200 <= Vpt) & (Vpt < 400), weight_2J_200_400,
                                                                                                  np.where((npNLO == 2) & (400 <= Vpt) & (Vpt < 600), weight_2J_400_600,
                                                                                                           np.where((npNLO == 2) & (600 <= Vpt), weight_2J_600,
                                                                                                                    stitching_weights
                                                                                                                    )
                                                                                                           )
                                                                                                  )
                                                                                         )
                                                                                )
                                                                       )
                                                              )
                                                     )
                                            )
                                   )
                          )
                 )
    )

    return stitching_weights
