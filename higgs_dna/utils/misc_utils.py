import awkward as ak


def choose_jet(jets_variable, n, fill_value):
    """
    this helper function is used to create flat jets from a jagged collection,
    parameters:
    * jet_variable: (ak array) selected variable from the jet collection
    * n: (int) nth jet to be selected
    * fill_value: (float) value with wich to fill the padded none.
    """
    leading_jets_variable = jets_variable[
        ak.local_index(jets_variable) == n
    ]
    leading_jets_variable = ak.pad_none(
        leading_jets_variable, 1
    )
    leading_jets_variable = ak.flatten(
        ak.fill_none(leading_jets_variable, fill_value)
    )
    return leading_jets_variable


def identify_dataset(dataset_name, year):
    """
    this helper function is used to identify the dataset from the dataset name, year
    parameters:
    * dataset_name: (str) name of the dataset
    """

    if year == "Run3_2022":
        identifiers = {
            'Run2022C': 'RunC',
            'Run2022D': 'RunD',
        }
    elif year == "Run3_2022EE":
        identifiers = {
            'Run2022E': 'RunE',
            'Run2022F': 'RunF',
            'Run2022G': 'RunG',
        }
    elif year == "Run3_2023":
        identifiers = {
            'Run3_2023C': 'RunC',
        }
    elif year == "Run3_2023BPix":
        identifiers = {
            'Run3_2023D': 'RunD',
        }
    else:
        raise ValueError(f"Year {year} not recognized, only Run3_2022, Run3_2022EE, Run3_2023, Run3_2023BPix are supported")

    value = None
    for key, value in identifiers.items():
        if key in dataset_name:
            value = value
            break

    if value is None:
        raise ValueError(f"Dataset {dataset_name} not recognized, only {identifiers.keys()} are supported")

    return value
