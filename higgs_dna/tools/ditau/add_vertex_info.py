import awkward
import logging

logger = logging.getLogger(__name__)


def add_vertex_information(pairs: awkward.Array, events: awkward.Array) -> awkward.Array:

    logger.debug("Adding Vertex information")

    pairs['PV_x'] = events.PV.x
    pairs['PV_y'] = events.PV.y
    pairs['PV_z'] = events.PV.z

    pairs["PVBS_x"] = awkward.flatten(events.PVBS.x)
    pairs["PVBS_y"] = awkward.flatten(events.PVBS.y)
    pairs["PVBS_z"] = awkward.flatten(events.PVBS.z)

    pairs["ip_x_1"] = pairs["obj_1"].IPx
    pairs["ip_y_1"] = pairs["obj_1"].IPy
    pairs["ip_z_1"] = pairs["obj_1"].IPz
    pairs["ip_x_2"] = pairs["obj_2"].IPx
    pairs["ip_y_2"] = pairs["obj_2"].IPy
    pairs["ip_z_2"] = pairs["obj_2"].IPz

    pairs["ip_LengthSig_1"] = abs(pairs["obj_1"].ipLengthSig)
    pairs["ip_LengthSig_2"] = abs(pairs["obj_2"].ipLengthSig)

    for index in ['1', '2']:
        if 'hasRefitSV' in pairs[f'obj_{index}'].fields:
            pairs[f"hasRefitSV_{index}"] = pairs[f"obj_{index}"].hasRefitSV
            pairs[f"sv_x_{index}"] = pairs[f"obj_{index}"].refitSVx
            pairs[f"sv_y_{index}"] = pairs[f"obj_{index}"].refitSVy
            pairs[f"sv_z_{index}"] = pairs[f"obj_{index}"].refitSVz
        else:
            pairs[f"hasRefitSV_{index}"] = awkward.zeros_like(pairs[f"obj_{index}"].pt)
            pairs[f"sv_x_{index}"] = awkward.ones_like(pairs[f"obj_{index}"].IPx) * -9999.0
            pairs[f"sv_y_{index}"] = awkward.ones_like(pairs[f"obj_{index}"].IPy) * -9999.0
            pairs[f"sv_z_{index}"] = awkward.ones_like(pairs[f"obj_{index}"].IPz) * -9999.0

    return pairs
