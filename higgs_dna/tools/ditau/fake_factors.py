import os
import awkward
import numpy as np
import correctionlib
import logging

logger = logging.getLogger(__name__)


def apply_fake_factors(pairs: awkward.Array, year: str, variation: str, combined: bool) -> awkward.Array:

    higgs_dna_path = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

    if combined:
        logger.debug(f"Applying fake factor weight (combined eras) - for variation {variation}")
        path_to_json = os.path.join(higgs_dna_path, "systematics/ditau/JSONs/FakeFactors/fake_factor_20222023.json.gz")
    else:
        logger.debug(f"Applying fake factor weight (individual eras) - for variation {variation}")
        year_mapping = {
            "Run3_2022": "2022_preEE",
            "Run3_2022EE": "2022_postEE",
            "Run3_2023": "2023_preBPix",
            "Run3_2023BPix": "2023_postBPix",
        }
        era = year_mapping.get(year, None)
        # load era specific fake factors json
        path_to_json = os.path.join(higgs_dna_path, f"systematics/ditau/JSONs/FakeFactors/fake_factor_{era}.json.gz")

    # evaluate with correctionlib
    evaluator = correctionlib.CorrectionSet.from_file(path_to_json)["fake_factors_fit"]

    # inputs are: pt, dm, njets, syst
    pt = pairs['obj_1'].pt
    dm = pairs['obj_1'].decayModePNet
    masked_dm = np.where(dm == -1, 0, dm)  # mask -1 dm values
    n_jets = np.where(pairs['n_jets'] >= 2, 2, pairs['n_jets'])  # limit to 2 (bin is >= 2)

    if variation == 'nominal':
        variation = 'nom'  # nominal is called 'nom' in the json...

    fake_factor = evaluator.evaluate(pt, masked_dm, n_jets, variation)

    # apply fake factor to events satisfying application region criteria
    os_pair = pairs['os']
    tau_vsjet_1 = pairs['obj_1'].idDeepTau2018v2p5VSjet
    tau_vsjet_2 = pairs['obj_2'].idDeepTau2018v2p5VSjet
    m_vis = pairs["mass"]
    # fake factor weight is 1 by default
    w_fake_factor = np.ones(len(pt))
    # apply fake factor weight to application region (os + anti-iso tau1 + iso tau 2)
    w_fake_factor = np.where((m_vis > 40) & (os_pair == 1) & (tau_vsjet_1 < 7) & (tau_vsjet_1 >= 2) & (tau_vsjet_2 >= 7), fake_factor, w_fake_factor)

    return w_fake_factor
