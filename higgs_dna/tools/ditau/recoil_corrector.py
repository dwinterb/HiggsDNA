import os
import awkward
import numpy as np
import correctionlib
import logging

logger = logging.getLogger(__name__)


def field_check(pairs: awkward.Array) -> bool:
    fields = ["gen_boson_pT", "gen_boson_phi", "gen_boson_visible_pT", "gen_boson_visible_phi"]
    for field in fields:
        if field not in pairs.fields:
            logger.warning(f"MET Recoil correction skipped for this chunk. Missing field: {field}")
            return False
    return True


def apply_RecoilCorrections(met: awkward.Array, pairs: awkward.Array, year: str, dataset_name: str) -> awkward.Array:
    year_mapping = {
        "Run3_2022": "2022preEE",
        "Run3_2022EE": "2022postEE",
        "Run3_2023": "2023preBPix",
        "Run3_2023BPix": "2023postBPix",
    }

    era = year_mapping.get(year, None)

    LO_datasets = [
        "DYto2L_M_10to50_madgraphMLM","DYto2L_M_50_madgraphMLM","DYto2L_M_50_madgraphMLM_ext1","DYto2L_M_50_1J_madgraphMLM","DYto2L_M_50_2J_madgraphMLM","DYto2L_M_50_3J_madgraphMLM","DYto2L_M_50_4J_madgraphMLM",
        "WtoLNu_madgraphMLM", "WtoLNu_madgraphMLM_ext1", "WtoLNu_1J_madgraphMLM", "WtoLNu_2J_madgraphMLM", "WtoLNu_3J_madgraphMLM", "WtoLNu_4J_madgraphMLM",
    ]

    NLO_datasets = [
        "DYto2L_M_10to50_amcatnloFXFX","DYto2L_M_50_amcatnloFXFX","DYto2L_M_50_amcatnloFXFX_ext1","DYto2L_M_50_0J_amcatnloFXFX","DYto2L_M_50_1J_amcatnloFXFX","DYto2L_M_50_2J_amcatnloFXFX",
        "DYto2L_M_50_PTLL_40to100_1J_amcatnloFXFX","DYto2L_M_50_PTLL_100to200_1J_amcatnloFXFX","DYto2L_M_50_PTLL_200to400_1J_amcatnloFXFX","DYto2L_M_50_PTLL_400to600_1J_amcatnloFXFX","DYto2L_M_50_PTLL_600_1J_amcatnloFXFX",
        "DYto2L_M_50_PTLL_40to100_2J_amcatnloFXFX","DYto2L_M_50_PTLL_100to200_2J_amcatnloFXFX","DYto2L_M_50_PTLL_200to400_2J_amcatnloFXFX","DYto2L_M_50_PTLL_400to600_2J_amcatnloFXFX","DYto2L_M_50_PTLL_600_2J_amcatnloFXFX",
        "GluGluHTo2Tau_UncorrelatedDecay_CPodd_UnFiltered_ProdAndDecay", "GluGluHTo2Tau_UncorrelatedDecay_MM_UnFiltered_ProdAndDecay", "GluGluHTo2Tau_UncorrelatedDecay_SM_UnFiltered_ProdAndDecay", "ZHToTauTau_UncorrelatedDecay_UnFiltered", "GluGluHTo2Tau_UncorrelatedDecay_UnFiltered", "WplusHToTauTau_UncorrelatedDecay_UnFiltered", "WminusHToTauTau_UncorrelatedDecay_UnFiltered", "VBFHToTauTau_UncorrelatedDecay_UnFiltered",
        "GluGluHTo2Tau_UncorrelatedDecay_CPodd_Filtered_ProdAndDecay", "GluGluHTo2Tau_UncorrelatedDecay_MM_Filtered_ProdAndDecay", "GluGluHTo2Tau_UncorrelatedDecay_SM_Filtered_ProdAndDecay", "ZHToTauTau_UncorrelatedDecay_Filtered", "GluGluHTo2Tau_UncorrelatedDecay_Filtered", "WplusHToTauTau_UncorrelatedDecay_Filtered", "WminusHToTauTau_UncorrelatedDecay_Filtered", "VBFHToTauTau_UncorrelatedDecay_Filtered",
    ]

    if era is None:
        logger.error(f"MET recoil correction not applied to {dataset_name}. Invalid year {year}")
        return met

    if dataset_name in LO_datasets:
        logger.debug(f"Applying LO MET recoil correction to {dataset_name}. Era: {era}")
        if not field_check(pairs):
            return met
        met = correct_met_recoil(met, pairs, era, isLO=True)
    elif dataset_name in NLO_datasets:
        logger.debug(f"Applying NLO MET recoil correction to {dataset_name}. Era: {era}")
        if not field_check(pairs):
            return met
        met = correct_met_recoil(met, pairs, era, isLO=False)
    else:
        logger.debug("No recoil correction applied")

    return met


def correct_met_recoil(met: awkward.Array, pairs: awkward.Array, era: str, isLO: bool) -> awkward.Array:

    met_pt = met.pt
    met_phi = met.phi
    gen_boson_pt = pairs.gen_boson_pT
    gen_boson_phi = pairs.gen_boson_phi
    gen_boson_visible_pt = pairs.gen_boson_visible_pT
    gen_boson_visible_phi = pairs.gen_boson_visible_phi

    met_x = met_pt * np.cos(met_phi)
    met_y = met_pt * np.sin(met_phi)
    gen_boson_x = gen_boson_pt * np.cos(gen_boson_phi)
    gen_boson_y = gen_boson_pt * np.sin(gen_boson_phi)
    gen_boson_visible_x = gen_boson_visible_pt * np.cos(gen_boson_visible_phi)
    gen_boson_visible_y = gen_boson_visible_pt * np.sin(gen_boson_visible_phi)

    U_x = met_x + gen_boson_visible_x - gen_boson_x
    U_y = met_y + gen_boson_visible_y - gen_boson_y

    U_pt = np.sqrt(U_x**2 + U_y**2)
    U_phi = np.arctan2(U_y, U_x)

    Upara = U_pt * np.cos(U_phi - gen_boson_phi)
    Uperp = U_pt * np.sin(U_phi - gen_boson_phi)

    higgs_dna_path = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
    path_to_json = os.path.join(higgs_dna_path, "systematics/ditau/JSONs/MET_Recoil/Recoil_corrections.json.gz")
    evaluator = correctionlib.CorrectionSet.from_file(path_to_json)["Recoil_correction_QuantileMapHist"]

    n_jets = awkward.to_numpy(pairs.n_jets_recoil).astype(float)

    if isLO:
        Upara_corr = evaluator.evaluate(era, "LO", n_jets, pairs.gen_boson_pT, "Upara", Upara)
        Uperp_corr = evaluator.evaluate(era, "LO", n_jets, pairs.gen_boson_pT, "Uperp", Uperp)
    else:
        Upara_corr = evaluator.evaluate(era, "NLO", n_jets, pairs.gen_boson_pT, "Upara", Upara)
        Uperp_corr = evaluator.evaluate(era, "NLO", n_jets, pairs.gen_boson_pT, "Uperp", Uperp)

    U_pt_corr = np.sqrt(Upara_corr**2 + Uperp_corr**2)
    U_phi_corr = np.arctan2(Uperp_corr, Upara_corr) + gen_boson_phi
    U_x_corr = U_pt_corr * np.cos(U_phi_corr)
    U_y_corr = U_pt_corr * np.sin(U_phi_corr)
    met_x_corr = U_x_corr - gen_boson_visible_x + gen_boson_x
    met_y_corr = U_y_corr - gen_boson_visible_y + gen_boson_y

    met_pt_corr = np.sqrt(met_x_corr**2 + met_y_corr**2)
    met_phi_corr = np.arctan2(met_y_corr, met_x_corr)

    met["pt"] = met_pt_corr
    met["phi"] = met_phi_corr

    return met
