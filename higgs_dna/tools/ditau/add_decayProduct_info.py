import awkward
import logging
from higgs_dna.utils.kinematic_utilities import calculate_p4, sum_p4
import warnings

logger = logging.getLogger(__name__)


def add_decayProduct_information(pairs: awkward.Array, events: awkward.Array, channel: str) -> awkward.Array:

    logger.debug("Adding decay product information")

    pdgId_to_mass = {
        211: 0.13957,
        13: 0.13957,
        10211: 0.13957,
        130: 0.13957,
        11: 0.000511,
        22: 0.0,
        -211: 0.13957,
        -13: 0.13957,
        -10211: 0.13957,
        -130: 0.13957,
        -11: 0.000511,
    }
    neutral_pion_mass = 0.13498

    particle_names = ['pi', 'pi2', 'pi3', 'pi0']
    components = ['pt', 'eta', 'phi', 'mass', 'charge', 'pdgId', 'Energy']

    def assign_values(pairs, particle, index, value_dict):
        for component in components:
            pairs[f"{particle}_{component}_{index}"] = value_dict[component]

    # Suppress warnings associated with non-sense LorentzVectors (As Advised by coffea devs)
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=RuntimeWarning)

        for index,lepton in enumerate(channel):
            index += 1
            if lepton == 't':
                default_value = -9999.0
                decayMode = pairs[f'obj_{index}'].decayMode
                tau_index = pairs[f"obj_{index}"].original_index
                tauProds = events.TauProd
                mask = tauProds.tauIdx == awkward.broadcast_arrays(tau_index, tauProds.tauIdx)[0]
                tauProds = tauProds[mask]

                mass = default_value * awkward.ones_like(awkward.flatten(tauProds.pdgId))
                for pdgId, mass_value in pdgId_to_mass.items():
                    mass = awkward.where(awkward.flatten(tauProds.pdgId) == pdgId, mass_value, mass)
                mass = awkward.unflatten(mass, awkward.num(tauProds))
                tauProds = awkward.with_field(tauProds, mass, "mass")

                # ----------------------------------------------------------------------------------------------------
                # Procedure:
                # Careful: there is currently no way to distinguish between electrons in strips and prongs
                # Charged Hadrons could be pions, muons, recovery tracks, neutral hadrons or electrons (in that order)

                # Apply masks to get the different types of particles
                charged_pion_mask = (abs(tauProds.pdgId) == 211)
                muon_mask = (abs(tauProds.pdgId) == 13)
                recoveryTrack_mask = (abs(tauProds.pdgId) == 10211)
                neutral_hadron_mask = (tauProds.pdgId == 130)
                electron_mask = (abs(tauProds.pdgId) == 11)
                photon_mask = (tauProds.pdgId == 22)

                pions = tauProds[charged_pion_mask]
                muons = tauProds[muon_mask]
                recoveryTracks = tauProds[recoveryTrack_mask]
                neutral_hadrons = tauProds[neutral_hadron_mask]
                electrons = tauProds[electron_mask]
                photons = tauProds[photon_mask]

                charged_hadrons = pions
                charged_hadrons = awkward.concatenate([charged_hadrons, muons, recoveryTracks, neutral_hadrons], axis=1)

                # Step 1: Sort the electrons by pt
                sorted_indices = awkward.argsort(electrons.pt, ascending=False)
                electrons_sorted = electrons[sorted_indices]

                # Step 2: Fill the charged hadrons with electrons if there are not enough charged hadrons

                ## Decay Modes 0 OR 1
                mask_dm_0_OR_1 = (decayMode == 0) | (decayMode == 1)
                charged_hadrons = awkward.where(
                    mask_dm_0_OR_1,
                    awkward.where(
                        (awkward.num(charged_hadrons) == 0) & (awkward.num(electrons_sorted) >= 1),
                        awkward.concatenate([charged_hadrons, electrons_sorted[:,:1]], axis=1),
                        charged_hadrons,
                    ),
                    charged_hadrons
                )

                ## Decay Modes 10 OR 11
                mask_dm_10_OR_11 = (decayMode == 10) | (decayMode == 11)
                charged_hadrons = awkward.where(
                    mask_dm_10_OR_11,
                    awkward.where(
                        awkward.num(charged_hadrons) == 0 & (awkward.num(electrons_sorted) >= 3),
                        awkward.concatenate([charged_hadrons, electrons_sorted[:,:3]], axis=1),
                        awkward.where(
                            awkward.num(charged_hadrons) == 1 & (awkward.num(electrons_sorted) >= 2),
                            awkward.concatenate([charged_hadrons, electrons_sorted[:,:2]], axis=1),
                            awkward.where(
                                awkward.num(charged_hadrons) == 2 & (awkward.num(electrons_sorted) >= 1),
                                awkward.concatenate([charged_hadrons, electrons_sorted[:,:1]], axis=1),
                                charged_hadrons
                            ),
                        ),
                    ),
                    charged_hadrons
                )

                # Step 3: Building pi0s, photons + remaining electrons (i.e. electrons that were not used for charged hadrons)

                ## Decay Mode 1
                mask_dm_1 = (decayMode == 1)
                neutral_hadrons = awkward.where(
                    mask_dm_1,
                    awkward.where(
                        (awkward.num(charged_hadrons) == 0) & (awkward.num(electrons_sorted) > 1),
                        awkward.concatenate([photons, electrons_sorted[:,1:]], axis=1),
                        awkward.where(
                            (awkward.num(charged_hadrons) == 1) & (awkward.num(electrons_sorted) > 0),
                            awkward.concatenate([photons, electrons_sorted], axis=1),
                            photons),
                    ),
                    photons
                )

                ## Decay Mode 11
                mask_dm_11 = (decayMode == 11)
                neutral_hadrons = awkward.where(
                    mask_dm_11,
                    awkward.where(
                        (awkward.num(charged_hadrons) == 0) & (awkward.num(electrons_sorted) > 3),
                        awkward.concatenate([photons, electrons_sorted[:,3:]], axis=1),
                        awkward.where(
                            (awkward.num(charged_hadrons) == 1) & (awkward.num(electrons_sorted) > 2),
                            awkward.concatenate([photons, electrons_sorted[:,2:]], axis=1),
                            awkward.where(
                                (awkward.num(charged_hadrons) == 2) & (awkward.num(electrons_sorted) > 1),
                                awkward.concatenate([photons, electrons_sorted[:,1:]], axis=1),
                                awkward.where(
                                    (awkward.num(charged_hadrons) == 3) & (awkward.num(electrons_sorted) > 0),
                                    awkward.concatenate([photons, electrons_sorted], axis=1),
                                    photons
                                ),
                            ),
                        ),
                    ),
                    photons
                )

                pions = charged_hadrons
                pions = awkward.fill_none(awkward.pad_none(pions, 3, clip=True), {"pt": default_value, "eta": default_value, "phi": default_value, "pdgId": default_value, "mass": default_value})
                charges = awkward.where(pions.pdgId > 0, 1, -1)
                pions = awkward.with_field(pions, charges, "charge")
                pions_p4 = calculate_p4(pions)

                photons = neutral_hadrons
                photons = photons[awkward.argsort(photons.pt, ascending=False)]
                photons_p4 = calculate_p4(photons)
                pi0_eta = awkward.fill_none(awkward.firsts(photons.eta, axis=-1), default_value)
                pi0_phi = awkward.fill_none(awkward.firsts(photons.phi, axis=-1), default_value)
                pi0_mass = neutral_pion_mass * awkward.ones_like(pi0_eta)
                sum_photons_p4 = sum_p4(photons_p4, axis=-1)
                pi0_pt = awkward.where(sum_photons_p4.pt == 0, default_value, sum_photons_p4.pt)
                pi0_pdgId = 111 * awkward.ones_like(pi0_eta)

                pi0 = awkward.zip(
                    {
                        "pt": pi0_pt,
                        "eta": pi0_eta,
                        "phi": pi0_phi,
                        "mass": pi0_mass,
                        "pdgId": pi0_pdgId,
                        "charge": awkward.zeros_like(pi0_pt),
                    }
                )
                pi0_p4 = calculate_p4(pi0)

                pions_E = awkward.where(pions_p4.pt == default_value, default_value, pions_p4.E)
                pi0_E = awkward.where(pi0_p4.pt == default_value, default_value, pi0_p4.E)
                pions_p4 = awkward.with_field(pions_p4, pions_E, "Energy")
                pi0_p4 = awkward.with_field(pi0_p4, pi0_E, "Energy")

                for i, particle in enumerate(particle_names[:3]):
                    value_dict = {comp: getattr(pions_p4[:, i], comp) for comp in components}
                    assign_values(pairs, particle, index, value_dict)

                value_dict = {comp: getattr(pi0_p4, comp) for comp in components}
                assign_values(pairs, particle_names[3], index, value_dict)

            else:
                default_value = -9999.0 * awkward.ones_like(pairs['obj_1'].pt)
                for particle in particle_names:
                    value_dict = {comp: default_value for comp in components}
                    assign_values(pairs, particle, index, value_dict)

    return pairs
