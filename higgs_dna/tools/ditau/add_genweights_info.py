import awkward
import logging

logger = logging.getLogger(__name__)


def add_lhe_weights(pairs: awkward.Array, events: awkward.Array, dataset_name: str) -> awkward.Array:
    """
    Adds LHE weights to pairs of objects and returns the modified pairs.
    Args:
        pairs (awkward.Array): An Awkward Array containing pairs of objects.
        events (awkward.Array): An Awkward Array containing the events data.
    Returns:
        awkward.Array: The modified pairs with additional LHE weights.
    - The LHE weights are added to the pairs of objects.
    """
    logger.debug("Adding LHE weights")
    if "ProdAndDecay" in dataset_name:
        pairs["LHEReweightingWeight_SM"] = events.LHEReweightingWeight[:,0]
        pairs["LHEReweightingWeight_PS"] = events.LHEReweightingWeight[:,1]
        pairs["LHEReweightingWeight_MM"] = events.LHEReweightingWeight[:,2]
    else:
        pairs["LHEReweightingWeight_SM"] = awkward.ones_like(pairs["obj_1"].pt)
        pairs["LHEReweightingWeight_PS"] = awkward.ones_like(pairs["obj_1"].pt)
        pairs["LHEReweightingWeight_MM"] = awkward.ones_like(pairs["obj_1"].pt)

    if "LHE" in events.fields:
        if hasattr(events.LHE, "Njets"):
            pairs["nLHEjets"] = events.LHE.Njets

        if hasattr(events.LHE, "NpNLO"):
            pairs["npNLOjets"] = events.LHE.NpNLO

        if hasattr(events.LHE, "Vpt"):
            pairs["LHE_Vpt"] = events.LHE.Vpt

    # store gen weight:
    pairs['genWeight'] = events.genWeight

    return pairs


def add_tauSpinner_weights(pairs: awkward.Array, events: awkward.Array) -> awkward.Array:

    logger.debug("Adding TauSpinner quantities")

    wt_cp_sm = awkward.zeros_like(pairs["obj_1"].pt)
    wt_cp_ps = awkward.zeros_like(pairs["obj_1"].pt)
    wt_cp_mm = awkward.zeros_like(pairs["obj_1"].pt)

    if "TauSpinner" in events.fields:
        wt_cp_sm = events.TauSpinner.weight_cp_0
        wt_cp_ps = events.TauSpinner.weight_cp_0p5
        wt_cp_mm = events.TauSpinner.weight_cp_0p25

    pairs["wt_cp_sm"] = wt_cp_sm
    pairs["wt_cp_ps"] = wt_cp_ps
    pairs["wt_cp_mm"] = wt_cp_mm

    return pairs
