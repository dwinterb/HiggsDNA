import awkward
import logging
from higgs_dna.utils.kinematic_utilities import calculate_cartesian_p4, convert_cartesian_to_p4

logger = logging.getLogger(__name__)


def add_gen_information(pairs: awkward.Array, events: awkward.Array, channel: str) -> awkward.Array:
    """
    Add generator-level information to particle pairs.

    This method extracts and processes generator-level particle information from the `events` input.
    Specifically, it identifies decay products of leptonic W, Z, or H boson decays and tau neutrinos,
    calculates their four-momentum vectors, and adds this information to the `pairs` array.

    Args:
        events (awkward.Array): An array containing event-level data, including generator-level particles.
        pairs (awkward.Array): An array of particle pairs to which the generator-level information will be added.

    Returns:
        awkward.Array: The updated `pairs` array with additional fields for the generator-level information.
    """

    logger.debug("Adding generic gen information info")
    selected_events = events

    genParts = selected_events.GenPart

    # ----------------------------------------------
    # Define cuts to select generator-level particles:

    # get all decay products of leptonic W, Z, or H decays
    gen_cuts = ((abs(genParts.pdgId) >= 11) & (abs(genParts.pdgId) <= 16) & (genParts.statusFlags & 256 != 0) & (genParts.status == 1)) | (genParts.statusFlags & 1024 != 0)

    # gen neutrinos from tau decays
    nus_gen_cuts = ((abs(genParts.pdgId) == 12) | (abs(genParts.pdgId) == 14) | (abs(genParts.pdgId) == 16)) & (genParts.status == 1) & (genParts.statusFlags & 1024 != 0)

    # get visible decay products (pdgId 11, 13, 15)
    gen_vis_cuts = gen_cuts & ~nus_gen_cuts

    # top quarks for top pt reweighting
    gen_top_cuts = ((abs(genParts.pdgId) == 6) & (genParts.statusFlags & 256 != 0) & (genParts.statusFlags & 8192 != 0))
    pairs['gen_top_pT'] = genParts[gen_top_cuts].pt

    # ----------------------------------------------

    Boson_decayProds = genParts[gen_cuts]
    Boson_visible_decayProds = genParts[gen_vis_cuts]
    Tau_neutrinoProds = genParts[nus_gen_cuts]

    Boson_p4 = convert_cartesian_to_p4(awkward.sum(calculate_cartesian_p4(Boson_decayProds),axis=-1))
    Boson_visible_p4 = convert_cartesian_to_p4(awkward.sum(calculate_cartesian_p4(Boson_visible_decayProds),axis=-1))
    Tau_neutrinos_p4 = convert_cartesian_to_p4(awkward.sum(calculate_cartesian_p4(Tau_neutrinoProds),axis=-1))

    pairs['gen_boson_pT'] = Boson_p4.pt
    pairs['gen_boson_mass'] = Boson_p4.mass
    pairs['gen_boson_eta'] = Boson_p4.eta
    pairs['gen_boson_phi'] = Boson_p4.phi
    pairs['gen_boson_visible_pT'] = Boson_visible_p4.pt
    pairs['gen_boson_visible_mass'] = Boson_visible_p4.mass
    pairs['gen_boson_visible_eta'] = Boson_visible_p4.eta
    pairs['gen_boson_visible_phi'] = Boson_visible_p4.phi
    pairs['gen_taunus_pT'] = Tau_neutrinos_p4.pt
    pairs['gen_taunus_phi'] = Tau_neutrinos_p4.phi

    pairs["genPartFlav_1"] = pairs["obj_1"].genPartFlav
    pairs["genPartFlav_2"] = pairs["obj_2"].genPartFlav
    genpart_idx_1 = awkward.Array(pairs["obj_1"].genPartIdx)
    genpart_idx_2 = awkward.Array(pairs["obj_2"].genPartIdx)

    default_value = -9999.
    dummy_record_genPart = awkward.Record({'eta': default_value, 'mass': default_value, 'phi': default_value, 'pt': default_value, 'genPartIdxMother': default_value, 'pdgId': default_value, 'status': default_value, 'statusFlags': default_value, 'vx': default_value, 'vy': default_value, 'vz':default_value, 'genPartIdxMotherG': default_value, 'distinctParentIdxG': default_value, 'childrenIdxG': [], 'distinctChildrenIdxG': [], 'distinctChildrenDeepIdxG': []})
    dummy_genPart = awkward.Array([{'eta': default_value, 'mass': default_value, 'phi': default_value, 'pt': default_value, 'genPartIdxMother': default_value, 'pdgId': default_value, 'status': default_value, 'statusFlags': default_value, 'vx': default_value, 'vy': default_value, 'vz':default_value, 'genPartIdxMotherG': default_value, 'distinctParentIdxG': default_value, 'childrenIdxG': [], 'distinctChildrenIdxG': [], 'distinctChildrenDeepIdxG': []}] * len(pairs['obj_1'].matched_gen))
    genpart_1 = awkward.where(awkward.is_none(pairs['obj_1'].matched_gen), dummy_genPart, pairs['obj_1'].matched_gen)
    genpart_2 = awkward.where(awkward.is_none(pairs['obj_2'].matched_gen), dummy_genPart, pairs['obj_2'].matched_gen)

    dummy_record_genVisTau = awkward.Record({'eta': default_value, 'mass': default_value, 'phi': default_value, 'pt': default_value, 'charge': default_value, 'genPartIdxMother': -1, 'status': default_value, 'genPartIdxMotherG': default_value})
    genVisTau = selected_events.GenVisTau
    genvistau_1 = genVisTau[awkward.local_index(genVisTau) == genpart_idx_1[:, None]]
    genvistau_1 = awkward.where(awkward.num(genvistau_1, axis=1) == 0, [[dummy_record_genVisTau]], genvistau_1)
    genvistau_2 = genVisTau[awkward.local_index(genVisTau) == genpart_idx_2[:, None]]
    genvistau_2 = awkward.where(awkward.num(genvistau_2, axis=1) == 0, [[dummy_record_genVisTau]], genvistau_2)

    genvistau_idxMother_1 = awkward.flatten(awkward.Array(genvistau_1.genPartIdxMother))
    genvistau_idxMother_2 = awkward.flatten(awkward.Array(genvistau_2.genPartIdxMother))

    gentau_1 = genParts[awkward.local_index(genParts, axis=1) == genvistau_idxMother_1[:, None]]
    gentau_1 = awkward.where(awkward.num(gentau_1, axis=1) == 0, [[dummy_record_genPart]], gentau_1)
    gentau_2 = genParts[awkward.local_index(genParts, axis=1) == genvistau_idxMother_2[:, None]]
    gentau_2 = awkward.where(awkward.num(gentau_2, axis=1) == 0, [[dummy_record_genPart]], gentau_2)

    channel_array = awkward.Array([channel] * len(pairs['obj_1'].matched_gen))
    condition_1 = (pairs['obj_1'].genPartFlav == 5) & (channel_array == "tt")
    condition_2 = (pairs['obj_2'].genPartFlav == 5) & ((channel_array == "tt") | (channel_array == "et") | (channel_array == "mt"))

    pairs["genPart_eta_1"] = awkward.where(condition_1, awkward.flatten(gentau_1.eta), genpart_1.eta)
    pairs["genPart_eta_2"] = awkward.where(condition_2, awkward.flatten(gentau_2.eta), genpart_2.eta)
    pairs["genPart_pdgId_1"] = awkward.where(condition_1, awkward.flatten(gentau_1.pdgId), genpart_1.pdgId)
    pairs["genPart_pdgId_2"] = awkward.where(condition_2, awkward.flatten(gentau_2.pdgId), genpart_2.pdgId)
    pairs["genPart_phi_1"] = awkward.where(condition_1, awkward.flatten(gentau_1.phi), genpart_1.phi)
    pairs["genPart_phi_2"] = awkward.where(condition_2, awkward.flatten(gentau_2.phi), genpart_2.phi)
    pairs["genPart_pt_1"] = awkward.where(condition_1, awkward.flatten(gentau_1.pt), genpart_1.pt)
    pairs["genPart_pt_2"] = awkward.where(condition_2, awkward.flatten(gentau_2.pt), genpart_2.pt)

    pairs["genVisTau_eta_1"] = awkward.where(condition_1, awkward.flatten(genvistau_1.eta), default_value)
    pairs["genVisTau_eta_2"] = awkward.where(condition_2, awkward.flatten(genvistau_2.eta), default_value)
    pairs["genVisTau_mass_1"] = awkward.where(condition_1, awkward.flatten(genvistau_1.mass), default_value)
    pairs["genVisTau_mass_2"] = awkward.where(condition_2, awkward.flatten(genvistau_2.mass), default_value)
    pairs["genVisTau_phi_1"] = awkward.where(condition_1, awkward.flatten(genvistau_1.phi), default_value)
    pairs["genVisTau_phi_2"] = awkward.where(condition_2, awkward.flatten(genvistau_2.phi), default_value)
    pairs["genVisTau_pt_1"] = awkward.where(condition_1, awkward.flatten(genvistau_1.pt), default_value)
    pairs["genVisTau_pt_2"] = awkward.where(condition_2, awkward.flatten(genvistau_2.pt), default_value)

    pairs['gen_decayMode_1'] = awkward.where(condition_1, awkward.flatten(genvistau_1.status), -1)
    pairs['gen_decayMode_2'] = awkward.where(condition_2, awkward.flatten(genvistau_2.status), -1)

    # TODO: FIX
    # This only works et, mt, and tt (Only Pions (DM0)):
    genDaughters_1 = genParts[genParts.genPartIdxMother == genvistau_idxMother_1[:, None]]
    genDaughters_1 = awkward.where(awkward.num(genDaughters_1, axis=1) == 0, [[dummy_record_genPart]], genDaughters_1)
    genDaughters_2 = genParts[genParts.genPartIdxMother == genvistau_idxMother_2[:, None]]
    genDaughters_2 = awkward.where(awkward.num(genDaughters_2, axis=1) == 0, [[dummy_record_genPart]], genDaughters_2)
    charged_pion_cut_1 = abs(genDaughters_1.pdgId) == 211
    genDaughter_1 = awkward.where(charged_pion_cut_1, genDaughters_1, dummy_genPart)
    genDaughter_1 = awkward.firsts(genDaughter_1[awkward.argsort(genDaughter_1.pdgId, axis=1, ascending=False)])
    charged_pion_cut_2 = abs(genDaughters_2.pdgId) == 211
    genDaughter_2 = awkward.where(charged_pion_cut_2, genDaughters_2, dummy_genPart)
    genDaughter_2 = awkward.firsts(genDaughter_2[awkward.argsort(genDaughter_2.pdgId, axis=1, ascending=False)])

    pairs["genDaughter_eta_1"] = awkward.where(condition_1, genDaughter_1.eta, genpart_1.eta)
    pairs["genDaughter_eta_2"] = awkward.where(condition_2, genDaughter_2.eta, genpart_1.eta)
    pairs["genDaughter_pdgId_1"] = awkward.where(condition_1, genDaughter_1.pdgId, genpart_1.pdgId)
    pairs["genDaughter_pdgId_2"] = awkward.where(condition_2, genDaughter_2.pdgId, genpart_1.pdgId)
    pairs["genDaughter_phi_1"] = awkward.where(condition_1, genDaughter_1.phi, genpart_1.phi)
    pairs["genDaughter_phi_2"] = awkward.where(condition_2, genDaughter_2.phi, genpart_1.phi)
    pairs["genDaughter_pt_1"] = awkward.where(condition_1, genDaughter_1.pt, genpart_1.pt)
    pairs["genDaughter_pt_2"] = awkward.where(condition_2, genDaughter_2.pt, genpart_1.pt)

    pairs['genDaughter_vx_1'] = awkward.where(condition_1, genDaughter_1.vx, genpart_1.vx)
    pairs['genDaughter_vy_1'] = awkward.where(condition_1, genDaughter_1.vy, genpart_1.vy)
    pairs['genDaughter_vz_1'] = awkward.where(condition_1, genDaughter_1.vz, genpart_1.vz)
    pairs['genDaughter_vx_2'] = awkward.where(condition_2, genDaughter_2.vx, genpart_2.vx)
    pairs['genDaughter_vy_2'] = awkward.where(condition_2, genDaughter_2.vy, genpart_2.vy)
    pairs['genDaughter_vz_2'] = awkward.where(condition_2, genDaughter_2.vz, genpart_2.vz)

    return pairs


def add_genVertex_information(pairs: awkward.Array, events: awkward.Array) -> awkward.Array:

    logger.debug("Adding gen vertex information")

    pairs['gen_Vtx_x'] = events.GenVtx.x
    pairs['gen_Vtx_y'] = events.GenVtx.y
    pairs['gen_Vtx_z'] = events.GenVtx.z

    return pairs
