import awkward
import numpy
import logging
from higgs_dna.utils.misc_utils import choose_jet
from higgs_dna.utils.kinematic_utilities import calculate_delta_phi, calculate_transverse_mass, calculate_transverse_momentum
import warnings

logger = logging.getLogger(__name__)


def add_pair_information(pairs: awkward.Array) -> awkward.Array:
    """
    Adds quantities to pairs of objects and returns the modified pairs.
    Args:
        pairs (awkward.Array): An Awkward Array containing pairs of objects.
    Returns:
        awkward.Array: The modified pairs with additional quantities.
    - The pairs are transformed into candidates with four momenta and additional properties.
    The modified pairs are returned as an Awkward Array with the name 'PtEtaPhiMCandidate'.
    """

    logger.debug("Adding ditau pair quantities")
    # turn the pairs into candidates with four momenta and such
    pair_4mom = pairs["obj_1"] + pairs["obj_2"]
    pairs["pt"] = pair_4mom.pt
    pairs["eta"] = pair_4mom.eta
    pairs["phi"] = pair_4mom.phi
    pairs["mass"] = pair_4mom.mass
    pairs["charge"] = pair_4mom.charge
    pairs["os"] = pair_4mom.charge == 0
    pairs["dR"] = pairs["obj_1"].delta_r(pairs["obj_2"])
    pairs = awkward.with_name(pairs, "PtEtaPhiMCandidate")
    pairs['dphi'] = calculate_delta_phi(pairs["obj_1"].phi,pairs["obj_2"].phi)

    return pairs


def add_met_information(pairs: awkward.Array, met: awkward.Array) -> awkward.Array:
    """
    Adds MET-related quantities to pairs of objects and returns the modified pairs.
    Args:
        pairs (awkward.Array): An Awkward Array containing pairs of objects.
        met (awkward.Array): An Awkward Array containing the MET.
    Returns:
        awkward.Array: The modified pairs with additional MET-related quantities.
    """

    logger.debug("Adding MET quantities")

    # met related quantities
    pairs["met_pt"] = met.pt
    pairs["met_phi"] = met.phi
    pairs["met_covXX"] = met.covXX
    pairs["met_covYY"] = met.covYY
    pairs["met_covXY"] = met.covXY
    pairs["met_dphi_1"] = calculate_delta_phi(pairs["obj_1"].phi,met.phi)
    pairs["met_dphi_2"] = calculate_delta_phi(pairs["obj_2"].phi,met.phi)
    pairs['mt_1'] = calculate_transverse_mass(pairs["obj_1"],met)
    pairs['mt_2'] = calculate_transverse_mass(pairs["obj_2"],met)
    pairs['mt_lep'] = calculate_transverse_mass(pairs["obj_1"],pairs["obj_2"])
    pairs['mt_tot'] = numpy.sqrt(pairs.mt_1**2 + pairs.mt_2**2 + pairs.mt_lep**2)
    pairs['pt_vis'] = calculate_transverse_momentum(pairs["obj_1"],pairs["obj_2"])
    pairs['pt_tt'] = calculate_transverse_momentum(pairs,met)
    pairs['phi_vis'] = (pairs["obj_1"] + pairs["obj_2"]).phi
    pairs['eta_vis'] = (pairs["obj_1"] + pairs["obj_2"]).eta

    return pairs


def add_jet_information(pairs: awkward.Array, jets: awkward.Array, seeding_string: str) -> awkward.Array:
    """
    Adds jet-related quantities to pairs of objects and returns the modified pairs.
    Args:
        pairs (awkward.Array): An Awkward Array containing pairs of objects.
        jets (awkward.Array): An Awkward Array containing the jets.
        seeding_string (str): The string to use for seeding jets.
    Returns:
        awkward.Array: The modified pairs with additional jet-related quantities.
    """

    # Suppress warnings associated with non-sense LorentzVectors (As advised by coffea devs)
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=RuntimeWarning)

        logger.debug(f"Adding {seeding_string}jet quantities")

        # ensure jets are ordered by pT
        jets = jets[awkward.argsort(jets.pt, ascending=False)]

        # set charge of all jets to 0 as it is not defined
        jets["charge"] = awkward.zeros_like(jets.pt)

        default_value = -9999.

        lead_jet = awkward.zip(
            {
                "pt": choose_jet(jets.pt, 0, default_value),
                "eta": choose_jet(jets.eta, 0, default_value),
                "phi": choose_jet(jets.phi, 0, default_value),
                "mass": choose_jet(jets.mass, 0, default_value),
                "charge":choose_jet(jets.charge, 0, default_value),
            }
        )
        lead_jet = awkward.with_name(lead_jet, "PtEtaPhiMCandidate")

        sublead_jet = awkward.zip(
            {
                "pt": choose_jet(jets.pt, 1, default_value),
                "eta": choose_jet(jets.eta, 1, default_value),
                "phi": choose_jet(jets.phi, 1, default_value),
                "mass": choose_jet(jets.mass, 1, default_value),
                "charge":choose_jet(jets.charge, 1, default_value),
            }
        )
        sublead_jet = awkward.with_name(sublead_jet, "PtEtaPhiMCandidate")

        pairs[f'{seeding_string}jet_1'] = lead_jet
        pairs[f'{seeding_string}jet_2'] = sublead_jet

        dijet = lead_jet + sublead_jet
        dijet = awkward.with_name(dijet, "Candidate")

        pairs[f"{seeding_string}jpt_1"] = lead_jet.pt
        pairs[f"{seeding_string}jeta_1"] = lead_jet.eta
        pairs[f"{seeding_string}jphi_1"] = lead_jet.phi
        pairs[f"{seeding_string}jpt_2"] = sublead_jet.pt
        pairs[f"{seeding_string}jeta_2"] = sublead_jet.eta
        pairs[f"{seeding_string}jphi_2"] = sublead_jet.phi
        pairs[f"{seeding_string}n_jets"] = awkward.num(jets)

        pairs[f"{seeding_string}dijetpt"] = awkward.where(
            pairs[f"{seeding_string}n_jets"] >= 2,
            awkward.where((lead_jet.pt == default_value) | (sublead_jet.pt == default_value), default_value, dijet.pt),
            default_value
        )

        pairs[f"{seeding_string}mjj"] = awkward.where(
            pairs[f"{seeding_string}n_jets"] >= 2,
            awkward.where((lead_jet.pt == default_value) | (sublead_jet.pt == default_value), default_value, dijet.mass),
            default_value
        )

        pairs[f"{seeding_string}jdeta"] = awkward.where(
            pairs[f"{seeding_string}n_jets"] >= 2,
            awkward.where((lead_jet.pt == default_value) | (sublead_jet.pt == default_value), default_value, numpy.abs(pairs[f"{seeding_string}jeta_1"] - pairs[f"{seeding_string}jeta_2"])),
            default_value)

        # signed jet delta phi, where sign is defined by eta ordering of the jets
        sjdphi = awkward.where(lead_jet.eta >= sublead_jet.eta, calculate_delta_phi(lead_jet.phi,sublead_jet.phi), calculate_delta_phi(sublead_jet.phi,lead_jet.phi))
        pairs[f"{seeding_string}sjdphi"] = awkward.where(
            pairs[f"{seeding_string}n_jets"] >= 2,
            awkward.where((lead_jet.pt == default_value) | (sublead_jet.pt == default_value), default_value, sjdphi),
            default_value)

    return pairs


def add_generic_information(pairs: awkward.Array) -> awkward.Array:
    """
    Adds generic quantities to pairs of objects and returns the modified pairs.
    Args:
        pairs (awkward.Array): An Awkward Array containing pairs of objects.
    Returns:
        awkward.Array: The modified pairs with additional generic quantities.
    """

    logger.debug("Adding generic quantities")

    default_value = -9999.

    pairs["pion_E_split_1"] = numpy.where((pairs['pi0_Energy_1'] < 0) | (pairs['pi_Energy_1'] < 0), default_value, numpy.abs((pairs['pi0_Energy_1'] - pairs['pi_Energy_1']) / (pairs['pi0_Energy_1'] + pairs['pi_Energy_1'])))
    pairs["pion_E_split_2"] = numpy.where((pairs['pi0_Energy_2'] < 0) | (pairs['pi_Energy_2'] < 0), default_value, numpy.abs((pairs['pi0_Energy_2'] - pairs['pi_Energy_2']) / (pairs['pi0_Energy_2'] + pairs['pi_Energy_2'])))

    return pairs
