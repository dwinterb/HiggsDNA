import awkward
import numpy as np
import logging

logger = logging.getLogger(__name__)


def tauEnergyScale_shift(channel: str, met: awkward.Array, pairs: awkward.Array) -> awkward.Array:

    logger.info("Applying Tau Energy Scale shift to MET")

    corrected_met_pt = met["pt"]
    corrected_met_phi = met["phi"]
    corrected_met_px = met["pt"] * np.cos(met["phi"])
    corrected_met_py = met["pt"] * np.sin(met["phi"])

    for index in range(len(channel)):
        pnumber = index + 1
        if channel[index] == "t":
            tau = pairs[f'obj_{pnumber}']

            # calculate the met shift
            delta_tau_px = tau["EnergyScale_Shift_px"]
            delta_tau_py = tau["EnergyScale_Shift_py"]

            corrected_met_px = corrected_met_px - delta_tau_px
            corrected_met_py = corrected_met_py - delta_tau_py

            corrected_met_pt = np.sqrt(corrected_met_px**2 + corrected_met_py**2)
            corrected_met_phi = np.arctan2(corrected_met_py, corrected_met_px)

    met["pt"] = corrected_met_pt
    met["phi"] = corrected_met_phi

    return met


def jetEnergyResolution_shift(jets: awkward.Array, met: awkward.Array, variation: str) -> awkward.Array:

    logger.info("Applying Jet Energy Resolution shift to MET")
    uncorrected_met_px = met["pt"] * np.cos(met["phi"])
    uncorrected_met_py = met["pt"] * np.sin(met["phi"])

    # if the variation is JER or JEC, use the shifted jet px and py for the corresponding variation
    if "jer" in variation or "jec" in variation:
        delta_jet_px = jets[f"Jet_Shift_px_{variation}"]
        delta_jet_py = jets[f"Jet_Shift_py_{variation}"]
    # otherwise, use the nominally shited jet px and py
    else:
        delta_jet_px = jets["Jet_Shift_px_nominal"]
        delta_jet_py = jets["Jet_Shift_py_nominal"]

    # sum the jet shifts per event
    total_delta_jet_px = awkward.sum(delta_jet_px, axis=1)
    total_delta_jet_py = awkward.sum(delta_jet_py, axis=1)

    corrected_met_px = uncorrected_met_px - total_delta_jet_px
    corrected_met_py = uncorrected_met_py - total_delta_jet_py

    corrected_met_pt = np.sqrt(corrected_met_px**2 + corrected_met_py**2)
    corrected_met_phi = np.arctan2(corrected_met_py, corrected_met_px)

    met["pt"] = corrected_met_pt
    met["phi"] = corrected_met_phi

    return met
