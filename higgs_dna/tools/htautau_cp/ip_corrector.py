import uproot
import numpy as np
import re
import awkward
from scipy.ndimage import gaussian_filter1d
import warnings
import os
import logging

logger = logging.getLogger(__name__)


def load_histograms(file_name, ip_names):

    file = uproot.open(file_name)
    if not file:
        raise FileNotFoundError(f"IpCorrection: file {file_name} does not exist or is corrupted")

    # Retrieve histogram names and strip any ";1" suffix
    hist_names = [name.split(";")[0] for name in file.keys()]

    # Extract eta bin names using regex and process them
    eta_pattern = re.compile(r"(\d+p\d+to\d+p\d+)")
    eta_names = sorted(set(eta_pattern.search(name).group(1) for name in hist_names if eta_pattern.search(name)))

    # Convert eta bin names to ranges and construct the eta_ranges list
    eta_bin_edges = sorted([name.replace("p", ".").replace("to", "-") for name in eta_names])
    eta_ranges = [float(edge.split("-")[0]) for edge in eta_bin_edges]
    eta_ranges.append(float(eta_bin_edges[-1].split("-")[1]))  # Append upper edge of last bin

    # Initialize empty dictionaries to store histograms by category
    hist_ip_data = {ip_name: {eta_name: None for eta_name in eta_names} for ip_name in ip_names}
    hist_ip_mc = {ip_name: {eta_name: None for eta_name in eta_names} for ip_name in ip_names}

    # Helper function to load histograms as (counts, edges) numpy arrays using uproot
    def load_hist_as_numpy(hist_name):
        if hist_name in file:
            hist = file[hist_name]
            counts = hist.values()
            edges = hist.axis().edges()
            return counts, edges
        else:
            print(f"Warning: histogram {hist_name} does not exist")
            return None

    # Load and convert IP histograms
    for ip_name in ip_names:
        for eta_name in eta_names:
            hist_ip_data[ip_name][eta_name] = load_hist_as_numpy(f"{ip_name}_{eta_name}_data")
            hist_ip_mc[ip_name][eta_name] = load_hist_as_numpy(f"{ip_name}_{eta_name}_MC")

    return {
        "eta_names": eta_names,
        "eta_ranges": eta_ranges,
        "hist_ip_data": hist_ip_data,
        "hist_ip_mc": hist_ip_mc
    }


def correct_IP(hist_data, ip_x, ip_y, ip_z, gen_ip_x, gen_ip_y, gen_ip_z, eta, genmask):

    def apply_mapping(var, gen_var, component, hist_mc, hist_data, eta_bins, eta_names, genmask):
        shifted_var = var - gen_var

        mc_counts = np.array([hist_mc[component][eta_name][0] for eta_name in eta_names])  # Shape: (n_eta_bins, n_bins)
        mc_edges = np.array([hist_mc[component][eta_name][1] for eta_name in eta_names])  # Shape: (n_eta_bins, n_bins + 1)
        data_counts = np.array([hist_data[component][eta_name][0] for eta_name in eta_names])  # Shape: (n_eta_bins, n_bins)
        data_edges = np.array([hist_data[component][eta_name][1] for eta_name in eta_names])  # Shape: (n_eta_bins, n_bins + 1)

        corrected_var_dict = {}

        for index in range(len(eta_names)):
            corrected_var_dict[str(index)] = applyQuantileMapping_numpy(
                mc_counts[index], mc_edges[index],
                data_counts[index], data_edges[index],
                shifted_var, genmask
            )

        corrected_var = np.zeros_like(var)
        for index in range(len(eta_names)):
            corrected_var = np.where(eta_bins == index, corrected_var_dict[str(index)], corrected_var)

        return corrected_var + gen_var

    # Determine eta bins for all events
    absEta = np.abs(eta)
    eta_bins = np.digitize(absEta, bins=hist_data["eta_ranges"]) - 1
    eta_bins = np.clip(eta_bins, 0, len(hist_data["eta_names"]) - 1)
    eta_names = hist_data["eta_names"]

    # Correct IP values
    ip_x_corr = apply_mapping(ip_x, gen_ip_x, "ip_x", hist_data["hist_ip_mc"], hist_data["hist_ip_data"], eta_bins, eta_names, genmask)
    ip_y_corr = apply_mapping(ip_y, gen_ip_y, "ip_y", hist_data["hist_ip_mc"], hist_data["hist_ip_data"], eta_bins, eta_names, genmask)
    ip_z_corr = apply_mapping(ip_z, gen_ip_z, "ip_z", hist_data["hist_ip_mc"], hist_data["hist_ip_data"], eta_bins, eta_names, genmask)

    return ip_x_corr, ip_y_corr, ip_z_corr


def applyQuantileMapping_numpy(hist_MC_counts, hist_MC_edges, hist_data_counts, hist_data_edges, var, genmask):
    """
    Perform quantile mapping for event-level data with pre-assigned histograms,
    now with smoothed CDFs and cubic interpolation for more accurate correction.
    """

    hist_MC_counts_smooth = gaussian_filter1d(hist_MC_counts, sigma=1)
    hist_data_counts_smooth = gaussian_filter1d(hist_data_counts, sigma=1)

    hist_MC_counts_smooth = hist_MC_counts
    hist_data_counts_smooth = hist_data_counts

    # Recompute normalized cumulative sums with smoothed histograms
    normMC = np.sum(hist_MC_counts_smooth)
    cumsumMC = np.cumsum(hist_MC_counts_smooth)
    normData = np.sum(hist_data_counts_smooth)
    cumsumData = np.cumsum(hist_data_counts_smooth)

    xmin = hist_MC_edges[0]
    xmax = hist_MC_edges[-1]

    # Check for out-of-bounds values or genmask is not True
    out_of_bounds = (var < xmin) | (var > xmax) | np.isnan(var) | ~genmask
    corrected_var = np.copy(var)

    # --------------------------- MC ---------------------------
    # Find the bin in MC histogram for each event
    nBinMC = np.digitize(var, bins=hist_MC_edges) - 1
    nBinMC = np.clip(nBinMC, 0, len(hist_MC_counts_smooth) - 1)

    integral_lower_MC = cumsumMC[nBinMC - 1] / normMC
    integral_upper_MC = cumsumMC[np.clip(nBinMC, 0, len(hist_MC_edges) - 2)] / normMC
    integral_difference_MC = integral_upper_MC - integral_lower_MC

    binMC_x_lower = hist_MC_edges[nBinMC]
    bin_width_MC = hist_MC_edges[np.clip(nBinMC + 1, 0, len(hist_MC_edges) - 1)] - hist_MC_edges[nBinMC]

    integral_center = integral_lower_MC + integral_difference_MC * (var - binMC_x_lower) / bin_width_MC

    # --------------------------- Data ---------------------------
    # Find the bin in data histogram for each event that matches the integral_center
    nBinData = np.searchsorted(cumsumData / normData, integral_center)
    nBinData = np.clip(nBinData, 0, len(hist_data_counts_smooth) - 1)

    integral_lower_Data = (cumsumData[nBinData - 1]) / normData
    integral_upper_Data = cumsumData[np.clip(nBinData, 0, len(hist_data_edges) - 2)] / normData
    integral_difference_Data = integral_upper_Data - integral_lower_Data

    binData_x_lower = hist_data_edges[nBinData]
    bin_width_Data = hist_data_edges[np.clip(nBinData + 1, 0, len(hist_data_edges) - 1)] - hist_data_edges[nBinData]

    # -----------------------------------------------------------
    varrcorr = binData_x_lower + (integral_center - integral_lower_Data) * bin_width_Data / integral_difference_Data
    corrected_var = np.where(out_of_bounds, var, varrcorr)
    return corrected_var


def calculate_IPSig(ipx, ipy, ipz, ip_cov):

    ip_vectors = np.vstack((ipx, ipy, ipz)).T
    ip_magnitudes = np.linalg.norm(ip_vectors, axis=1)
    ip_normalized = ip_vectors / ip_magnitudes[:, np.newaxis]

    error_ip = np.sqrt(np.einsum('ni,nij,nj->n', ip_normalized, ip_cov, ip_normalized))

    ip_significance = ip_magnitudes / error_ip

    return ip_significance


def apply_IP_Corrections(pairs: awkward.Array, channel: str, year: str):
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=RuntimeWarning)

        higgs_dna_path = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

        ip_names = ["ip_x", "ip_y", "ip_z"]

        input_path_ee = os.path.join(higgs_dna_path, f"systematics/ditau/ROOT/IP_Corrections/{year}/ip_corrections_ee.root")
        input_path_mm = os.path.join(higgs_dna_path, f"systematics/ditau/ROOT/IP_Corrections/{year}/ip_corrections_mm.root")

        if not os.path.exists(input_path_ee) or not os.path.exists(input_path_mm):
            logger.warning(f"IP correction file not found for channel {channel} in year {year}")
            return pairs

        logger.debug(f"Applying IP corrections for channel {channel} using file {input_path_ee} & {input_path_mm}")

        hist_data_dict = {
            "1": load_histograms(input_path_ee, ip_names),
            "2": load_histograms(input_path_mm, ip_names),
        }

        def compute_ip_corrections(obj_index):
            """Compute IP corrections for a given object index."""
            ip_x = np.array(pairs[f"ip_x_{obj_index}"])
            ip_y = np.array(pairs[f"ip_y_{obj_index}"])
            ip_z = np.array(pairs[f"ip_z_{obj_index}"])
            gen_ip_x = np.array(pairs[f"genIP_{obj_index}_x"])
            gen_ip_y = np.array(pairs[f"genIP_{obj_index}_y"])
            gen_ip_z = np.array(pairs[f"genIP_{obj_index}_z"])
            eta = np.array(pairs[f'obj_{obj_index}'].eta)
            genPartFlav = np.array(pairs[f'obj_{obj_index}'].genPartFlav)

            def apply_corrections(hist_data, genmask):
                """Helper to apply corrections with a mask."""
                return correct_IP(hist_data, ip_x, ip_y, ip_z, gen_ip_x, gen_ip_y, gen_ip_z, eta, genmask)

            default_values = [
                ip_x, ip_y, ip_z,
            ]

            if channel == "ee":
                genmask = (genPartFlav == 1)
                hist_data = hist_data_dict["1"]
                return apply_corrections(hist_data, genmask)

            elif channel == "mm":
                genmask = (genPartFlav == 1)
                hist_data = hist_data_dict["2"]
                return apply_corrections(hist_data, genmask)

            elif channel == "et" and obj_index == 1:
                genmask = (genPartFlav == 1) | (genPartFlav == 15)
                hist_data = hist_data_dict["1"]
                return apply_corrections(hist_data, genmask)

            elif channel == "et" and obj_index != 1:
                genmask_1_3 = (genPartFlav == 1) | (genPartFlav == 3)
                genmask_2_4_5 = (genPartFlav == 2) | (genPartFlav == 4) | (genPartFlav == 5)
                corrected_1_3 = apply_corrections(hist_data_dict["1"], genmask_1_3)
                corrected_2_4_5 = apply_corrections(hist_data_dict["2"], genmask_2_4_5)
                return [
                    np.where(genmask_1_3, corrected_1_3[i],
                             np.where(genmask_2_4_5, corrected_2_4_5[i], default_values[i]))
                    for i in range(len(default_values))
                ]

            elif channel == "mt" and obj_index == 1:
                genmask = (genPartFlav == 1) | (genPartFlav == 15)
                hist_data = hist_data_dict["2"]
                return apply_corrections(hist_data, genmask)

            elif channel == "mt" and obj_index != 1:
                genmask_2_4_5 = (genPartFlav == 2) | (genPartFlav == 4) | (genPartFlav == 5)
                return [
                    np.where(genmask_2_4_5,
                             apply_corrections(hist_data_dict["2"], genmask_2_4_5)[i], default_values[i])
                    for i in range(len(default_values))
                ]

            elif channel == "tt":
                genmask_1_3 = (genPartFlav == 1) | (genPartFlav == 3)
                genmask_2_4_5 = (genPartFlav == 2) | (genPartFlav == 4) | (genPartFlav == 5)
                corrected_1_3 = apply_corrections(hist_data_dict["1"], genmask_1_3)
                corrected_2_4_5 = apply_corrections(hist_data_dict["2"], genmask_2_4_5)
                return [
                    np.where(genmask_1_3, corrected_1_3[i],
                             np.where(genmask_2_4_5, corrected_2_4_5[i], default_values[i]))
                    for i in range(len(default_values))
                ]

        # Apply corrections for both objects
        for obj_index in [1, 2]:
            corrections = compute_ip_corrections(obj_index)
            (ip_x_corr, ip_y_corr, ip_z_corr) = corrections

            pairs[f"ip_x_{obj_index}"] = ip_x_corr
            pairs[f"ip_y_{obj_index}"] = ip_y_corr
            pairs[f"ip_z_{obj_index}"] = ip_z_corr

    return pairs
