import awkward
import coffea
import numpy
import logging

logger = logging.getLogger(__name__)


def add_ipCalibration_information(pairs: awkward.Array, events: awkward.Array) -> awkward.Array:
    logger.debug("Adding quantities required for IP Calibration")
    gen_Vtx = awkward.zip({"x": pairs.gen_Vtx_x, "y": pairs.gen_Vtx_y, "z": pairs.gen_Vtx_z}, with_name="ThreeVector", behavior=coffea.nanoevents.methods.vector.behavior)

    genPart_vx_1 = awkward.zip({"x": pairs.genDaughter_vx_1, "y": pairs.genDaughter_vy_1, "z": pairs.genDaughter_vz_1}, with_name="ThreeVector", behavior=coffea.nanoevents.methods.vector.behavior)
    genPart_vx_2 = awkward.zip({"x": pairs.genDaughter_vx_2, "y": pairs.genDaughter_vy_2, "z": pairs.genDaughter_vz_2}, with_name="ThreeVector", behavior=coffea.nanoevents.methods.vector.behavior)

    genPart_1 = convert_PtEtaPhi_to_PxPyPz(pairs.genDaughter_pt_1, pairs.genDaughter_eta_1, pairs.genDaughter_phi_1)
    genPart_2 = convert_PtEtaPhi_to_PxPyPz(pairs.genDaughter_pt_2, pairs.genDaughter_eta_2, pairs.genDaughter_phi_2)

    flight_direction_1 = genPart_1.unit
    flight_direction_2 = genPart_2.unit

    distance_1 = genPart_vx_1 - gen_Vtx
    distance_2 = genPart_vx_2 - gen_Vtx

    projection_1 = distance_1.dot(flight_direction_1)
    projection_2 = distance_2.dot(flight_direction_2)

    IP_vector_1 = distance_1 - projection_1 * flight_direction_1
    IP_vector_2 = distance_2 - projection_2 * flight_direction_2

    pairs['genIP_1_x'] = IP_vector_1.x
    pairs['genIP_1_y'] = IP_vector_1.y
    pairs['genIP_1_z'] = IP_vector_1.z

    pairs['genIP_2_x'] = IP_vector_2.x
    pairs['genIP_2_y'] = IP_vector_2.y
    pairs['genIP_2_z'] = IP_vector_2.z

    pairs['PVBS_cov00'] = awkward.flatten(events.PVBS.cov00)
    pairs['PVBS_cov10'] = awkward.flatten(events.PVBS.cov10)
    pairs['PVBS_cov11'] = awkward.flatten(events.PVBS.cov11)
    pairs['PVBS_cov20'] = awkward.flatten(events.PVBS.cov20)
    pairs['PVBS_cov21'] = awkward.flatten(events.PVBS.cov21)
    pairs['PVBS_cov22'] = awkward.flatten(events.PVBS.cov22)

    pairs['GenVsReco_PVBS_dxy'] = numpy.sqrt((pairs.gen_Vtx_x - pairs.PVBS_x)**2 + (pairs.gen_Vtx_y - pairs.PVBS_y)**2)
    pairs['GenVsReco_PVBS_dz'] = numpy.abs(pairs.gen_Vtx_z - pairs.PVBS_z)

    pairs['GenVsReco_PV_dxy'] = numpy.sqrt((pairs.gen_Vtx_x - pairs.PV_x)**2 + (pairs.gen_Vtx_y - pairs.PV_y)**2)
    pairs['GenVsReco_PV_dz'] = numpy.abs(pairs.gen_Vtx_z - pairs.PV_z)

    return pairs


def convert_PtEtaPhi_to_PxPyPz(pt: awkward.Array, eta: awkward.Array, phi: awkward.Array) -> awkward.Array:
    mask = (pt == -9999.0) | (eta == -9999.0) | (phi == -9999.0)
    pt = awkward.where(mask, 0, pt)
    eta = awkward.where(mask, 0, eta)
    phi = awkward.where(mask, 0, phi)

    px = pt * numpy.cos(phi)
    py = pt * numpy.sin(phi)
    pz = pt * numpy.sinh(eta)

    px = awkward.where(mask, -9999.0, px)
    py = awkward.where(mask, -9999.0, py)
    pz = awkward.where(mask, -9999.0, pz)

    return awkward.zip({"x": px, "y": py, "z": pz}, with_name="ThreeVector", behavior=coffea.nanoevents.methods.vector.behavior)
