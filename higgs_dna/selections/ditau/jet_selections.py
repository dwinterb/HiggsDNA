import awkward
import correctionlib
import os
from coffea.analysis_tools import PackedSelection
from copy import deepcopy
import numpy as np
from correctionlib.highlevel import model_auto, open_auto
import json
import logging

logger = logging.getLogger(__name__)


def apply_jetID(jets: awkward.highlevel.Array, nanoVersion: str) -> awkward.highlevel.Array:
    if nanoVersion in ["v13","v14"]:
        abs_eta = abs(jets.eta)
        passJetIdTight = awkward.where(
            abs_eta <= 2.6,
            (jets.neHEF < 0.99)
            & (jets.neEmEF < 0.9)
            & (jets.chMultiplicity + jets.neMultiplicity > 1)
            & (jets.chHEF > 0.01)
            & (jets.chMultiplicity > 0),  # Tight criteria for abs_eta <= 2.6
            awkward.where(
                (abs_eta > 2.6) & (abs_eta <= 2.7),
                (jets.neHEF < 0.9) & (jets.neEmEF < 0.99),  # Tight criteria for 2.6 < abs_eta <= 2.7
                awkward.where(
                    (abs_eta > 2.7) & (abs_eta <= 3.0),
                    jets.neHEF < 0.99,  # Tight criteria for 2.7 < abs_eta <= 3.0
                    (jets.neMultiplicity >= 2) & (jets.neEmEF < 0.4)  # Tight criteria for abs_eta > 3.0
                )
            )
        )

        # Default tight lepton veto
        passJetIdTightLepVeto = awkward.where(
            abs_eta <= 2.7,
            passJetIdTight & (jets.muEF < 0.8) & (jets.chEmEF < 0.8),  # add lepton veto for abs_eta <= 2.7
            passJetIdTight  # No lepton veto for 2.7 < abs_eta
        )

    else:
        passJetIdTightLepVeto = (jets.jetId & 3) != 0

    return passJetIdTightLepVeto


def select_jets_eta_dependent(jets: awkward.highlevel.Array, jet_pt_thresholds: list, jet_eta_thresholds: list, nanoVersion: str) -> awkward.highlevel.Array:
    """
    Selects jets from the input `jets` array based on specified criteria. The pT cuts applied are dependent on the jet eta.
    Args:
        jets (awkward.highlevel.Array): The input array containing jet objects.
        jet_pt_thresholds (list): The minimum transverse momentum thresholds for jet selection in each eta bin (specified in the jet_eta_thresholds).
        jet_eta_thresholds (float): The absolute rapidity thresholds for jet selection, which should correspond to the pt thresholds specified in jet_pt_thresholds.
    Returns:
        awkward.highlevel.Array: The selected jets satisfying the given criteria.
    Example:
        jets = ...  # Initialize your awkward.highlevel.Array of jets
        selected_jets = select_ejst(jets, jet_pt_threshold=[20.0,50.0,30.0], jet_max_eta=[2.5,3.0,4.7])
    Note:
        - The provided `jets` should be an `awkward.highlevel.Array` containing jet objects.
        - The function returns the jets that satisfy all the specified selection criteria.
    """

    if len(jet_pt_thresholds) != len(jet_eta_thresholds):
        raise Exception("The number of pt thresholds should match the number of eta thresholds.")

    # Initialize a mask to select jets satisfying all eta-dependent thresholds
    mask = awkward.zeros_like(jets.pt, dtype=bool)

    id_cut = apply_jetID(jets, nanoVersion)

    # Apply pT and eta thresholds for each bin
    for i in range(len(jet_pt_thresholds)):
        pt_threshold = jet_pt_thresholds[i]
        eta_threshold = jet_eta_thresholds[i]

        # Update the mask for jets within the current eta range
        if i == 0:
            # First bin: eta < eta_threshold
            mask = mask | ((abs(jets.eta) < eta_threshold) & (jets.pt >= pt_threshold))
        else:
            # Intermediate bins: eta in (previous_threshold, current_threshold)
            mask = mask | ((abs(jets.eta) >= jet_eta_thresholds[i - 1]) & (abs(jets.eta) < eta_threshold) & (jets.pt >= pt_threshold))

    mask = mask & id_cut

    return jets[mask]


def select_jets(jets: awkward.highlevel.Array, jet_pt_threshold: float, jet_max_eta: float, nanoVersion: str) -> awkward.highlevel.Array:
    """
    Selects jets from the input `jets` array based on specified criteria.
    Args:
        jets (awkward.highlevel.Array): The input array containing jet objects.
        jet_pt_threshold (float): The minimum transverse momentum threshold for jet selection.
        jet_max_eta (float): The maximum absolute rapidity for jet selection.
    Returns:
        awkward.highlevel.Array: The selected jets satisfying the given criteria.
    Example:
        jets = ...  # Initialize your awkward.highlevel.Array of jets
        selected_jets = select_ejst(jets, jet_pt_threshold=30.0, jet_max_eta=4.7)
    Note:
        - The provided `jets` should be an `awkward.highlevel.Array` containing jet objects.
        - The function applies selection criteria on the `jets` array based on the following conditions:
            - Transverse momentum (pt) greater than `jet_pt_threshold`.
            - Pseudorapidity (eta) within the range (-`jet_max_eta`, +`jet_max_eta`).
        - The function returns the jets that satisfy all the specified selection criteria.
    """
    pt_cut = jets.pt > jet_pt_threshold
    eta_cut = abs(jets.eta) < abs(jet_max_eta)
    # apply the tight jet ID
    id_cut = apply_jetID(jets, nanoVersion)
    return jets[pt_cut & eta_cut & id_cut]


def jetvetomap(events, logger, dataset_name, year="2022preEE"):
    """
    Jet veto map
    """
    systematic = "jetvetomap"
    sel_obj = PackedSelection()

    json_dict = {
        "2016preVFP": os.path.join(
            os.path.dirname(__file__),
            "../../systematics/ditau/JSONs/JME/2016preVFP_UL/jetvetomaps.json.gz",
        ),
        "2016postVFP": os.path.join(
            os.path.dirname(__file__),
            "../../systematics/ditau/JSONs/JME/2016postVFP_UL/jetvetomaps.json.gz",
        ),
        "2017": os.path.join(
            os.path.dirname(__file__),
            "../../systematics/ditau/JSONs/JME/2017_UL/jetvetomaps.json.gz",
        ),
        "2018": os.path.join(
            os.path.dirname(__file__),
            "../../systematics/ditau/JSONs/JME/2018_UL/jetvetomaps.json.gz",
        ),
        "2022preEE": os.path.join(
            os.path.dirname(__file__),
            "../../systematics/ditau/JSONs/JME/2022_Summer22/jetvetomaps.json.gz",
        ),
        "2022postEE": os.path.join(
            os.path.dirname(__file__),
            "../../systematics/ditau/JSONs/JME/2022_Summer22EE/jetvetomaps.json.gz",
        ),
        "2023": os.path.join(
            os.path.dirname(__file__),
            "../../systematics/ditau/JSONs/JME/2023_Summer23/jetvetomaps.json.gz",
        ),
        "2023BPix": os.path.join(
            os.path.dirname(__file__),
            "../../systematics/ditau/JSONs/JME/2023_Summer23BPix/jetvetomaps.json.gz",
        ),
    }

    year_mapping = {
        "Run2_2016_HIPM": ("2016preVFP"),
        "Run2_2016": ("2016postVFP"),
        "Run2_2017": ("2017"),
        "Run2_2018": ("2018"),
        "Run3_2022": ("2022preEE"),
        "Run3_2022EE": ("2022postEE"),
        "Run3_2023": ("2023"),
        "Run3_2023BPix": ("2023BPix"),
    }

    year_ = year_mapping.get(year, (None))

    key_map = {
        "2016preVFP": "Summer19UL16_V1",
        "2016postVFP": "Summer19UL16_V1",
        "2017": "Summer19UL17_V1",
        "2018": "Summer19UL18_V1",
        "2022preEE": "Summer22_23Sep2023_RunCD_V1",
        "2022postEE": "Summer22EE_23Sep2023_RunEFG_V1",
        "2023": "Summer23Prompt23_RunC_V1",
        "2023BPix": "Summer23BPixPrompt23_RunD_V1",
    }

    logger.debug(
        f"[{systematic}] {key_map[year_]}, year: {year}"
    )

    year = year_

    # Edge check of input variables. The eta and phi variables don't enable flow
    # https://cms-nanoaod-integration.web.cern.ch/commonJSONSFs/summaries/JME_2022_Prompt_jetvetomaps.html
    _cset = model_auto(open_auto(json_dict[year]))
    _cset_json = json.loads(_cset.json())
    low_eta, high_eta = (
        _cset_json["corrections"][0]["data"]["content"][0]["value"]["edges"][0][0],
        _cset_json["corrections"][0]["data"]["content"][0]["value"]["edges"][0][-1],
    )
    # phi value must be within [-np.pi,np.pi]. Though values beyond are observed.
    # Might due to the accuracy of nanoaod format. So clip the values to be within the first and last bin centers
    low_phi, high_phi = (
        (
            _cset_json["corrections"][0]["data"]["content"][0]["value"]["edges"][1][0]
            + _cset_json["corrections"][0]["data"]["content"][0]["value"]["edges"][1][1]
        )
        / 2,
        (
            _cset_json["corrections"][0]["data"]["content"][0]["value"]["edges"][1][-1]
            + _cset_json["corrections"][0]["data"]["content"][0]["value"]["edges"][1][
                -2
            ]
        )
        / 2,
    )
    jets_jagged = deepcopy(events.Jet)
    # remove jets out of bin edges
    # https://cms-nanoaod-integration.web.cern.ch/commonJSONSFs/summaries/JME_2022_Prompt_jetvetomaps.html
    jets_jagged = jets_jagged[
        (jets_jagged.eta >= low_eta) & (jets_jagged.eta < high_eta)
    ]
    count = awkward.num(jets_jagged)
    jets = awkward.flatten(jets_jagged)

    cset = correctionlib.CorrectionSet.from_file(json_dict[year])

    # ref: https://twiki.cern.ch/twiki/bin/viewauth/CMS/PdmVRun3Analysis#From_JME
    # and: https://cms-talk.web.cern.ch/t/jet-veto-maps-for-run3/57850/6

    input_dict = {
        "type": "jetvetomap",
        "eta": jets.eta,
        "phi": np.clip(jets.phi, low_phi, high_phi),
    }

    input_dict["type"] = "jetvetomap"
    inputs = [input_dict[input.name] for input in cset[key_map[year]].inputs]
    vetomap = cset[key_map[year]].evaluate(*(inputs))
    flag_veto_jet = (np.abs(vetomap) > 0) & ((jets.pt > 15) & ((jets.jetId == 2) | (jets.jetId == 6)) & ((jets.chEmEF + jets.neEmEF) < 0.9) & (jets.muonIdx1 == -1) & (jets.muonIdx2 == -1))
    sel_obj.add("vetomap", (np.abs(vetomap) > 0) | (flag_veto_jet))

    sel_veto_jet = sel_obj.all(*(sel_obj.names))
    sel_good_jet = ~awkward.Array(sel_veto_jet)
    logger.debug(
        f"[{systematic}] total jets: {len(sel_good_jet)}, pass jets: {awkward.sum(sel_good_jet)}"
    )
    sel_good_jet_jagged = awkward.unflatten(sel_good_jet, count)
    flag_veto_jet_jagged = awkward.unflatten(flag_veto_jet, count)

    sel_event_veto = ~awkward.any(flag_veto_jet_jagged, axis=1)

    # Apply the veto mask, preserving all fields
    filtered_events = events[sel_event_veto]

    filtered_events["Jet"] = (jets_jagged[sel_good_jet_jagged])[sel_event_veto]

    logger.debug(
        f"[{systematic}] total events: {len(sel_event_veto)}, pass events: {awkward.sum(sel_event_veto)}"
    )
    return filtered_events
