import awkward


def select_muons(muons: awkward.highlevel.Array, muon_pt_threshold: float, muon_max_eta: float) -> awkward.highlevel.Array:
    """
    Selects muons from the input `muons` array based on specified criteria.
    Args:
        muons (awkward.highlevel.Array): The input array containing muon objects.
        muon_pt_threshold (float): The minimum transverse momentum threshold for muon selection.
        muon_max_eta (float): The maximum absolute rapidity for muon selection.
    Returns:
        awkward.highlevel.Array: The selected muons satisfying the given criteria.
    Example:
        muons = ...  # Initialize your awkward.highlevel.Array of muons
        selected_muons = select_muons(muons, muon_pt_threshold=10.0, muon_max_eta=2.4)
    Note:
        - The provided `muons` should be an `awkward.highlevel.Array` containing muon objects.
        - The function applies selection criteria on the `muons` array based on the following conditions:
            - Transverse momentum (pt) greater than `muon_pt_threshold`.
            - Pseudorapidity (eta) within the range (-`muon_max_eta`, +`muon_max_eta`).
            - Medium identification (mediumId) requirement.
            - Particle flow relative isolation (pfRelIso04_all) less than 0.3.
            - Transverse impact parameter (dz) less than 0.2.
            - Longitudinal impact parameter (dxy) less than 0.045.
        - The function returns the muons that satisfy all the specified selection criteria.
    """
    pt_cut = muons.pt > muon_pt_threshold
    eta_cut = abs(muons.eta) < abs(muon_max_eta)
    id_cut = muons.mediumId
    iso_cut = muons.pfRelIso04_all < 0.3
    dz_cut = abs(muons.dz) < 0.2
    dxy_cut = abs(muons.dxy) < 0.045
    return muons[pt_cut & eta_cut & id_cut & iso_cut & dz_cut & dxy_cut]


def select_electrons(electrons: awkward.highlevel.Array, electron_pt_threshold: float, electron_max_eta: float, electron_id : str) -> awkward.highlevel.Array:
    """
    Selects electrons from the input `electrons` array based on specified criteria.
    Args:
        electrons (awkward.highlevel.Array): The input array containing electron objects.
        electron_pt_threshold (float): The minimum transverse momentum threshold for electron selection.
        electron_max_eta (float): The maximum absolute rapidity for electron selection.
        electron_id (str): The name of the electron ID discriminator to use.
    Returns:
        awkward.highlevel.Array: The selected electrons satisfying the given criteria.
    Example:
        electrons = ...  # Initialize your awkward.highlevel.Array of electrons
        selected_electrons = select_electrons(electrons, electron_pt_threshold=10.0, electron_max_eta=2.5, electron_id='mvaNoIso_WP90')
        Recommended IDs are mvaNoIso_WP90 for Run-3 or mvaFall17V1Iso_WP90 for Run-2
    Note:
        - The provided `electrons` should be an `awkward.highlevel.Array` containing electron objects.
        - The function applies selection criteria on the `electrons` array based on the following conditions:
            - Transverse momentum (pt) greater than `electron_pt_threshold`.
            - Pseudorapidity (eta) within the range (-`electron_max_eta`, +`electron_max_eta`).
            - identification requirement -> electron should pass the ID discriminator `electron_id`.
            - Particle flow relative isolation (pfRelIso04_all) less than 0.3.
            - Transverse impact parameter (dz) less than 0.2.
            - Longitudinal impact parameter (dxy) less than 0.045.
        - The function returns the electrons that satisfy all the specified selection criteria.
    """
    pt_cut = electrons.pt > electron_pt_threshold
    eta_cut = abs(electrons.eta) < abs(electron_max_eta)
    id_cut = electrons[electron_id]
    iso_cut = electrons.miniPFRelIso_all < 0.3
    dz_cut = abs(electrons.dz) < 0.2
    dxy_cut = abs(electrons.dxy) < 0.045
    return electrons[pt_cut & eta_cut & id_cut & iso_cut & dz_cut & dxy_cut]


def select_taus(taus: awkward.highlevel.Array, tau_pt_threshold: float, tau_max_eta: float, tau_id : str) -> awkward.highlevel.Array:
    """
    Selects taus from the input `taus` array based on specified criteria.
    Args:
        taus (awkward.highlevel.Array): The input array containing tau objects.
        tau_pt_threshold (float): The minimum transverse momentum threshold for tau selection.
        tau_max_eta (float): The maximum absolute rapidity for tau selection.
        tau_id (str): The name of the tau ID discriminator to use.
    Returns:
        awkward.highlevel.Array: The selected taus satisfying the given criteria.
    Example:
        taus = ...  # Initialize your awkward.highlevel.Array of taus
        selected_taus = select_taus(taus, tau_pt_threshold=18.0, tau_max_eta=2.3, tau_id='DeepTau2018v2p5')
    Note:
        - The provided `taus` should be an `awkward.highlevel.Array` containing tau objects.
        - The function applies selection criteria on the `taus` array based on the following conditions:
            - Transverse momentum (pt) greater than `tau_pt_threshold`.
            - Pseudorapidity (eta) within the range (-`tau_max_eta`, +`tau_max_eta`).
            - identification requirement -> tau should pass the loosest working points of the ID discriminator `tau_id`.
            - Transverse impact parameter (dz) less than 0.2.
        - The function returns the taus that satisfy all the specified selection criteria.
    """
    pt_cut = taus.pt > tau_pt_threshold
    eta_cut = abs(taus.eta) < abs(tau_max_eta)
    # apply the loosest working points for jet and muon discriminators, and the second loosest electron discriminator
    # as we are using the DeepTau ID we also veto dm 5 and 6, when the code is updated to use particleNet we may want to remove this requirement
    id_cut = (taus[f"id{tau_id}VSjet"] > 0) & (taus[f"id{tau_id}VSmu"] > 0) & (taus[f"id{tau_id}VSe"] > 1) & (taus.decayMode != 5) & (taus.decayMode != 6)
    dz_cut = abs(taus.dz) < 0.2
    return taus[pt_cut & eta_cut & dz_cut & id_cut]
