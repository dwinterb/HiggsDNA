import numpy as np
import os
import correctionlib
import awkward as ak
import logging

logger = logging.getLogger(__name__)


def Trigger(pair, channel, weights, year, meta, is_correction=True, **kwargs):
    # Mapping from year names to folder names and JSON names
    year_mapping = {
        "Run2_2016_HIPM": ("2016preVFP_UL", "UL2016_preVFP"),
        "Run2_2016": ("2016postVFP_UL", "UL2016_postVFP"),
        "Run2_2017": ("2017_UL", "UL2017"),
        "Run2_2018": ("2018_UL", "UL2018"),
        "Run3_2022": ("2022_Summer22", "2022preEE"),
        "Run3_2022EE": ("2022_Summer22EE", "2022postEE"),
        "Run3_2023": ("2023_Summer23", "2023preBPix"),
        "Run3_2023BPix": ("2023_Summer23BPix", "2023postBPix"),
    }

    analysis = meta.get("Analysis", "Default")

    folder_name, file_name = year_mapping.get(year, (None, None))
    Muon_WP = meta.get("Muon_ID_WP")
    Tau_WP = meta["Selections"].get(channel, {}).get("Tau_ID_VSjet_WP", "Tight")
    Electron_WP = "Tight"
    muon_json_name = f"NUM_IsoMu24_DEN_CutBasedId{Muon_WP}_and_PFIsoMedium"
    electron_json_name = "Electron-HLT-SF"
    tau_json_name = "tauTriggerSF"
    mutau_xtrg_json_name = f"NUM_IsoMu20_DEN_CutBasedId{Muon_WP}_and_PFIsoMedium"
    etau_xtrg_json_name = "Electron-HLT-SF"
    jet_json_name = "jetlegSFs"

    electron_json_mapping = {
        "2022_Summer22": ("2022Re-recoBCD"),
        "2022_Summer22EE": ("2022Re-recoE+PromptFG"),
        "2023_Summer23": ("2023PromptC"),
        "2023_Summer23BPix": ("2023PromptD"),
    }

    if folder_name:
        try:
            path_to_muon_json = os.path.join(os.path.dirname(__file__), f"JSONs/Muon/{folder_name}/muon_Z.json.gz")
            muon_evaluator = correctionlib.CorrectionSet.from_file(path_to_muon_json)[muon_json_name]
        except Exception as e:
            raise ValueError(f"Failed to load muon JSON for year {year} from path {path_to_muon_json}: {e}")

        try:
            path_to_electron_json = os.path.join(os.path.dirname(__file__), f"JSONs/EGamma/{folder_name}/electronHlt.json.gz")
            electron_evaluator = correctionlib.CorrectionSet.from_file(path_to_electron_json)[electron_json_name]
        except Exception as e:
            raise ValueError(f"Failed to load electron JSON for year {year} from path {path_to_electron_json}: {e}")

        try:
            path_to_tau_json = os.path.join(os.path.dirname(__file__), f"JSONs/Tau_Trigger/{folder_name}/tau_trigger.json.gz")
            if analysis == "CP":
                path_to_tau_json = os.path.join(os.path.dirname(__file__), f"JSONs/Tau_Trigger/CP/tau_trigger_DeepTau2018v2p5_{file_name}.json.gz")
            tau_evaluator = correctionlib.CorrectionSet.from_file(path_to_tau_json)[tau_json_name]
        except Exception as e:
            raise ValueError(f"Failed to load tau JSON for year {year} from path {path_to_tau_json}: {e}")

        try:
            path_to_mutau_json = os.path.join(os.path.dirname(__file__), f"JSONs/CrossTriggers/{folder_name}/CrossMuTauHlt.json.gz")
            mutau_evaluator = correctionlib.CorrectionSet.from_file(path_to_mutau_json)[mutau_xtrg_json_name]
        except Exception as e:
            raise ValueError(f"Failed to load mu-tau cross trigger JSON for year {year} from path {path_to_mutau_json}: {e}")

        try:
            path_to_etau_json = os.path.join(os.path.dirname(__file__), f"JSONs/CrossTriggers/{folder_name}/CrossEleTauHlt.json.gz")
            etau_evaluator = correctionlib.CorrectionSet.from_file(path_to_etau_json)[etau_xtrg_json_name]
        except Exception as e:
            raise ValueError(f"Failed to load e-tau cross trigger JSON for year {year} from path {path_to_etau_json}: {e}")

        try:
            path_to_jetleg_json = os.path.join(os.path.dirname(__file__), f"JSONs/Tau_Trigger/{folder_name}/ditaujet_jetleg_SFs.json.gz")
            jetleg_evaluator = correctionlib.CorrectionSet.from_file(path_to_jetleg_json)[jet_json_name]
        except Exception as e:
            raise ValueError(f"Failed to load ditau+jet jet leg JSON for year {year} from path {path_to_jetleg_json}: {e}")
    else:
        raise ValueError(f"Year {year} not supported for triggers.")

    sf = np.ones(len(weights._weight))
    sf_doubletau = np.ones(len(weights._weight))
    sf_doubletaujet = np.ones(len(weights._weight))

    if channel == "mt":
        pt_1_clipped_mu_trg = np.clip(ak.to_numpy(pair['obj_1'].pt), 26.0, np.inf)
        pt_1_clipped_mu_xtrg = np.clip(ak.to_numpy(pair['obj_1'].pt), 20.0, np.inf)
        pt_2_clipped = np.clip(ak.to_numpy(pair['obj_2'].pt), 25.0, np.inf)
        eta_1_clipped_mu_xtrg = np.clip(ak.to_numpy(pair['obj_1'].eta), -2.1, 2.1)

        decay_mode = pair['obj_2'].decayMode
        pnet_decay_mode = pair['obj_2'].decayModePNet

        if analysis == "CP":
            dm = pnet_decay_mode
        else:
            dm = decay_mode

        sf_mu_dict = {}
        sf_mu_xtrg_dict = {}
        sf_tau_xtrg_dict = {}
        sf_dict = {}

        if is_correction:
            variations = ["nominal"]
        else:
            variations = ["nominal", "up", "down"]

        for var in variations:
            if var == "nominal":
                mu_trg = muon_evaluator.evaluate(
                    np.abs(pair['obj_1'].eta), pt_1_clipped_mu_trg, var
                )
                mu_xtrg = np.where(
                    abs(ak.to_numpy(pair['obj_1'].eta)) <= 2.1,
                    mutau_evaluator.evaluate(
                        np.abs(eta_1_clipped_mu_xtrg), pt_1_clipped_mu_xtrg, var
                    ),
                    np.ones(len(weights._weight))
                )

                tau_trg = tau_evaluator.evaluate(
                    pt_2_clipped, dm, "mutau", f"{Tau_WP}","sf", "nom"
                )

            else:
                mu_trg = muon_evaluator.evaluate(
                    np.abs(pair['obj_1'].eta), pt_1_clipped_mu_trg, 'syst' + var
                )
                mu_xtrg = np.where(
                    abs(ak.to_numpy(pair['obj_1'].eta)) <= 2.1,
                    mutau_evaluator.evaluate(
                        np.abs(eta_1_clipped_mu_xtrg), pt_1_clipped_mu_xtrg, 'syst' + var
                    ),
                    np.ones(len(weights._weight))
                )
                tau_trg = tau_evaluator.evaluate(
                    pt_2_clipped, dm, "mutau", f"{Tau_WP}","sf", var
                )

            sf = np.where(
                pair['obj_1'].pt >= 26.0,
                mu_trg,
                mu_xtrg * tau_trg
            )

            sf_mu_dict[var] = mu_trg
            sf_mu_xtrg_dict[var] = mu_xtrg
            sf_tau_xtrg_dict[var] = tau_trg
            sf_dict[var] = sf

        if not is_correction:
            sf_mu_dict["up"] = sf_mu_dict["up"] / sf_mu_dict["nominal"]
            sf_mu_dict["down"] = sf_mu_dict["down"] / sf_mu_dict["nominal"]
            sf_mu_xtrg_dict["up"] = sf_mu_xtrg_dict["up"] / sf_mu_xtrg_dict["nominal"]
            sf_mu_xtrg_dict["down"] = sf_mu_xtrg_dict["down"] / sf_mu_xtrg_dict["nominal"]
            sf_tau_xtrg_dict["up"] = sf_tau_xtrg_dict["up"] / sf_tau_xtrg_dict["nominal"]
            sf_tau_xtrg_dict["down"] = sf_tau_xtrg_dict["down"] / sf_tau_xtrg_dict["nominal"]
            sf_dict["up"] = sf_dict["up"] / sf_dict["nominal"]
            sf_dict["down"] = sf_dict["down"] / sf_dict["nominal"]

            weights.add(name="Trigger", weight=sf, weightUp=sf_dict["up"], weightDown=sf_dict["down"])

        else:
            weights.add(name="Trigger", weight=sf_dict["nominal"], weightUp=np.ones(len(weights._weight)), weightDown=np.ones(len(weights._weight)))
            weights.add(name="SingleMuon_leg1", weight=sf_mu_dict["nominal"], weightUp=np.ones(len(weights._weight)), weightDown=np.ones(len(weights._weight)))
            weights.add(name="CrossTrigger_Muon_leg1", weight=sf_mu_xtrg_dict["nominal"], weightUp=np.ones(len(weights._weight)), weightDown=np.ones(len(weights._weight)))
            weights.add(name="CrossTrigger_Tau_leg2", weight=sf_tau_xtrg_dict["nominal"], weightUp=np.ones(len(weights._weight)), weightDown=np.ones(len(weights._weight)))

    elif channel == "mm":
        pt_1_clipped_mu_trg = np.clip(ak.to_numpy(pair['obj_1'].pt), 26.0, np.inf)

        sf_dict = {}

        if is_correction:
            variations = ["nominal"]
        else:
            variations = ["nominal", "up", "down"]

        for var in variations:
            if var == "nominal":
                sf_lead = muon_evaluator.evaluate(
                    np.abs(pair['obj_1'].eta), pt_1_clipped_mu_trg, var
                )
            else:
                sf_lead = muon_evaluator.evaluate(
                    np.abs(pair['obj_1'].eta), pt_1_clipped_mu_trg, 'syst' + var
                )
            sf_lead = np.where(
                pair['obj_1'].pt >= 26.0,
                sf_lead,
                np.ones(len(weights._weight))
            )

            sf_dict[var] = sf_lead

        if not is_correction:
            sf_dict["up"] = sf_dict["up"] / sf_dict["nominal"]
            sf_dict["down"] = sf_dict["down"] / sf_dict["nominal"]

            weights.add(name="Trigger", weight=sf_dict["nominal"], weightUp=sf_dict["up"], weightDown=sf_dict["down"])
        else:
            weights.add(name="Trigger", weight=sf_dict["nominal"], weightUp=np.ones(len(weights._weight)), weightDown=np.ones(len(weights._weight)))

    elif channel == "et":
        pt_1_clipped_ele_trg = np.clip(ak.to_numpy(pair['obj_1'].pt), 25.0, np.inf)
        pt_1_clipped_ele_xtrg = np.clip(ak.to_numpy(pair['obj_1'].pt), 25.0, np.inf)
        eta_1_clipped_ele_xtrg = np.clip(ak.to_numpy(pair['obj_1'].eta), -2.1, 2.1)
        pt_2_clipped = np.clip(ak.to_numpy(pair['obj_2'].pt), 25.0, np.inf)

        decay_mode = pair['obj_2'].decayMode
        pnet_decay_mode = pair['obj_2'].decayModePNet

        if analysis == "CP":
            dm = pnet_decay_mode
        else:
            dm = decay_mode

        sf_e_dict = {}
        sf_e_xtrg_dict = {}
        sf_tau_xtrg_dict = {}
        sf_dict = {}

        if is_correction:
            variations = ["nominal"]
        else:
            variations = ["nominal", "up", "down"]

        for var in variations:
            if var == "nominal":
                ele_trg = electron_evaluator.evaluate(
                    electron_json_mapping.get(folder_name, (None, None)), "sf", f"HLT_SF_Ele30_{Electron_WP}ID", pair['obj_1'].eta, pt_1_clipped_ele_trg
                )
                ele_xtrg = etau_evaluator.evaluate(
                    electron_json_mapping.get(folder_name, (None, None)), "sf", f"HLT_SF_Ele24_{Electron_WP}ID", eta_1_clipped_ele_xtrg, pt_1_clipped_ele_xtrg
                )
                ele_xtrg = np.where(
                    ((abs(pair['obj_1'].eta)) <= 2.1),
                    ele_xtrg,
                    1.0
                )
                tau_trg = tau_evaluator.evaluate(
                    pt_2_clipped, dm, "etau", f"{Tau_WP}","sf", "nom"
                )
            else:
                ele_trg = electron_evaluator.evaluate(
                    electron_json_mapping.get(folder_name, (None, None)), "sf" + var, f"HLT_SF_Ele30_{Electron_WP}ID", pair['obj_1'].eta, pt_1_clipped_ele_trg
                )
                ele_xtrg = etau_evaluator.evaluate(
                    electron_json_mapping.get(folder_name, (None, None)), "sf" + var, f"HLT_SF_Ele24_{Electron_WP}ID", eta_1_clipped_ele_xtrg, pt_1_clipped_ele_xtrg
                )
                ele_xtrg = np.where(
                    ((abs(pair['obj_1'].eta)) <= 2.1),
                    ele_xtrg,
                    1.0
                )
                tau_trg = tau_evaluator.evaluate(
                    pt_2_clipped, dm, "etau", f"{Tau_WP}","sf", var
                )

            sf = np.where(
                pair['obj_1'].pt >= 31.0,
                ele_trg,
                ele_xtrg * tau_trg
            )

            sf_e_dict[var] = ele_trg
            sf_e_xtrg_dict[var] = ele_xtrg
            sf_tau_xtrg_dict[var] = tau_trg
            sf_dict[var] = sf

        if not is_correction:
            sf_e_dict["up"] = sf_e_dict["up"] / sf_e_dict["nominal"]
            sf_e_dict["down"] = sf_e_dict["down"] / sf_e_dict["nominal"]
            sf_e_xtrg_dict["up"] = sf_e_xtrg_dict["up"] / sf_e_xtrg_dict["nominal"]
            sf_e_xtrg_dict["down"] = sf_e_xtrg_dict["down"] / sf_e_xtrg_dict["nominal"]
            sf_tau_xtrg_dict["up"] = sf_tau_xtrg_dict["up"] / sf_tau_xtrg_dict["nominal"]
            sf_tau_xtrg_dict["down"] = sf_tau_xtrg_dict["down"] / sf_tau_xtrg_dict["nominal"]
            sf_dict["up"] = sf_dict["up"] / sf_dict["nominal"]
            sf_dict["down"] = sf_dict["down"] / sf_dict["nominal"]

            weights.add(name="Trigger", weight=sf_dict["nominal"], weightUp=sf_dict["up"], weightDown=sf_dict["down"])

        else:
            weights.add(name="Trigger", weight=sf_dict["nominal"], weightUp=np.ones(len(weights._weight)), weightDown=np.ones(len(weights._weight)))
            weights.add(name="SingleElectron_leg1", weight=sf_e_dict["nominal"], weightUp=np.ones(len(weights._weight)), weightDown=np.ones(len(weights._weight)))
            weights.add(name="CrossTrigger_Electron_leg1", weight=sf_e_xtrg_dict["nominal"], weightUp=np.ones(len(weights._weight)), weightDown=np.ones(len(weights._weight)))
            weights.add(name="CrossTrigger_Tau_leg2", weight=sf_tau_xtrg_dict["nominal"], weightUp=np.ones(len(weights._weight)), weightDown=np.ones(len(weights._weight)))

    elif channel == "ee":
        pt_1_clipped_ele_trg = np.clip(ak.to_numpy(pair['obj_1'].pt), 25.0, np.inf)

        sf_dict = {}

        if is_correction:
            variations = ["nominal"]
        else:
            variations = ["nominal", "up", "down"]

        for var in variations:
            if var == "nominal":
                sf = electron_evaluator.evaluate(
                    electron_json_mapping.get(folder_name, (None, None)), "sf", f"HLT_SF_Ele30_{Electron_WP}ID", pair['obj_1'].eta, pt_1_clipped_ele_trg
                )
            else:
                sf = electron_evaluator.evaluate(
                    electron_json_mapping.get(folder_name, (None, None)), "sf" + var, f"HLT_SF_Ele30_{Electron_WP}ID", pair['obj_1'].eta, pt_1_clipped_ele_trg
                )

            sf = np.where(
                pair['obj_1'].pt >= 31.0,
                sf,
                np.ones(len(weights._weight))
            )

            sf_dict[var] = sf

        if not is_correction:
            sf_dict["up"] = sf_dict["up"] / sf_dict["nominal"]
            sf_dict["down"] = sf_dict["down"] / sf_dict["nominal"]

            weights.add(name="Trigger", weight=sf_dict["nominal"], weightUp=sf_dict["up"], weightDown=sf_dict["down"])

        else:
            weights.add(name="Trigger", weight=sf_dict["nominal"], weightUp=np.ones(len(weights._weight)), weightDown=np.ones(len(weights._weight)))

    elif channel == "tt":
        pt_1_clipped = np.clip(ak.to_numpy(pair['obj_1'].pt), 40.0, np.inf)
        pt_2_clipped = np.clip(ak.to_numpy(pair['obj_2'].pt), 40.0, np.inf)
        pt_jet_clipped = np.clip(ak.to_numpy(pair['jpt_1']), 60.0, np.inf)
        pt_1_doubletaujet_clipped = np.clip(ak.to_numpy(pair['obj_1'].pt), 35.0, np.inf)
        pt_2_doubletaujet_clipped = np.clip(ak.to_numpy(pair['obj_2'].pt), 35.0, np.inf)

        decay_mode_1 = pair['obj_1'].decayMode
        pnet_decay_mode_1 = pair['obj_1'].decayModePNet
        decay_mode_2 = pair['obj_2'].decayMode
        pnet_decay_mode_2 = pair['obj_2'].decayModePNet

        if analysis == "CP":
            dm_1 = pnet_decay_mode_1
            dm_2 = pnet_decay_mode_2
        else:
            dm_1 = decay_mode_1
            dm_2 = decay_mode_2

        condition1 = (pair['jpt_1'] < 60.0) & (pair['obj_1'].pt > 40.0) & (pair['obj_2'].pt > 40.0)
        condition2 = (pair['jpt_1'] > 60.0) & (35.0 < pair['obj_1'].pt) & (pair['obj_1'].pt < 40.0) & (35.0 < pair['obj_2'].pt) & (pair['obj_2'].pt < 40.0)
        condition3 = (pair['jpt_1'] > 60.0) & (pair['obj_1'].pt > 40.0) & (pair['obj_2'].pt > 40.0)

        sf_doubletau_dict = {}
        sf_doubletaujet_dict = {}
        sf_dict = {}

        if is_correction:
            variations = ["nom"]
        else:
            variations = ["nom", "up", "down"]

        for var in variations:
            tau_trg_1_data = tau_evaluator.evaluate(pt_1_clipped, dm_1, "ditau", f"{Tau_WP}","eff_data", var)
            tau_trg_2_data = tau_evaluator.evaluate(pt_2_clipped, dm_2, "ditau", f"{Tau_WP}","eff_data", var)
            jet_trg_data = jetleg_evaluator.evaluate(pt_jet_clipped, np.abs(pair['jeta_1']), var, "data")
            tau_trg_doubletaujet_1_data = tau_evaluator.evaluate(pt_1_doubletaujet_clipped, dm_1, "ditaujet", f"{Tau_WP}","eff_data", var)
            tau_trg_doubletaujet_2_data = tau_evaluator.evaluate(pt_2_doubletaujet_clipped, dm_2, "ditaujet", f"{Tau_WP}","eff_data", var)

            tau_trg_1_mc = tau_evaluator.evaluate(pt_1_clipped, dm_1, "ditau", f"{Tau_WP}","eff_mc", var)
            tau_trg_2_mc = tau_evaluator.evaluate(pt_2_clipped, dm_2, "ditau", f"{Tau_WP}","eff_mc", var)
            jet_trg_mc = jetleg_evaluator.evaluate(pt_jet_clipped, np.abs(pair['jeta_1']), var, "mc")
            tau_trg_doubletaujet_1_mc = tau_evaluator.evaluate(pt_1_doubletaujet_clipped, dm_1, "ditaujet", f"{Tau_WP}","eff_mc", var)
            tau_trg_doubletaujet_2_mc = tau_evaluator.evaluate(pt_2_doubletaujet_clipped, dm_2, "ditaujet", f"{Tau_WP}","eff_mc", var)

            numerator = ak.where(
                condition1,
                tau_trg_1_data * tau_trg_2_data,
                ak.where(
                    condition2,
                    tau_trg_doubletaujet_1_data * tau_trg_doubletaujet_2_data * jet_trg_data,
                    ak.where(
                        condition3,
                        (tau_trg_1_data * tau_trg_2_data) + (tau_trg_doubletaujet_1_data * tau_trg_doubletaujet_2_data * jet_trg_data) - (tau_trg_1_data * tau_trg_2_data * jet_trg_data),
                        np.ones(len(weights._weight))
                    )
                )
            )

            denominator = ak.where(
                condition1,
                tau_trg_1_mc * tau_trg_2_mc,
                ak.where(
                    condition2,
                    tau_trg_doubletaujet_1_mc * tau_trg_doubletaujet_2_mc * jet_trg_mc,
                    ak.where(
                        condition3,
                        (tau_trg_1_mc * tau_trg_2_mc) + (tau_trg_doubletaujet_1_mc * tau_trg_doubletaujet_2_mc * jet_trg_mc) - (tau_trg_1_mc * tau_trg_2_mc * jet_trg_mc),
                        np.ones(len(weights._weight))
                    )
                )
            )

            sf = numerator / denominator

            sf_doubletau = (tau_trg_1_data * tau_trg_2_data) / (tau_trg_1_mc * tau_trg_2_mc)
            sf_doubletaujet = (tau_trg_doubletaujet_1_data * tau_trg_doubletaujet_2_data * jet_trg_data) / (tau_trg_doubletaujet_1_mc * tau_trg_doubletaujet_2_mc * jet_trg_mc)

            sf = np.where(
                pair['obj_1'].pt >= 35.0,
                sf,
                np.ones(len(weights._weight))
            )

            sf_doubletau = np.where(
                pair['obj_1'].pt >= 40.0,
                sf_doubletau,
                np.ones(len(weights._weight))
            )

            sf_doubletaujet = np.where(
                pair['obj_1'].pt >= 35.0,
                sf_doubletaujet,
                np.ones(len(weights._weight))
            )

            sf_dict[var] = sf
            sf_doubletau_dict[var] = sf_doubletau
            sf_doubletaujet_dict[var] = sf_doubletaujet

        if not is_correction:
            sf_doubletau_dict["up"] = sf_doubletau_dict["up"] / sf_doubletau_dict["nom"]
            sf_doubletau_dict["down"] = sf_doubletau_dict["down"] / sf_doubletau_dict["nom"]
            sf_doubletaujet_dict["up"] = sf_doubletaujet_dict["up"] / sf_doubletaujet_dict["nom"]
            sf_doubletaujet_dict["down"] = sf_doubletaujet_dict["down"] / sf_doubletaujet_dict["nom"]
            sf_dict["up"] = sf_dict["up"] / sf_dict["nom"]
            sf_dict["down"] = sf_dict["down"] / sf_dict["nom"]

            weights.add(name="Trigger", weight=sf_dict["nom"], weightUp=sf_dict["up"], weightDown=sf_dict["down"])

        else:

            weights.add(name="Trigger", weight=sf_dict["nom"], weightUp=np.ones(len(weights._weight)), weightDown=np.ones(len(weights._weight)))
            weights.add(name="DoubleTauTrigger", weight=sf_doubletau_dict["nom"], weightUp=np.ones(len(weights._weight)), weightDown=np.ones(len(weights._weight)))
            weights.add(name="DoubleTauJetTrigger", weight=sf_doubletaujet_dict["nom"], weightUp=np.ones(len(weights._weight)), weightDown=np.ones(len(weights._weight)))

    else:
        sf = np.ones(len(weights._weight))
        sfup = np.zeros(len(sf))
        sfdown = np.zeros(len(sf))

        weights.add(name="Trigger", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights
