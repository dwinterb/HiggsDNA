import numpy as np
import awkward as ak
import correctionlib
import os
from copy import deepcopy


def Tau_EnergyScale(events, year, meta, channel, is_correction=True):
    # Mapping from year names to folder names and JSON names
    year_mapping = {
        "Run2_2016_HIPM": ("2016preVFP_UL", "UL2016_preVFP"),
        "Run2_2016": ("2016postVFP_UL", "UL2016_postVFP"),
        "Run2_2017": ("2017_UL", "UL2017"),
        "Run2_2018": ("2018_UL", "UL2018"),
        "Run3_2022": ("2022_preEE", "2022_preEE", "2022_preEE"),
        "Run3_2022EE": ("2022_postEE", "2022_postEE", "2022_postEE"),
        "Run3_2023": ("2023_Summer23", "2023preBPix", "2023_preBPix"),
        "Run3_2023BPix": ("2023_Summer23BPix", "2023postBPix", "2023_postBPix"),
    }

    folder_name, file_name, file_name_CP = year_mapping.get(year, (None, None, None))
    json_name = "tau_energy_scale"
    if folder_name and json_name:
        path_to_json = os.path.join(os.path.dirname(__file__), f"JSONs/TauID/{folder_name}/tau_DeepTau2018v2p5_{file_name}.json.gz")
        evaluator = correctionlib.CorrectionSet.from_file(path_to_json)[json_name]
    else:
        raise ValueError(f"Year {year} not supported for TAU ES.")

    analysis = meta.get("Analysis", "Default")
    if analysis == "CP":
        folder_name_CP = os.path.join(os.path.dirname(__file__), f"JSONs/TauID/CP/tau_es_dm_DeepTau2018v2p5_{file_name_CP}.json.gz")
        evaluator_CP = correctionlib.CorrectionSet.from_file(folder_name_CP)[f"tau_es_dm_DeepTau2018v2p5_{file_name_CP}"]

    name = "DeepTau2018v2p5"
    wp_VSjet = meta["Selections"].get(channel, {}).get("Tau_ID_VSjet_WP", "Tight")
    wp_VSe = meta["Selections"].get(channel, {}).get("Tau_ID_VSe_WP", "Tight")

    counts = ak.num(events.Tau.pt)

    tau_4vec = ak.zip(
        {
            "pt": ak.flatten(events.Tau.pt),
            "eta": ak.flatten(events.Tau.eta),
            "phi": ak.flatten(events.Tau.phi),
            "mass": ak.flatten(events.Tau.mass),
        },
        with_name="PtEtaPhiMLorentzVector",
    )

    mass = ak.flatten(events.Tau.mass)
    dm = ak.flatten(events.Tau.decayMode)
    pnet_dm = ak.flatten(events.Tau.decayModePNet)
    gen = ak.flatten(events.Tau.genPartFlav)

    # Masking the taus with decay modes 5 and 6 to avoid correctionlib crashing
    mask = (dm == 5) | (dm == 6)
    masked_dm = np.where(mask, 0, dm)
    masked_pnet_dm = np.where((pnet_dm == -1), 0, pnet_dm)

    if is_correction:
        correction = evaluator.evaluate(ak.flatten(events.Tau.pt), ak.flatten(events.Tau.eta), masked_dm, gen, name, wp_VSjet, wp_VSe, "nom")

        if analysis == "CP":
            correction_CP = evaluator_CP.evaluate(ak.flatten(events.Tau.pt), ak.flatten(events.Tau.eta), masked_pnet_dm, gen, name, "Vtight", wp_VSe, "nom")
            correction_CP = np.where((pnet_dm == -1), 1.0, correction_CP)
            correction = np.where((gen == 5), correction_CP, correction)

        corrected_px = np.where((dm != 5) | (dm != 6),tau_4vec.px * correction, tau_4vec.px)
        corrected_py = np.where((dm != 5) | (dm != 6),tau_4vec.py * correction, tau_4vec.py)
        corrected_pz = np.where((dm != 5) | (dm != 6),tau_4vec.pz * correction, tau_4vec.pz)
        corrected_M = np.where((dm != 0) | (dm != 5) | (dm != 6),mass * correction, mass)

        corrected_tau_3vec = ak.zip(
            {
                "x": corrected_px,
                "y": corrected_py,
                "z": corrected_pz,
            },
            with_name="Momentum3D",
        )

        corrected_pt = np.sqrt(corrected_tau_3vec.x**2 + corrected_tau_3vec.y**2)
        corrected_eta = corrected_tau_3vec.pseudorapidity
        corrected_phi = corrected_tau_3vec.phi

        corrected_taus = deepcopy(events.Tau)
        corrected_taus["pt"] = ak.unflatten(corrected_pt, counts)
        corrected_taus["eta"] = ak.unflatten(corrected_eta, counts)
        corrected_taus["phi"] = ak.unflatten(corrected_phi, counts)
        corrected_taus["mass"] = ak.unflatten(corrected_M, counts)

        delta_tau_px = corrected_px - tau_4vec.px
        delta_tau_py = corrected_py - tau_4vec.py

        corrected_taus["EnergyScale_Shift_px"] = ak.unflatten(delta_tau_px, counts)
        corrected_taus["EnergyScale_Shift_py"] = ak.unflatten(delta_tau_py, counts)

        events["Tau"] = corrected_taus

        return events

    else:
        # CAREFUL: The nominal correction is already applied (check the format of the workflow) thus, the up and down variations need to be divided by the nominal correction
        corrected_taus = {}
        variations = {
            'TSCALE_0PI': [0, [5]],
            'TSCALE_1PI': [1, [5]],
            'TSCALE_3PRONG': [10, [5]],
            'TSCALE_3PRONG1PI0': [11, [5]],
            'ESCALE_0PI': [0, [1, 3]],
            'ESCALE_1PI': [1, [1, 3]],
            'ESCALE_3PRONG': [10, [1, 3]],
            'ESCALE_3PRONG1PI0': [11, [1, 3]],
            'MUSCALE_0PI': [0, [2, 4]],
            'MUSCALE_1PI': [1, [2, 4]],
            'MUSCALE_3PRONG': [10, [2, 4]],
            'MUSCALE_3PRONG1PI0': [11, [2, 4]],
        }

        for key, requirements in variations.items():
            nominal_correction = evaluator.evaluate(ak.flatten(events.Tau.pt), ak.flatten(events.Tau.eta), masked_dm, gen, name, wp_VSjet, wp_VSe, "nom")
            uncertainty_up = evaluator.evaluate(ak.flatten(events.Tau.pt), ak.flatten(events.Tau.eta), masked_dm, gen, name, wp_VSjet, wp_VSe, "up")
            uncertainty_down = evaluator.evaluate(ak.flatten(events.Tau.pt), ak.flatten(events.Tau.eta), masked_dm, gen, name, wp_VSjet, wp_VSe, "down")

            if analysis == "CP":
                nominal_correction_CP = evaluator_CP.evaluate(ak.flatten(events.Tau.pt), ak.flatten(events.Tau.eta), masked_pnet_dm, gen, name, "Vtight", wp_VSe, "nom")
                uncertainty_up_CP = evaluator_CP.evaluate(ak.flatten(events.Tau.pt), ak.flatten(events.Tau.eta), masked_pnet_dm, gen, name, "Vtight", wp_VSe, "up")
                uncertainty_down_CP = evaluator_CP.evaluate(ak.flatten(events.Tau.pt), ak.flatten(events.Tau.eta), masked_pnet_dm, gen, name, "Vtight", wp_VSe, "down")

                nominal_correction_CP = np.where((pnet_dm == -1), 1.0, nominal_correction_CP)
                uncertainty_up_CP = np.where((pnet_dm == -1), 1.0, uncertainty_up_CP)
                uncertainty_down_CP = np.where((pnet_dm == -1), 1.0, uncertainty_down_CP)

                nominal_correction = np.where((gen == 5), nominal_correction_CP, nominal_correction)
                uncertainty_up = np.where((gen == 5), uncertainty_up_CP, uncertainty_up)
                uncertainty_down = np.where((gen == 5), uncertainty_down_CP, uncertainty_down)

            variation_mask = (gen == requirements[1][0])
            for index,flavour in enumerate(requirements[1]):
                if index == 0:
                    continue
                else:
                    variation_mask = (gen == flavour) | variation_mask

            variation_mask = (dm == requirements[0]) & variation_mask

            # HERE: The correction is already applied, thus we need to divide the up and down variations by the nominal correction
            uncertainty_up = np.where(variation_mask, uncertainty_up / nominal_correction, 1.0)
            uncertainty_down = np.where(variation_mask, uncertainty_down / nominal_correction, 1.0)

            corrected_px_up = np.where((dm != 5) | (dm != 6),tau_4vec.px * uncertainty_up, tau_4vec.px)
            corrected_py_up = np.where((dm != 5) | (dm != 6),tau_4vec.py * uncertainty_up, tau_4vec.py)
            corrected_pz_up = np.where((dm != 5) | (dm != 6),tau_4vec.pz * uncertainty_up, tau_4vec.pz)
            corrected_M_up = np.where((dm != 0) | (dm != 5) | (dm != 6),mass * uncertainty_up, mass)

            corrected_tau_3vec_up = ak.zip(
                {
                    "x": corrected_px_up,
                    "y": corrected_py_up,
                    "z": corrected_pz_up,
                },
                with_name="Momentum3D",
            )

            corrected_pt_up = np.sqrt(corrected_tau_3vec_up.x**2 + corrected_tau_3vec_up.y**2)
            corrected_eta_up = corrected_tau_3vec_up.pseudorapidity
            corrected_phi_up = corrected_tau_3vec_up.phi

            corrected_taus_up = deepcopy(events.Tau)
            corrected_taus_up["pt"] = ak.unflatten(corrected_pt_up, counts)
            corrected_taus_up["eta"] = ak.unflatten(corrected_eta_up, counts)
            corrected_taus_up["phi"] = ak.unflatten(corrected_phi_up, counts)
            corrected_taus_up["mass"] = ak.unflatten(corrected_M_up, counts)

            delta_tau_px_up = corrected_px_up - tau_4vec.px
            delta_tau_py_up = corrected_py_up - tau_4vec.py

            corrected_taus_up["EnergyScale_Shift_px"] = ak.unflatten(delta_tau_px_up, counts)
            corrected_taus_up["EnergyScale_Shift_py"] = ak.unflatten(delta_tau_py_up, counts)

            corrected_taus[f'{key}_up'] = corrected_taus_up

            corrected_px_down = np.where((dm != 5) | (dm != 6),tau_4vec.px * uncertainty_down, tau_4vec.px)
            corrected_py_down = np.where((dm != 5) | (dm != 6),tau_4vec.py * uncertainty_down, tau_4vec.py)
            corrected_pz_down = np.where((dm != 5) | (dm != 6),tau_4vec.pz * uncertainty_down, tau_4vec.pz)
            corrected_M_down = np.where((dm != 0) | (dm != 5) | (dm != 6),mass * uncertainty_down, mass)

            corrected_tau_3vec_down = ak.zip(
                {
                    "x": corrected_px_down,
                    "y": corrected_py_down,
                    "z": corrected_pz_down,
                },
                with_name="Momentum3D",
            )

            corrected_pt_down = np.sqrt(corrected_tau_3vec_down.x**2 + corrected_tau_3vec_down.y**2)
            corrected_eta_down = corrected_tau_3vec_down.pseudorapidity
            corrected_phi_down = corrected_tau_3vec_down.phi

            corrected_taus_down = deepcopy(events.Tau)
            corrected_taus_down["pt"] = ak.unflatten(corrected_pt_down, counts)
            corrected_taus_down["eta"] = ak.unflatten(corrected_eta_down, counts)
            corrected_taus_down["phi"] = ak.unflatten(corrected_phi_down, counts)
            corrected_taus_down["mass"] = ak.unflatten(corrected_M_down, counts)

            delta_tau_px_down = corrected_px_down - tau_4vec.px
            delta_tau_py_down = corrected_py_down - tau_4vec.py

            corrected_taus_down["EnergyScale_Shift_px"] = ak.unflatten(delta_tau_px_down, counts)
            corrected_taus_down["EnergyScale_Shift_py"] = ak.unflatten(delta_tau_py_down, counts)

            corrected_taus[f'{key}_down'] = corrected_taus_down

        return corrected_taus


def Tau_EnergyScale_forTauIDSFs(events, year, meta, is_correction=True):

    counts = ak.num(events.Tau.pt)

    tau_4vec = ak.zip(
        {
            "pt": ak.flatten(events.Tau.pt),
            "eta": ak.flatten(events.Tau.eta),
            "phi": ak.flatten(events.Tau.phi),
            "mass": ak.flatten(events.Tau.mass),
        },
        with_name="PtEtaPhiMLorentzVector",
    )

    mass = ak.flatten(events.Tau.mass)
    dm_hps = ak.flatten(events.Tau.decayMode)
    dm_pnet = ak.flatten(events.Tau.decayModePNet)
    gen = ak.flatten(events.Tau.genPartFlav)

    if is_correction:

        corrected_taus = deepcopy(events.Tau)

        delta_tau_px = np.zeros_like(tau_4vec.px)
        delta_tau_py = np.zeros_like(tau_4vec.py)

        corrected_taus["EnergyScale_Shift_px"] = ak.unflatten(delta_tau_px, counts)
        corrected_taus["EnergyScale_Shift_py"] = ak.unflatten(delta_tau_py, counts)

        events["Tau"] = corrected_taus

        return events

    else:

        corrected_taus = {}
        variations = {
            'TSCALE_1PRONG': [0, [5], 0.015],
            'TSCALE_1PRONG_1PI0': [1, [5], 0.015],
            'TSCALE_1PRONG_2PI0': [2, [5], 0.015],
            'TSCALE_3PRONG': [10, [5], 0.015],
            'TSCALE_3PRONG_1PI0': [11, [5], 0.020],
            'MUSCALE_1PRONG': [0, [2, 4], 0.01],
            'MUSCALE_1PRONG_1PI0': [1, [2, 4], 0.01],
            'MUSCALE_1PRONG_2PI0': [2, [2, 4], 0.01],
            'MUSCALE_3PRONG': [10, [2, 4], 0.01],
            'MUSCALE_3PRONG_1PI0': [11, [2, 4], 0.01],
            'JSCALE': ["ALL", [0], 0.03],
        }

        for key, requirements in variations.items():

            variation_mask = (gen == requirements[1][0])
            for index,flavour in enumerate(requirements[1]):
                if index == 0:
                    continue
                else:
                    variation_mask = (gen == flavour) | variation_mask

            if requirements[0] != "ALL":
                variation_mask = (dm_pnet == requirements[0]) & variation_mask

            if key in [
                'TSCALE_1PI', 'MUSCALE_1PI',
                'TSCALE_1PI_1PI0', 'MUSCALE_1PI_1PI0',
            ]:
                variation_mask = variation_mask & (dm_hps == 1)

            uncertainty_up = np.where(variation_mask, 1 + requirements[2], 1.0)
            uncertainty_down = np.where(variation_mask, 1 - requirements[2], 1.0)

            corrected_px_up = np.where((dm_hps != 5) | (dm_hps != 6),tau_4vec.px * uncertainty_up, tau_4vec.px)
            corrected_py_up = np.where((dm_hps != 5) | (dm_hps != 6),tau_4vec.py * uncertainty_up, tau_4vec.py)
            corrected_pz_up = np.where((dm_hps != 5) | (dm_hps != 6),tau_4vec.pz * uncertainty_up, tau_4vec.pz)
            corrected_M_up = np.where((dm_hps != 0) | (dm_hps != 5) | (dm_hps != 6),mass * uncertainty_up, mass)

            corrected_tau_3vec_up = ak.zip(
                {
                    "x": corrected_px_up,
                    "y": corrected_py_up,
                    "z": corrected_pz_up,
                },
                with_name="Momentum3D",
            )

            corrected_pt_up = np.sqrt(corrected_tau_3vec_up.x**2 + corrected_tau_3vec_up.y**2)
            corrected_eta_up = corrected_tau_3vec_up.pseudorapidity
            corrected_phi_up = corrected_tau_3vec_up.phi

            corrected_taus_up = deepcopy(events.Tau)
            corrected_taus_up["pt"] = ak.unflatten(corrected_pt_up, counts)
            corrected_taus_up["eta"] = ak.unflatten(corrected_eta_up, counts)
            corrected_taus_up["phi"] = ak.unflatten(corrected_phi_up, counts)
            corrected_taus_up["mass"] = ak.unflatten(corrected_M_up, counts)

            delta_tau_px_up = corrected_px_up - tau_4vec.px
            delta_tau_py_up = corrected_py_up - tau_4vec.py

            corrected_taus_up["EnergyScale_Shift_px"] = ak.unflatten(delta_tau_px_up, counts)
            corrected_taus_up["EnergyScale_Shift_py"] = ak.unflatten(delta_tau_py_up, counts)

            corrected_taus[f'{key}_up'] = corrected_taus_up

            corrected_px_down = np.where((dm_hps != 5) | (dm_hps != 6),tau_4vec.px * uncertainty_down, tau_4vec.px)
            corrected_py_down = np.where((dm_hps != 5) | (dm_hps != 6),tau_4vec.py * uncertainty_down, tau_4vec.py)
            corrected_pz_down = np.where((dm_hps != 5) | (dm_hps != 6),tau_4vec.pz * uncertainty_down, tau_4vec.pz)
            corrected_M_down = np.where((dm_hps != 0) | (dm_hps != 5) | (dm_hps != 6),mass * uncertainty_down, mass)

            corrected_tau_3vec_down = ak.zip(
                {
                    "x": corrected_px_down,
                    "y": corrected_py_down,
                    "z": corrected_pz_down,
                },
                with_name="Momentum3D",
            )

            corrected_pt_down = np.sqrt(corrected_tau_3vec_down.x**2 + corrected_tau_3vec_down.y**2)
            corrected_eta_down = corrected_tau_3vec_down.pseudorapidity
            corrected_phi_down = corrected_tau_3vec_down.phi

            corrected_taus_down = deepcopy(events.Tau)
            corrected_taus_down["pt"] = ak.unflatten(corrected_pt_down, counts)
            corrected_taus_down["eta"] = ak.unflatten(corrected_eta_down, counts)
            corrected_taus_down["phi"] = ak.unflatten(corrected_phi_down, counts)
            corrected_taus_down["mass"] = ak.unflatten(corrected_M_down, counts)

            delta_tau_px_down = corrected_px_down - tau_4vec.px
            delta_tau_py_down = corrected_py_down - tau_4vec.py

            corrected_taus_down["EnergyScale_Shift_px"] = ak.unflatten(delta_tau_px_down, counts)
            corrected_taus_down["EnergyScale_Shift_py"] = ak.unflatten(delta_tau_py_down, counts)

            corrected_taus[f'{key}_down'] = corrected_taus_down

        return corrected_taus
