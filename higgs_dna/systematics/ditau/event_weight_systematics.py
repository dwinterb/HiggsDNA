import numpy as np
import os
import correctionlib
import awkward as ak
import uproot
import logging

logger = logging.getLogger(__name__)


def Pileup(events, weights, year, is_correction=True, **kwargs):
    """
    Function to apply either the pileup correction to MC to make it match the pileup profile of a certain year/period,
    or the respective uncertainties.
    The parameter `year` needs to be specified as one of ["Run3_2022", "Run3_2022EE"] for Run-3 or ["Run2_2016_HIPM","Run2_2016","Run2_2017","Run2_2018"] for Run-2.
    """

    # Mapping from year names to folder names and JSON names
    year_mapping = {
        "Run2_2016_HIPM": ("2016preVFP_UL", "Collisions16_UltraLegacy_goldenJSON"),
        "Run2_2016": ("2016postVFP_UL", "Collisions16_UltraLegacy_goldenJSON"),
        "Run2_2017": ("2017_UL", "Collisions17_UltraLegacy_goldenJSON"),
        "Run2_2018": ("2018_UL", "Collisions18_UltraLegacy_goldenJSON"),
        "Run3_2022": ("2022_Summer22", "Collisions2022_355100_357900_eraBCD_GoldenJson"),
        "Run3_2022EE": ("2022_Summer22EE", "Collisions2022_359022_362760_eraEFG_GoldenJson"),
        "Run3_2023": ("2023_Summer23", "Collisions2023_366403_369802_eraBC_GoldenJson"),
        "Run3_2023BPix": ("2023_Summer23BPix", "Collisions2023_369803_370790_eraD_GoldenJson"),
    }

    folder_name, json_name = year_mapping.get(year, (None, None))
    if folder_name and json_name:
        path_to_json = os.path.join(os.path.dirname(__file__), f"JSONs/pileup/{folder_name}/puWeights.json.gz")
        evaluator = correctionlib.CorrectionSet.from_file(path_to_json)[json_name]
    else:
        raise ValueError(f"Year {year} not supported for pileup reweighting.")

    if is_correction:
        sf = evaluator.evaluate(events.Pileup.nTrueInt, "nominal")
        sfup, sfdown = None, None
    else:
        sf = np.ones(len(weights._weight))
        sf_nom = evaluator.evaluate(events.Pileup.nTrueInt, "nominal")
        sfup = evaluator.evaluate(events.Pileup.nTrueInt, "up") / sf_nom
        sfdown = evaluator.evaluate(events.Pileup.nTrueInt, "down") / sf_nom

    weights.add(name="Pileup", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights


def Zpt_Reweighting_Imperial(pair, weights, year, dataset_name, is_correction=True, **kwargs):
    if dataset_name in ["DYto2L_M_10to50_madgraphMLM","DYto2L_M_50_madgraphMLM","DYto2L_M_50_madgraphMLM_ext1","DYto2L_M_50_1J_madgraphMLM","DYto2L_M_50_2J_madgraphMLM","DYto2L_M_50_3J_madgraphMLM","DYto2L_M_50_4J_madgraphMLM"]:
        if year not in ["Run3_2022", "Run3_2022EE", "Run3_2023", "Run3_2023BPix"]:
            raise ValueError(f"Year {year} not supported for Zpt reweighting.")
        else:
            path_to_root = os.path.join(os.path.dirname(__file__), f"ROOT/Zpt/{year}/zpt_reweighting_LO.root")
            evaluator = uproot.open(path_to_root)["zptmass_histo"]
            if is_correction:

                z_pt = pair.gen_boson_pT
                z_mass = pair.gen_boson_mass
                z_pt = ak.fill_none(z_pt, 0.0)
                z_mass = ak.fill_none(z_mass, 0.0)
                z_pt = np.asarray(z_pt)
                z_mass = np.asarray(z_mass)

                corrections, x_edges, y_edges = evaluator.to_numpy()
                x_indices = np.searchsorted(x_edges, z_mass, side="right") - 1
                y_indices = np.searchsorted(y_edges, z_pt, side="right") - 1
                x_indices = np.clip(x_indices, 0, len(x_edges) - 2)
                y_indices = np.clip(y_indices, 0, len(y_edges) - 2)

                sf = corrections[x_indices, y_indices]
                sfup, sfdown = None, None
    else:
        sf = np.ones(len(weights._weight))
        sfup, sfdown = None, None

    weights.add(name="Zpt_Reweighting", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights


def Zpt_Reweighting(pair, weights, year, dataset_name, is_correction=True, **kwargs):
    year_mapping = {
        "Run3_2022": ("2022preEE_LO", "2022preEE_NLO"),
        "Run3_2022EE": ("2022postEE_LO", "2022postEE_NLO"),
        "Run3_2023": ("2023preBPix_LO", "2023preBPix_NLO"),
        "Run3_2023BPix": ("2023postBPix_LO", "2023postBPix_NLO"),
    }

    LO_datasets = ["DYto2L_M_10to50_madgraphMLM","DYto2L_M_50_madgraphMLM","DYto2L_M_50_madgraphMLM_ext1","DYto2L_M_50_1J_madgraphMLM","DYto2L_M_50_2J_madgraphMLM","DYto2L_M_50_3J_madgraphMLM","DYto2L_M_50_4J_madgraphMLM"]
    NLO_datasets = [
        "DYto2L_M_10to50_amcatnloFXFX","DYto2L_M_50_amcatnloFXFX","DYto2L_M_50_amcatnloFXFX_ext1","DYto2L_M_50_0J_amcatnloFXFX","DYto2L_M_50_1J_amcatnloFXFX","DYto2L_M_50_2J_amcatnloFXFX",
        "DYto2L_M_50_PTLL_40to100_1J_amcatnloFXFX","DYto2L_M_50_PTLL_100to200_1J_amcatnloFXFX","DYto2L_M_50_PTLL_200to400_1J_amcatnloFXFX","DYto2L_M_50_PTLL_400to600_1J_amcatnloFXFX","DYto2L_M_50_PTLL_600_1J_amcatnloFXFX",
        "DYto2L_M_50_PTLL_40to100_2J_amcatnloFXFX","DYto2L_M_50_PTLL_100to200_2J_amcatnloFXFX","DYto2L_M_50_PTLL_200to400_2J_amcatnloFXFX","DYto2L_M_50_PTLL_400to600_2J_amcatnloFXFX","DYto2L_M_50_PTLL_600_2J_amcatnloFXFX"
    ]

    if dataset_name in LO_datasets or dataset_name in NLO_datasets:
        if year not in ["Run3_2022", "Run3_2022EE", "Run3_2023", "Run3_2023BPix"]:
            raise ValueError(f"Year {year} not supported for Zpt reweighting.")
        else:
            LO_json_name, NLO_json_name = year_mapping.get(year, (None, None))

            path_to_json = os.path.join(os.path.dirname(__file__), "JSONs/zpt/DY_pTll_recoil_corrections.json.gz")
            evaluator = correctionlib.CorrectionSet.from_file(path_to_json)["DY_pTll_reweighting"]
            if dataset_name in LO_datasets:
                sf = evaluator.evaluate(LO_json_name, pair.gen_boson_pT, "nom")
            elif dataset_name in NLO_datasets:
                sf = evaluator.evaluate(NLO_json_name, pair.gen_boson_pT, "nom")
            sfup, sfdown = None, None
    else:
        sf = np.ones(len(weights._weight))
        sfup, sfdown = None, None

    weights.add(name="Zpt_Reweighting", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights


def Top_pt_Reweighting(pair, weights, dataset_name, is_correction=True, **kwargs):
    full_sf = np.ones(len(weights._weight))
    sfup, sfdown = None, None

    if dataset_name in ["TTto2L2Nu", "TTto2L2Nu_ext1", "TTtoLNu2Q", "TTtoLNu2Q_ext1", "TTto4Q", "TTto4Q_ext1", "TT", "TT_ext1"]:
        top_pT = pair.gen_top_pT
        sf_13p0 = 0.103 * np.exp(-0.0018 * top_pT) - 0.000134 * top_pT + 0.973
        extrapolation_sf_13p6 = 0.991 + 0.000075 * top_pT
        sf = sf_13p0 * extrapolation_sf_13p6
        full_sf = ak.prod(sf, axis=1)
        full_sf = np.sqrt(full_sf)
        if not is_correction:
            sfup = (full_sf**2) / full_sf
            sfdown = 1.0 / full_sf
            full_sf = np.ones(len(weights._weight))

    weights.add(name="Top_pt_Reweighting", weight=full_sf, weightUp=sfup, weightDown=sfdown)

    return weights


def ggH_QuarkMass_Effects(pair, weights, year, dataset_name, is_correction=True, **kwargs):
    if dataset_name in ["GluGluHTo2Tau_UncorrelatedDecay_CPodd_UnFiltered_ProdAndDecay", "GluGluHTo2Tau_UncorrelatedDecay_CPodd_Filtered_ProdAndDecay"
                        "GluGluHTo2Tau_UncorrelatedDecay_MM_UnFiltered_ProdAndDecay", "GluGluHTo2Tau_UncorrelatedDecay_MM_Filtered_ProdAndDecay",
                        "GluGluHTo2Tau_UncorrelatedDecay_SM_UnFiltered_ProdAndDecay", "GluGluHTo2Tau_UncorrelatedDecay_SM_Filtered_ProdAndDecay"]:
        if year not in ["Run3_2022", "Run3_2022EE", "Run3_2023", "Run3_2023BPix"]:
            raise ValueError(f"Year {year} not supported for quark mass effect inclusion")
        else:
            # path to input file once Danny unlocks it :)
            path_to_root = os.path.join(os.path.dirname(__file__), "ROOT/QuarkMass_ggH/Run3/ggH_quarkmass_effects_correction_13p6TeV_v1.root")
            evaluator = uproot.open(path_to_root)["corr_fullmasseffects"]
            if is_correction:

                z_pt = pair.gen_boson_pT
                z_pt = ak.fill_none(z_pt, 0.0)
                z_pt = np.asarray(z_pt)

                corrections, x_edges = evaluator.to_numpy()
                x_indices = np.searchsorted(x_edges, z_pt, side="right") - 1
                x_indices = np.clip(x_indices, 0, len(x_edges) - 2)

                sf = corrections[x_indices]
                sfup, sfdown = None, None
    else:
        sf = np.ones(len(weights._weight))
        sfup, sfdown = None, None

    weights.add(name="ggH_QuarkMass_Effects", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights


def Tau_ID(pair, channel, weights, year, meta, is_correction=True, **kwargs):

    # Mapping from year names to folder names and JSON names
    year_mapping = {
        "Run2_2016_HIPM": ("2016preVFP_UL", "UL2016_preVFP"),
        "Run2_2016": ("2016postVFP_UL", "UL2016_postVFP"),
        "Run2_2017": ("2017_UL", "UL2017"),
        "Run2_2018": ("2018_UL", "UL2018"),
        "Run3_2022": ("2022_preEE", "2022_preEE", "2022_preEE"),
        "Run3_2022EE": ("2022_postEE", "2022_postEE", "2022_postEE"),
        "Run3_2023": ("2023_Summer23", "2023preBPix", "2023_preBPix"),
        "Run3_2023BPix": ("2023_Summer23BPix", "2023postBPix", "2023_postBPix"),
    }

    analysis = meta.get("Analysis", "Default")

    folder_name, file_name, file_name_CP = year_mapping.get(year, (None, None, None))
    json_name = "DeepTau2018v2p5VSjet"
    if folder_name and json_name:
        if analysis == "CP":
            json_name_CP = f"tau_sf_pt-dm_DeepTau2018v2p5VSjet_{file_name_CP}"
            path_to_json = os.path.join(os.path.dirname(__file__), f"JSONs/TauID/CP/{json_name_CP}.json.gz")
            evaluator = correctionlib.CorrectionSet.from_file(path_to_json)[json_name_CP]
        else:
            path_to_json = os.path.join(os.path.dirname(__file__), f"JSONs/TauID/{folder_name}/tau_DeepTau2018v2p5_{file_name}.json.gz")
            evaluator = correctionlib.CorrectionSet.from_file(path_to_json)[json_name]
    else:
        raise ValueError(f"Year {year} not supported for TAU ID.")

    year = file_name
    wp_VSjet = meta["Selections"].get(channel, {}).get("Tau_ID_VSjet_WP", "Tight")
    wp_VSe = meta["Selections"].get(channel, {}).get("Tau_ID_VSe_WP", "Tight")

    systematics_map = {
        "stat1": "stat1_dm$DM",
        "stat2": "stat2_dm$DM",
        "syst_all_eras": "syst_alleras",
        "syst_era": "syst_$ERA",
        "syst_TES_era_dm": "syst_TES_$ERA_dm$DM"
    }

    all_variations = [
        "1_stat1_",
        "2_stat1_",
        "1_stat2_",
        "2_stat2_",
        "1_syst_all_eras_",
        "2_syst_all_eras_",
        "1_syst_era_",
        "2_syst_era_",
        "1_syst_TES_era_dm_",
        "2_syst_TES_era_dm_"
    ]

    decay_mode_key = "decayModePNet" if analysis == "CP" else "decayMode"

    obj_decay_modes = {}
    if channel in ["et", "mt"]:
        obj_2_decay_mode = getattr(pair.obj_2, decay_mode_key)
        masked_obj_2_decay_mode = np.where(obj_2_decay_mode == -1, 0, obj_2_decay_mode)
        obj_decay_modes["2"] = masked_obj_2_decay_mode

    elif channel in ["tt"]:
        obj_1_decay_mode = getattr(pair.obj_1, decay_mode_key)
        masked_obj_1_decay_mode = np.where(obj_1_decay_mode == -1, 0, obj_1_decay_mode)
        obj_decay_modes["1"] = masked_obj_1_decay_mode
        obj_2_decay_mode = getattr(pair.obj_2, decay_mode_key)
        masked_obj_2_decay_mode = np.where(obj_2_decay_mode == -1, 0, obj_2_decay_mode)
        obj_decay_modes["2"] = masked_obj_2_decay_mode

    if is_correction:
        sf = np.ones(len(weights._weight))
        for index in range(len(channel)):
            pnumber = index + 1
            if channel[index] == "t":
                sf *= evaluator.evaluate(
                    pair[f'obj_{pnumber}'].pt, obj_decay_modes[str(pnumber)], pair[f'obj_{pnumber}'].genPartFlav,wp_VSjet, wp_VSe, "nom", "dm"
                )

        sfs_up = [None for _ in all_variations]
        sfs_down = [None for _ in all_variations]

    else:
        variations = {}

        decay_modes = [0, 1, 10, 11]
        if analysis == "CP":
            decay_modes = [0, 1, 2, 10]

        for syst_name in all_variations:
            variations[syst_name] = {}
            variations[syst_name]["up"] = np.ones(len(weights._weight))
            variations[syst_name]["down"] = np.ones(len(weights._weight))

        for index in range(len(channel)):
            pnumber = index + 1
            sf = np.ones(len(weights._weight))

            if channel[index] == "t":
                _sf = evaluator.evaluate(
                    pair[f'obj_{pnumber}'].pt, obj_decay_modes[str(pnumber)], pair[f'obj_{pnumber}'].genPartFlav,wp_VSjet, wp_VSe, "nom", "dm"
                )
                for syst_name,json_syst_name in systematics_map.items():
                    if syst_name in ["stat1", "stat2"]:
                        for updown in ["up", "down"]:
                            temporary_variations = {}
                            for dm in decay_modes:
                                temporary_variations[f"DM{dm}"] = np.ones(len(weights._weight))
                            for dm in decay_modes:
                                json_variation = f"{json_syst_name}_{updown}".replace("$DM", str(dm))
                                temp_sf_updown = evaluator.evaluate(pair[f'obj_{pnumber}'].pt, dm, pair[f'obj_{pnumber}'].genPartFlav,wp_VSjet, wp_VSe, json_variation, "dm")
                                temporary_variations[f"DM{dm}"] *= (temp_sf_updown / _sf)

                            sf_updown = np.ones(len(weights._weight))
                            for dm in decay_modes:
                                mask = obj_decay_modes[str(pnumber)] == dm
                                sf_updown = np.array(np.where(mask, temporary_variations[f"DM{dm}"], sf_updown))

                            variations[f"{pnumber}_{syst_name}_"][updown] *= sf_updown

                    elif syst_name == "syst_all_eras":
                        for updown in ["up", "down"]:
                            json_variation = f"{json_syst_name}_{updown}"
                            sf_updown = evaluator.evaluate(pair[f'obj_{pnumber}'].pt, obj_decay_modes[str(pnumber)], pair[f'obj_{pnumber}'].genPartFlav,wp_VSjet, wp_VSe, json_variation, "dm")
                            variations[f"{pnumber}_{syst_name}_"][updown] *= (sf_updown / _sf)

                    elif syst_name == "syst_era":
                        for updown in ["up", "down"]:
                            json_variation = f"{json_syst_name}_{updown}".replace("$ERA", year)
                            sf_updown = evaluator.evaluate(pair[f'obj_{pnumber}'].pt, obj_decay_modes[str(pnumber)], pair[f'obj_{pnumber}'].genPartFlav,wp_VSjet, wp_VSe, json_variation, "dm")
                            variations[f"{pnumber}_{syst_name}_"][updown] *= (sf_updown / _sf)

                    elif syst_name == "syst_TES_era_dm":
                        for updown in ["up", "down"]:
                            temporary_variations = {}
                            for dm in decay_modes:
                                temporary_variations[f"DM{dm}"] = np.ones(len(weights._weight))
                            for dm in decay_modes:
                                json_variation = f"{json_syst_name}_{updown}".replace("$ERA", year).replace("$DM", str(dm))
                                temp_sf_updown = evaluator.evaluate(pair[f'obj_{pnumber}'].pt, dm, pair[f'obj_{pnumber}'].genPartFlav,wp_VSjet, wp_VSe, json_variation, "dm")
                                temporary_variations[f"DM{dm}"] *= (temp_sf_updown / _sf)

                            sf_updown = np.ones(len(weights._weight))
                            for dm in decay_modes:
                                mask = obj_decay_modes[str(pnumber)] == dm
                                sf_updown = np.array(np.where(mask, temporary_variations[f"DM{dm}"], sf_updown))

                            variations[f"{pnumber}_{syst_name}_"][updown] *= sf_updown

        sfs_up = [variations[syst_name]["up"] for syst_name in all_variations]
        sfs_down = [variations[syst_name]["down"] for syst_name in all_variations]

    weights.add_multivariation(
        name="Tau_ID",
        weight=sf,
        weightsUp=sfs_up,
        weightsDown=sfs_down,
        modifierNames=all_variations
    )

    return weights


def Tau_e_FakeRate(pair, channel, weights, year, meta, is_correction=True, **kwargs):

    # Mapping from year names to folder names and JSON names
    year_mapping = {
        "Run2_2016_HIPM": ("2016preVFP_UL", "UL2016_preVFP"),
        "Run2_2016": ("2016postVFP_UL", "UL2016_postVFP"),
        "Run2_2017": ("2017_UL", "UL2017"),
        "Run2_2018": ("2018_UL", "UL2018"),
        "Run3_2022": ("2022_preEE", "2022_preEE"),
        "Run3_2022EE": ("2022_postEE", "2022_postEE"),
        "Run3_2023": ("2023_Summer23", "2023preBPix"),
        "Run3_2023BPix": ("2023_Summer23BPix", "2023postBPix"),
    }

    folder_name, file_name = year_mapping.get(year, (None, None))
    json_name = "DeepTau2018v2p5VSe"
    if folder_name and json_name:
        path_to_json = os.path.join(os.path.dirname(__file__), f"JSONs/TauID/{folder_name}/tau_DeepTau2018v2p5_{file_name}.json.gz")
        evaluator = correctionlib.CorrectionSet.from_file(path_to_json)[json_name]
    else:
        raise ValueError(f"Year {year} not supported for TAU ID.")

    year = file_name
    wp_VSe = meta["Selections"].get(channel, {}).get("Tau_ID_VSe_WP", "Tight")

    if is_correction:
        sf = np.ones(len(weights._weight))
        sfs_up = [None for _ in ["1", "2"]]
        sfs_down = [None for _ in ["1", "2"]]

        for index in range(len(channel)):
            pnumber = index + 1
            if channel[index] == "t":
                mask = (pair[f'obj_{pnumber}'].decayMode == 2) | (pair[f'obj_{pnumber}'].decayMode == 5) | (pair[f'obj_{pnumber}'].decayMode == 6)
                filtered_DM = np.where(mask, np.zeros(len(weights._weight)), pair[f'obj_{pnumber}'].decayMode)
                filtered_sf = np.where(mask, np.ones(len(weights._weight)), evaluator.evaluate(pair[f'obj_{pnumber}'].eta, filtered_DM, pair[f'obj_{pnumber}'].genPartFlav, wp_VSe, "nom"))
                sf *= ak.to_numpy(filtered_sf)
    else:

        variations_up = {}
        variations_up["1"] = np.ones(len(weights._weight))
        variations_up["2"] = np.ones(len(weights._weight))
        variations_down = {}
        variations_down["1"] = np.ones(len(weights._weight))
        variations_down["2"] = np.ones(len(weights._weight))

        sf = np.ones(len(weights._weight))
        sfup = np.ones(len(weights._weight))
        sfdown = np.ones(len(weights._weight))

        for index in range(len(channel)):
            pnumber = index + 1
            if channel[index] == "t":
                mask = (pair[f'obj_{pnumber}'].decayMode == 2) | (pair[f'obj_{pnumber}'].decayMode == 5) | (pair[f'obj_{pnumber}'].decayMode == 6)
                filtered_DM = np.where(mask, np.zeros(len(weights._weight)), pair[f'obj_{pnumber}'].decayMode)
                filtered_sf = np.where(mask, np.ones(len(weights._weight)), evaluator.evaluate(pair[f'obj_{pnumber}'].eta, filtered_DM, pair[f'obj_{pnumber}'].genPartFlav, wp_VSe, "nom"))

                _sf = ak.to_numpy(filtered_sf)
                sfup = ak.to_numpy(np.where(mask, np.ones(len(weights._weight)), evaluator.evaluate(pair[f'obj_{pnumber}'].eta, filtered_DM, pair[f'obj_{pnumber}'].genPartFlav, wp_VSe, "up")))
                sfdown = ak.to_numpy(np.where(mask, np.ones(len(weights._weight)), evaluator.evaluate(pair[f'obj_{pnumber}'].eta, filtered_DM, pair[f'obj_{pnumber}'].genPartFlav, wp_VSe, "down")))

                variations_up[f'{pnumber}'] *= (sfup / _sf)
                variations_down[f'{pnumber}'] *= (sfdown / _sf)

        sfs_up = [variations_up[f'{pnumber}'] for pnumber in ["1", "2"]]
        sfs_down = [variations_down[f'{pnumber}'] for pnumber in ["1", "2"]]

    weights.add_multivariation(
        name="Tau_e_FakeRate",
        weight=sf,
        weightsUp=sfs_up,
        weightsDown=sfs_down,
        modifierNames=["1_", "2_"]
    )

    return weights


def Tau_mu_FakeRate(pair, channel, weights, year, meta, is_correction=True, **kwargs):
    # Mapping from year names to folder names and JSON names
    year_mapping = {
        "Run2_2016_HIPM": ("2016preVFP_UL", "UL2016_preVFP"),
        "Run2_2016": ("2016postVFP_UL", "UL2016_postVFP"),
        "Run2_2017": ("2017_UL", "UL2017"),
        "Run2_2018": ("2018_UL", "UL2018"),
        "Run3_2022": ("2022_preEE", "2022_preEE"),
        "Run3_2022EE": ("2022_postEE", "2022_postEE"),
        "Run3_2023": ("2023_Summer23", "2023preBPix"),
        "Run3_2023BPix": ("2023_Summer23BPix", "2023postBPix"),
    }

    folder_name, file_name = year_mapping.get(year, (None, None))
    json_name = "DeepTau2018v2p5VSmu"
    if folder_name and json_name:
        path_to_json = os.path.join(os.path.dirname(__file__), f"JSONs/TauID/{folder_name}/tau_DeepTau2018v2p5_{file_name}.json.gz")
        evaluator = correctionlib.CorrectionSet.from_file(path_to_json)[json_name]
    else:
        raise ValueError(f"Year {year} not supported for TAU ID.")

    year = file_name
    wp_VSmu = meta["Selections"].get(channel, {}).get("Tau_ID_VSmu_WP", "Tight")

    if is_correction:
        sf = np.ones(len(weights._weight))
        sfs_up = [None for _ in ["1", "2"]]
        sfs_down = [None for _ in ["1", "2"]]

        for index in range(len(channel)):
            pnumber = index + 1
            if channel[index] == "t":
                sf *= evaluator.evaluate(
                    pair[f'obj_{pnumber}'].eta, pair[f'obj_{pnumber}'].genPartFlav, wp_VSmu, "nom"
                )
    else:
        variations_up = {}
        variations_up["1"] = np.ones(len(weights._weight))
        variations_up["2"] = np.ones(len(weights._weight))
        variations_down = {}
        variations_down["1"] = np.ones(len(weights._weight))
        variations_down["2"] = np.ones(len(weights._weight))

        sf = np.ones(len(weights._weight))
        sfup = np.ones(len(weights._weight))
        sfdown = np.ones(len(weights._weight))

        for index in range(len(channel)):
            pnumber = index + 1
            if channel[index] == "t":
                _sf = evaluator.evaluate(
                    pair[f'obj_{pnumber}'].eta, pair[f'obj_{pnumber}'].genPartFlav, wp_VSmu, "nom"
                )
                sfup = evaluator.evaluate(
                    pair[f'obj_{pnumber}'].eta, pair[f'obj_{pnumber}'].genPartFlav, wp_VSmu, "up"
                )
                sfdown = evaluator.evaluate(
                    pair[f'obj_{pnumber}'].eta, pair[f'obj_{pnumber}'].genPartFlav, wp_VSmu, "down"
                )

                variations_up[f'{pnumber}'] *= (sfup / _sf)
                variations_down[f'{pnumber}'] *= (sfdown / _sf)

        sfs_up = [variations_up[f'{pnumber}'] for pnumber in ["1", "2"]]
        sfs_down = [variations_down[f'{pnumber}'] for pnumber in ["1", "2"]]

    weights.add_multivariation(
        name="Tau_mu_FakeRate",
        weight=sf,
        weightsUp=sfs_up,
        weightsDown=sfs_down,
        modifierNames=["1_", "2_"]
    )

    return weights


def Electron_ID(pair, channel, weights, year, meta, is_correction=True, **kwargs):

    # Mapping from year names to folder names and JSON names
    year_mapping = {
        "Run2_2016_HIPM": ("2016preVFP_UL", "UL-Electron-ID-SF","2016preVFP"),
        "Run2_2016": ("2016postVFP_UL", "UL-Electron-ID-SF","2016postVFP"),
        "Run2_2017": ("2017_UL", "UL-Electron-ID-SF","2017"),
        "Run2_2018": ("2018_UL", "UL-Electron-ID-SF","2018"),
        "Run3_2022": ("2022_Summer22", "Electron-ID-SF","2022Re-recoBCD"),
        "Run3_2022EE": ("2022_Summer22EE", "Electron-ID-SF","2022Re-recoE+PromptFG"),
        "Run3_2023": ("2023_Summer23", "Electron-ID-SF","2023PromptC"),
        "Run3_2023BPix": ("2023_Summer23BPix", "Electron-ID-SF","2023PromptD"),
    }

    folder_name, json_name, year_arg = year_mapping.get(year, (None, None, None))
    if folder_name and json_name:
        path_to_json = os.path.join(os.path.dirname(__file__), f"JSONs/EGamma/{folder_name}/electron.json.gz")
        evaluator = correctionlib.CorrectionSet.from_file(path_to_json)[json_name]
    else:
        raise ValueError(f"Year {year} not supported for electron ID.")

    # no iso is not available at the moment get this to just test
    id_WP = meta.get("Ele_ID","wp90iso")

    id_mapping = {
        "mvaNoIso_WP90": "wp90noiso",
        "mvaIso_WP90": "wp90iso",
    }

    id_WP = id_mapping.get(id_WP, None)

    if is_correction:
        sf = np.ones(len(weights._weight))
        sfup, sfdown = None, None

        if channel in ["et","em"]:
            if year in ["Run3_2022", "Run3_2022EE"]:
                sf *= evaluator.evaluate(
                    year_arg, "sf", id_WP, pair['obj_1'].eta, pair['obj_1'].pt
                )
            elif year in ["Run3_2023", "Run3_2023BPix"]:
                sf *= evaluator.evaluate(
                    year_arg, "sf", id_WP, pair['obj_1'].eta, pair['obj_1'].pt, pair['obj_1'].phi
                )
        elif channel == "ee":
            if year in ["Run3_2022", "Run3_2022EE"]:
                sf *= evaluator.evaluate(
                    year_arg, "sf", id_WP, pair['obj_1'].eta, pair['obj_1'].pt
                )
                sf *= evaluator.evaluate(
                    year_arg, "sf", id_WP, pair['obj_2'].eta, pair['obj_2'].pt
                )
            elif year in ["Run3_2023", "Run3_2023BPix"]:
                sf *= evaluator.evaluate(
                    year_arg, "sf", id_WP, pair['obj_1'].eta, pair['obj_1'].pt, pair['obj_1'].phi
                )
                sf *= evaluator.evaluate(
                    year_arg, "sf", id_WP, pair['obj_2'].eta, pair['obj_2'].pt, pair['obj_2'].phi
                )
    else:
        sf = np.ones(len(weights._weight))
        _sf = np.ones(len(weights._weight))
        sfup = np.ones(len(weights._weight))
        sfdown = np.ones(len(weights._weight))

        if channel in ["et","em"]:
            if year in ["Run3_2022", "Run3_2022EE"]:
                _sf *= evaluator.evaluate(
                    year_arg, "sf", id_WP, pair['obj_1'].eta, pair['obj_1'].pt
                )
                sfup *= evaluator.evaluate(
                    year_arg, "sfup", id_WP, pair['obj_1'].eta, pair['obj_1'].pt,
                )
                sfdown *= evaluator.evaluate(
                    year_arg, "sfdown", id_WP, pair['obj_1'].eta, pair['obj_1'].pt,
                )
            elif year in ["Run3_2023", "Run3_2023BPix"]:
                _sf *= evaluator.evaluate(
                    year_arg, "sf", id_WP, pair['obj_1'].eta, pair['obj_1'].pt, pair['obj_1'].phi
                )
                sfup *= evaluator.evaluate(
                    year_arg, "sfup", id_WP, pair['obj_1'].eta, pair['obj_1'].pt, pair['obj_1'].phi
                )
                sfdown *= evaluator.evaluate(
                    year_arg, "sfdown", id_WP, pair['obj_1'].eta, pair['obj_1'].pt, pair['obj_1'].phi
                )

        elif channel == "ee":
            if year in ["Run3_2022", "Run3_2022EE"]:
                _sf *= evaluator.evaluate(
                    year_arg, "sf", id_WP, pair['obj_1'].eta, pair['obj_1'].pt
                )
                _sf *= evaluator.evaluate(
                    year_arg, "sf", id_WP, pair['obj_2'].eta, pair['obj_2'].pt
                )
                sfup *= evaluator.evaluate(
                    year_arg, "sfup", id_WP, pair['obj_1'].eta, pair['obj_1'].pt
                )
                sfup *= evaluator.evaluate(
                    year_arg, "sfup", id_WP, pair['obj_2'].eta, pair['obj_2'].pt
                )
                sfdown *= evaluator.evaluate(
                    year_arg, "sfdown", id_WP, pair['obj_1'].eta, pair['obj_1'].pt
                )
                sfdown *= evaluator.evaluate(
                    year_arg, "sfdown", id_WP, pair['obj_2'].eta, pair['obj_2'].pt
                )
            elif year in ["Run3_2023", "Run3_2023BPix"]:
                _sf *= evaluator.evaluate(
                    year_arg, "sf", id_WP, pair['obj_1'].eta, pair['obj_1'].pt, pair['obj_1'].phi
                )
                _sf *= evaluator.evaluate(
                    year_arg, "sf", id_WP, pair['obj_2'].eta, pair['obj_2'].pt, pair['obj_2'].phi
                )
                sfup *= evaluator.evaluate(
                    year_arg, "sfup", id_WP, pair['obj_1'].eta, pair['obj_1'].pt, pair['obj_1'].phi
                )
                sfup *= evaluator.evaluate(
                    year_arg, "sfup", id_WP, pair['obj_2'].eta, pair['obj_2'].pt, pair['obj_2'].phi
                )
                sfdown *= evaluator.evaluate(
                    year_arg, "sfdown", id_WP, pair['obj_1'].eta, pair['obj_1'].pt, pair['obj_1'].phi
                )
                sfdown *= evaluator.evaluate(
                    year_arg, "sfdown", id_WP, pair['obj_2'].eta, pair['obj_2'].pt, pair['obj_2'].phi
                )

        sfup = sfup / _sf
        sfdown = sfdown / _sf

    weights.add(name="Electron_ID", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights


def Electron_Reco(pair, channel, weights, year, is_correction=True, **kwargs):
    # Mapping from year names to folder names and JSON names
    year_mapping = {
        "Run2_2016_HIPM": ("2016preVFP_UL", "UL-Electron-ID-SF","2016preVFP"),
        "Run2_2016": ("2016postVFP_UL", "UL-Electron-ID-SF","2016postVFP"),
        "Run2_2017": ("2017_UL", "UL-Electron-ID-SF","2017"),
        "Run2_2018": ("2018_UL", "UL-Electron-ID-SF","2018"),
        "Run3_2022": ("2022_Summer22", "Electron-ID-SF","2022Re-recoBCD"),
        "Run3_2022EE": ("2022_Summer22EE", "Electron-ID-SF","2022Re-recoE+PromptFG"),
        "Run3_2023": ("2023_Summer23", "Electron-ID-SF","2023PromptC"),
        "Run3_2023BPix": ("2023_Summer23BPix", "Electron-ID-SF","2023PromptD"),
    }

    folder_name, json_name, year_arg = year_mapping.get(year, (None, None, None))
    if folder_name and json_name:
        path_to_json = os.path.join(os.path.dirname(__file__), f"JSONs/EGamma/{folder_name}/electron.json.gz")
        evaluator = correctionlib.CorrectionSet.from_file(path_to_json)[json_name]
    else:
        raise ValueError(f"Year {year} not supported for electron reco.")

    low_electron_pt_1 = np.clip(ak.to_numpy(pair['obj_1'].pt), 10.0, 19.99999)
    low_electron_pt_2 = np.clip(ak.to_numpy(pair['obj_2'].pt), 10.0, 19.99999)
    if "Run3" in year:
        med_electron_pt_1 = np.clip(ak.to_numpy(pair['obj_1'].pt), 20.0, 74.99999)
        high_electron_pt_1 = np.clip(ak.to_numpy(pair['obj_1'].pt), 75.0, np.inf)
        med_electron_pt_2 = np.clip(ak.to_numpy(pair['obj_2'].pt), 20.0, 74.99999)
        high_electron_pt_2 = np.clip(ak.to_numpy(pair['obj_2'].pt), 75.0, np.inf)
    else:
        high_electron_pt_1 = np.clip(ak.to_numpy(pair['obj_1'].pt), 20.0, np.inf)
        high_electron_pt_2 = np.clip(ak.to_numpy(pair['obj_2'].pt), 20.0, np.inf)

    sf = np.ones(len(weights._weight))
    sfup, sfdown = None, None

    if channel in ["et","em","ee"]:
        if "Run3" not in year:
            sf_low = evaluator.evaluate(
                year_arg, "sf", "RecoBelow20", pair['obj_1'].eta, low_electron_pt_1
            )
            sf_high = evaluator.evaluate(
                year_arg, "sf", "RecoAbove20", pair['obj_1'].eta, high_electron_pt_1
            )
            sf_low = ak.to_numpy(np.where(pair['obj_1'].pt < 20.0, sf_low, np.ones(len(weights._weight))))
            sf_high = ak.to_numpy(np.where(pair['obj_1'].pt >= 20.0, sf_high, np.ones(len(weights._weight))))

            if channel == "ee":
                sf_low_2 = evaluator.evaluate(
                    year_arg, "sf", "RecoBelow20", pair['obj_2'].eta, low_electron_pt_2
                )
                sf_high_2 = evaluator.evaluate(
                    year_arg, "sf", "RecoAbove20", pair['obj_2'].eta, high_electron_pt_2
                )

                sf_low_2 = ak.to_numpy(np.where(pair['obj_2'].pt < 20.0, sf_low_2, np.ones(len(weights._weight))))
                sf_high_2 = ak.to_numpy(np.where(pair['obj_2'].pt >= 20.0, sf_high_2, np.ones(len(weights._weight))))

                sf_low = sf_low * sf_low_2
                sf_high = sf_high * sf_high_2

            sf *= (sf_low * sf_high)

        else:
            if year in ["Run3_2022", "Run3_2022EE"]:
                sf_low = evaluator.evaluate(
                    year_arg, "sf", "RecoBelow20", pair['obj_1'].eta, low_electron_pt_1
                )
                sf_med = evaluator.evaluate(
                    year_arg, "sf", "Reco20to75", pair['obj_1'].eta, med_electron_pt_1
                )
                sf_high = evaluator.evaluate(
                    year_arg, "sf", "RecoAbove75", pair['obj_1'].eta, high_electron_pt_1
                )

                sf_low = ak.to_numpy(np.where(pair['obj_1'].pt < 20.0, sf_low, np.ones(len(weights._weight))))
                sf_med = ak.to_numpy(np.where((pair['obj_1'].pt >= 20.0) & (pair['obj_1'].pt < 75.0), sf_med, np.ones(len(weights._weight))))
                sf_high = ak.to_numpy(np.where(pair['obj_1'].pt >= 75.0, sf_high, np.ones(len(weights._weight))))

                if channel == "ee":
                    sf_low_2 = evaluator.evaluate(
                        year_arg, "sf", "RecoBelow20", pair['obj_2'].eta, low_electron_pt_2
                    )
                    sf_med_2 = evaluator.evaluate(
                        year_arg, "sf", "Reco20to75", pair['obj_2'].eta, med_electron_pt_2
                    )
                    sf_high_2 = evaluator.evaluate(
                        year_arg, "sf", "RecoAbove75", pair['obj_2'].eta, high_electron_pt_2
                    )

                    sf_low_2 = ak.to_numpy(np.where(pair['obj_2'].pt < 20.0, sf_low_2, np.ones(len(weights._weight))))
                    sf_med_2 = ak.to_numpy(np.where((pair['obj_2'].pt >= 20.0) & (pair['obj_2'].pt < 75.0), sf_med_2, np.ones(len(weights._weight))))
                    sf_high_2 = ak.to_numpy(np.where(pair['obj_2'].pt >= 75.0, sf_high_2, np.ones(len(weights._weight))))

                    sf_low = sf_low * sf_low_2
                    sf_med = sf_med * sf_med_2
                    sf_high = sf_high * sf_high_2

            elif year in ["Run3_2023", "Run3_2023BPix"]:
                sf_low = evaluator.evaluate(
                    year_arg, "sf", "RecoBelow20", pair['obj_1'].eta, low_electron_pt_1, pair['obj_1'].phi
                )
                sf_med = evaluator.evaluate(
                    year_arg, "sf", "Reco20to75", pair['obj_1'].eta, med_electron_pt_1, pair['obj_1'].phi
                )
                sf_high = evaluator.evaluate(
                    year_arg, "sf", "RecoAbove75", pair['obj_1'].eta, high_electron_pt_1, pair['obj_1'].phi
                )

                sf_low = ak.to_numpy(np.where(pair['obj_1'].pt < 20.0, sf_low, np.ones(len(weights._weight))))
                sf_med = ak.to_numpy(np.where((pair['obj_1'].pt >= 20.0) & (pair['obj_1'].pt < 75.0), sf_med, np.ones(len(weights._weight))))
                sf_high = ak.to_numpy(np.where(pair['obj_1'].pt >= 75.0, sf_high, np.ones(len(weights._weight))))

                if channel == "ee":
                    sf_low_2 = evaluator.evaluate(
                        year_arg, "sf", "RecoBelow20", pair['obj_2'].eta, low_electron_pt_2, pair['obj_2'].phi
                    )
                    sf_med_2 = evaluator.evaluate(
                        year_arg, "sf", "Reco20to75", pair['obj_2'].eta, med_electron_pt_2, pair['obj_2'].phi
                    )
                    sf_high_2 = evaluator.evaluate(
                        year_arg, "sf", "RecoAbove75", pair['obj_2'].eta, high_electron_pt_2, pair['obj_2'].phi
                    )

                    sf_low_2 = ak.to_numpy(np.where(pair['obj_2'].pt < 20.0, sf_low_2, np.ones(len(weights._weight))))
                    sf_med_2 = ak.to_numpy(np.where((pair['obj_2'].pt >= 20.0) & (pair['obj_2'].pt < 75.0), sf_med_2, np.ones(len(weights._weight))))
                    sf_high_2 = ak.to_numpy(np.where(pair['obj_2'].pt >= 75.0, sf_high_2, np.ones(len(weights._weight))))

                    sf_low = sf_low * sf_low_2
                    sf_med = sf_med * sf_med_2
                    sf_high = sf_high * sf_high_2

            sf *= (sf_low * sf_med * sf_high)

        if not is_correction:
            sfup, sfdown = np.ones(len(weights._weight)), np.ones(len(weights._weight))
            if "Run3" not in year:
                sf_low_up = evaluator.evaluate(
                    year_arg, "sfup", "RecoBelow20", pair['obj_1'].eta, low_electron_pt_1
                )
                sf_low_down = evaluator.evaluate(
                    year_arg, "sfdown", "RecoBelow20", pair['obj_1'].eta, low_electron_pt_1
                )
                sf_high_up = evaluator.evaluate(
                    year_arg, "sfup", "RecoAbove20", pair['obj_1'].eta, high_electron_pt_1
                )
                sf_high_down = evaluator.evaluate(
                    year_arg, "sfdown", "RecoAbove20", pair['obj_1'].eta, high_electron_pt_1
                )
                sf_low_up = np.where(pair['obj_1'].pt < 20.0, sf_low_up, np.ones(len(weights._weight)))
                sf_low_down = np.where(pair['obj_1'].pt < 20.0, sf_low_down, np.ones(len(weights._weight)))
                sf_high_up = np.where(pair['obj_1'].pt >= 20.0, sf_high_up, np.ones(len(weights._weight)))
                sf_high_down = np.where(pair['obj_1'].pt >= 20.0, sf_high_down, np.ones(len(weights._weight)))
                if channel == "ee":
                    sf_low_up_2 = evaluator.evaluate(
                        year_arg, "sfup", "RecoBelow20", pair['obj_2'].eta, low_electron_pt_2
                    )
                    sf_low_down_2 = evaluator.evaluate(
                        year_arg, "sfdown", "RecoBelow20", pair['obj_2'].eta, low_electron_pt_2
                    )
                    sf_high_up_2 = evaluator.evaluate(
                        year_arg, "sfup", "RecoAbove20", pair['obj_2'].eta, high_electron_pt_2
                    )
                    sf_high_down_2 = evaluator.evaluate(
                        year_arg, "sfdown", "RecoAbove20", pair['obj_2'].eta, high_electron_pt_2
                    )
                    sf_low_up_2 = np.where(pair['obj_2'].pt < 20.0, sf_low_up_2, np.ones(len(weights._weight)))
                    sf_low_down_2 = np.where(pair['obj_2'].pt < 20.0, sf_low_down_2, np.ones(len(weights._weight)))
                    sf_high_up_2 = np.where(pair['obj_2'].pt >= 20.0, sf_high_up_2, np.ones(len(weights._weight)))
                    sf_high_down_2 = np.where(pair['obj_2'].pt >= 20.0, sf_high_down_2, np.ones(len(weights._weight)))

                    sf_low_up = sf_low_up * sf_low_up_2
                    sf_low_down = sf_low_down * sf_low_down_2
                    sf_high_up = sf_high_up * sf_high_up_2
                    sf_high_down = sf_high_down * sf_high_down_2

                sfup = sf_low_up * sf_high_up
                sfdown = sf_low_down * sf_high_down
            else:
                if year in ["Run3_2022", "Run3_2022EE"]:
                    sf_low_up = evaluator.evaluate(
                        year_arg, "sfup", "RecoBelow20", pair['obj_1'].eta, low_electron_pt_1
                    )
                    sf_low_down = evaluator.evaluate(
                        year_arg, "sfdown", "RecoBelow20", pair['obj_1'].eta, low_electron_pt_1
                    )
                    sf_med_up = evaluator.evaluate(
                        year_arg, "sfup", "Reco20to75", pair['obj_1'].eta, med_electron_pt_1
                    )
                    sf_med_down = evaluator.evaluate(
                        year_arg, "sfdown", "Reco20to75", pair['obj_1'].eta, med_electron_pt_1
                    )
                    sf_high_up = evaluator.evaluate(
                        year_arg, "sfup", "RecoAbove75", pair['obj_1'].eta, high_electron_pt_1
                    )
                    sf_high_down = evaluator.evaluate(
                        year_arg, "sfdown", "RecoAbove75", pair['obj_1'].eta, high_electron_pt_1
                    )
                    sf_low_up = np.where(pair['obj_1'].pt < 20.0, sf_low_up, np.ones(len(weights._weight)))
                    sf_low_down = np.where(pair['obj_1'].pt < 20.0, sf_low_down, np.ones(len(weights._weight)))
                    sf_med_up = np.where((pair['obj_1'].pt >= 20.0) & (pair['obj_1'].pt < 75.0), sf_med_up, np.ones(len(weights._weight)))
                    sf_med_down = np.where((pair['obj_1'].pt >= 20.0) & (pair['obj_1'].pt < 75.0), sf_med_down, np.ones(len(weights._weight)))
                    sf_high_up = np.where(pair['obj_1'].pt >= 75.0, sf_high_up, np.ones(len(weights._weight)))
                    sf_high_down = np.where(pair['obj_1'].pt >= 75.0, sf_high_down, np.ones(len(weights._weight)))
                    if channel == "ee":
                        sf_low_up_2 = evaluator.evaluate(
                            year_arg, "sfup", "RecoBelow20", pair['obj_2'].eta, low_electron_pt_2
                        )
                        sf_low_down_2 = evaluator.evaluate(
                            year_arg, "sfdown", "RecoBelow20", pair['obj_2'].eta, low_electron_pt_2
                        )
                        sf_med_up_2 = evaluator.evaluate(
                            year_arg, "sfup", "Reco20to75", pair['obj_2'].eta, med_electron_pt_2
                        )
                        sf_med_down_2 = evaluator.evaluate(
                            year_arg, "sfdown", "Reco20to75", pair['obj_2'].eta, med_electron_pt_2
                        )
                        sf_high_up_2 = evaluator.evaluate(
                            year_arg, "sfup", "RecoAbove75", pair['obj_2'].eta, high_electron_pt_2
                        )
                        sf_high_down_2 = evaluator.evaluate(
                            year_arg, "sfdown", "RecoAbove75", pair['obj_2'].eta, high_electron_pt_2
                        )
                        sf_low_up_2 = np.where(pair['obj_2'].pt < 20.0, sf_low_up_2, np.ones(len(weights._weight)))
                        sf_low_down_2 = np.where(pair['obj_2'].pt < 20.0, sf_low_down_2, np.ones(len(weights._weight)))
                        sf_med_up_2 = np.where((pair['obj_2'].pt >= 20.0) & (pair['obj_2'].pt < 75.0), sf_med_up_2, np.ones(len(weights._weight)))
                        sf_med_down_2 = np.where((pair['obj_2'].pt >= 20.0) & (pair['obj_2'].pt < 75.0), sf_med_down_2, np.ones(len(weights._weight)))
                        sf_high_up_2 = np.where(pair['obj_2'].pt >= 75.0, sf_high_up_2, np.ones(len(weights._weight)))
                        sf_high_down_2 = np.where(pair['obj_2'].pt >= 75.0, sf_high_down_2, np.ones(len(weights._weight)))

                        sf_low_up = sf_low_up * sf_low_up_2
                        sf_low_down = sf_low_down * sf_low_down_2
                        sf_med_up = sf_med_up * sf_med_up_2
                        sf_med_down = sf_med_down * sf_med_down_2
                        sf_high_up = sf_high_up * sf_high_up_2
                        sf_high_down = sf_high_down * sf_high_down_2

                elif year in ["Run3_2023", "Run3_2023BPix"]:
                    sf_low_up = evaluator.evaluate(
                        year_arg, "sfup", "RecoBelow20", pair['obj_1'].eta, low_electron_pt_1, pair['obj_1'].phi
                    )
                    sf_low_down = evaluator.evaluate(
                        year_arg, "sfdown", "RecoBelow20", pair['obj_1'].eta, low_electron_pt_1, pair['obj_1'].phi
                    )
                    sf_med_up = evaluator.evaluate(
                        year_arg, "sfup", "Reco20to75", pair['obj_1'].eta, med_electron_pt_1, pair['obj_1'].phi
                    )
                    sf_med_down = evaluator.evaluate(
                        year_arg, "sfdown", "Reco20to75", pair['obj_1'].eta, med_electron_pt_1, pair['obj_1'].phi
                    )
                    sf_high_up = evaluator.evaluate(
                        year_arg, "sfup", "RecoAbove75", pair['obj_1'].eta, high_electron_pt_1, pair['obj_1'].phi
                    )
                    sf_high_down = evaluator.evaluate(
                        year_arg, "sfdown", "RecoAbove75", pair['obj_1'].eta, high_electron_pt_1, pair['obj_1'].phi
                    )
                    sf_low_up = np.where(pair['obj_1'].pt < 20.0, sf_low_up, np.ones(len(weights._weight)))
                    sf_low_down = np.where(pair['obj_1'].pt < 20.0, sf_low_down, np.ones(len(weights._weight)))
                    sf_med_up = np.where((pair['obj_1'].pt >= 20.0) & (pair['obj_1'].pt < 75.0), sf_med_up, np.ones(len(weights._weight)))
                    sf_med_down = np.where((pair['obj_1'].pt >= 20.0) & (pair['obj_1'].pt < 75.0), sf_med_down, np.ones(len(weights._weight)))
                    sf_high_up = np.where(pair['obj_1'].pt >= 75.0, sf_high_up, np.ones(len(weights._weight)))
                    sf_high_down = np.where(pair['obj_1'].pt >= 75.0, sf_high_down, np.ones(len(weights._weight)))
                    if channel == "ee":
                        sf_low_up_2 = evaluator.evaluate(
                            year_arg, "sfup", "RecoBelow20", pair['obj_2'].eta, low_electron_pt_2, pair['obj_2'].phi
                        )
                        sf_low_down_2 = evaluator.evaluate(
                            year_arg, "sfdown", "RecoBelow20", pair['obj_2'].eta, low_electron_pt_2, pair['obj_2'].phi
                        )
                        sf_med_up_2 = evaluator.evaluate(
                            year_arg, "sfup", "Reco20to75", pair['obj_2'].eta, med_electron_pt_2, pair['obj_2'].phi
                        )
                        sf_med_down_2 = evaluator.evaluate(
                            year_arg, "sfdown", "Reco20to75", pair['obj_2'].eta, med_electron_pt_2, pair['obj_2'].phi
                        )
                        sf_high_up_2 = evaluator.evaluate(
                            year_arg, "sfup", "RecoAbove75", pair['obj_2'].eta, high_electron_pt_2, pair['obj_2'].phi
                        )
                        sf_high_down_2 = evaluator.evaluate(
                            year_arg, "sfdown", "RecoAbove75", pair['obj_2'].eta, high_electron_pt_2, pair['obj_2'].phi
                        )
                        sf_low_up_2 = np.where(pair['obj_2'].pt < 20.0, sf_low_up_2, np.ones(len(weights._weight)))
                        sf_low_down_2 = np.where(pair['obj_2'].pt < 20.0, sf_low_down_2, np.ones(len(weights._weight)))
                        sf_med_up_2 = np.where((pair['obj_2'].pt >= 20.0) & (pair['obj_2'].pt < 75.0), sf_med_up_2, np.ones(len(weights._weight)))
                        sf_med_down_2 = np.where((pair['obj_2'].pt >= 20.0) & (pair['obj_2'].pt < 75.0), sf_med_down_2, np.ones(len(weights._weight)))
                        sf_high_up_2 = np.where(pair['obj_2'].pt >= 75.0, sf_high_up_2, np.ones(len(weights._weight)))
                        sf_high_down_2 = np.where(pair['obj_2'].pt >= 75.0, sf_high_down_2, np.ones(len(weights._weight)))

                        sf_low_up = sf_low_up * sf_low_up_2
                        sf_low_down = sf_low_down * sf_low_down_2
                        sf_med_up = sf_med_up * sf_med_up_2
                        sf_med_down = sf_med_down * sf_med_down_2
                        sf_high_up = sf_high_up * sf_high_up_2
                        sf_high_down = sf_high_down * sf_high_down_2

                sfup = sf_low_up * sf_med_up * sf_high_up
                sfdown = sf_low_down * sf_med_down * sf_high_down

            sfup = sfup / sf
            sfdown = sfdown / sf
            sf = np.ones(len(weights._weight))

    weights.add(name="Electron_Reco", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights


def Muon_ID(pair, channel, weights, year, meta, is_correction=True, **kwargs):
    # Mapping from year names to folder names and JSON names
    year_mapping = {
        "Run2_2016_HIPM": ("2016preVFP_UL"),
        "Run2_2016": ("2016postVFP_UL"),
        "Run2_2017": ("2017_UL"),
        "Run2_2018": ("2018_UL"),
        "Run3_2022": ("2022_Summer22"),
        "Run3_2022EE": ("2022_Summer22EE"),
        "Run3_2023": ("2023_Summer23"),
        "Run3_2023BPix": ("2023_Summer23BPix"),
    }

    folder_name = year_mapping.get(year, (None, None))
    id_WP = meta.get("Muon_ID_WP")
    json_name = "NUM_" + id_WP + "ID_DEN_TrackerMuons"

    if folder_name:
        path_to_json = os.path.join(os.path.dirname(__file__), f"JSONs/Muon/{folder_name}/muon_Z.json.gz")
        evaluator = correctionlib.CorrectionSet.from_file(path_to_json)[json_name]
    else:
        raise ValueError(f"Year {year} not supported for muon ID.")

    if is_correction:
        sf = np.ones(len(weights._weight))
        sfup, sfdown = None, None

        if channel == "mt":
            sf *= evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "nominal"
            )
        elif channel == "em":
            sf *= evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "nominal"
            )
        elif channel == "mm":
            sf_lead = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "nominal"
            )
            sf_sublead = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "nominal"
            )
            sf *= (sf_lead * sf_sublead)

    else:
        sf = np.ones(len(weights._weight))
        sfup, sfdown = np.ones(len(weights._weight)), np.ones(len(weights._weight))
        if channel == "mt":
            sf = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "nominal"
            )
            sfup = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "systup"
            )
            sfdown = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "systdown"
            )

        elif channel == "em":
            sf = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "nominal"
            )
            sfup = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "systup"
            )
            sfdown = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "systdown"
            )

        elif channel == "mm":
            sf = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "nominal"
            )
            sf = sf * evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "nominal"
            )
            sf_lead_up = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "systup"
            )
            sf_sublead_up = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "systup"
            )
            sf_lead_down = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "systdown"
            )
            sf_sublead_down = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "systdown"
            )

            sfup = sf_lead_up * sf_sublead_up
            sfdown = sf_lead_down * sf_sublead_down

        sfup = sfup / sf
        sfdown = sfdown / sf
        sf = np.ones(len(weights._weight))

    weights.add(name="Muon_ID", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights


def Muon_Isolation(pair, channel, weights, year, is_correction=True, **kwargs):
    # Mapping from year names to folder names and JSON names
    year_mapping = {
        "Run2_2016_HIPM": ("2016preVFP_UL"),
        "Run2_2016": ("2016postVFP_UL"),
        "Run2_2017": ("2017_UL"),
        "Run2_2018": ("2018_UL"),
        "Run3_2022": ("2022_Summer22"),
        "Run3_2022EE": ("2022_Summer22EE"),
        "Run3_2023": ("2023_Summer23"),
        "Run3_2023BPix": ("2023_Summer23BPix"),
    }

    folder_name = year_mapping.get(year, (None, None))
    json_name = "NUM_TightPFIso_DEN_MediumID"

    if folder_name:
        path_to_json = os.path.join(os.path.dirname(__file__), f"JSONs/Muon/{folder_name}/muon_Z.json.gz")
        evaluator = correctionlib.CorrectionSet.from_file(path_to_json)[json_name]
    else:
        raise ValueError(f"Year {year} not supported for muon Isolation.")

    if is_correction:
        sf = np.ones(len(weights._weight))
        sfup, sfdown = None, None

        if channel == "mt":
            sf *= evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "nominal"
            )
        elif channel == "em":
            sf *= evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "nominal"
            )
        elif channel == "mm":
            sf_lead = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "nominal"
            )
            sf_sublead = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "nominal"
            )
            sf *= (sf_lead * sf_sublead)

    else:
        sf = np.ones(len(weights._weight))
        sfup, sfdown = np.ones(len(weights._weight)), np.ones(len(weights._weight))
        if channel == "mt":
            sf = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "nominal"
            )
            sfup = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "systup"
            )
            sfdown = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "systdown"
            )

        elif channel == "em":
            sf = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "nominal"
            )
            sfup = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "systup"
            )
            sfdown = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "systdown"
            )

        elif channel == "mm":
            sf = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "nominal"
            )
            sf = sf * evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "nominal"
            )
            sf_lead_up = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "systup"
            )
            sf_sublead_up = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "systup"
            )
            sf_lead_down = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "systdown"
            )
            sf_sublead_down = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "systdown"
            )

            sfup = sf_lead_up * sf_sublead_up
            sfdown = sf_lead_down * sf_sublead_down

        sfup = sfup / sf
        sfdown = sfdown / sf
        sf = np.ones(len(weights._weight))

    weights.add(name="Muon_Isolation", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights


def Muon_Reco(pair, channel, weights, year, is_correction=True, **kwargs):
    # Mapping from year names to folder names and JSON names
    year_mapping = {
        "Run2_2016_HIPM": ("2016preVFP_UL"),
        "Run2_2016": ("2016postVFP_UL"),
        "Run2_2017": ("2017_UL"),
        "Run2_2018": ("2018_UL"),
        "Run3_2022": ("2022_Summer22"),
        "Run3_2022EE": ("2022_Summer22EE"),
        "Run3_2023": ("2023_Summer23"),
        "Run3_2023BPix": ("2023_Summer23BPix"),
    }

    folder_name = year_mapping.get(year, (None, None))
    json_name = "NUM_GlobalMuons_DEN_genTracks"

    if folder_name:
        path_to_json = os.path.join(os.path.dirname(__file__), f"JSONs/Muon/{folder_name}/muon_Z.json.gz")
        if "Run3" not in year:
            evaluator = correctionlib.CorrectionSet.from_file(path_to_json)[json_name]
    else:
        raise ValueError(f"Year {year} not supported for muon reco.")

    if is_correction:
        # Only calculate for UL (Not ready for 2022 onwards yet)
        if "Run3" not in year:
            if channel == "mt":
                sf = evaluator.evaluate(
                    np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "nominal"
                )
            elif channel == "em":
                sf = evaluator.evaluate(
                    np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "nominal"
                )
            elif channel == "mm":
                sf_lead = evaluator.evaluate(
                    np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "nominal"
                )
                sf_sublead = evaluator.evaluate(
                    np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "nominal"
                )
                sf = sf_lead * sf_sublead
        else:
            sf = np.ones(len(weights._weight))

        sfup, sfdown = None, None

    weights.add(name="Muon_Reco", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights
