{
  "name": "tau_es_dm_UL2016_postVFP",
  "description": "DM-dependent tau energy scale in UL2016_postVFP, to be applied to reconstructed tau_h Lorentz vector (pT, mass and energy) in simulated data",
  "version": 0,
  "inputs": [
    { "name": "pt",
      "type": "real",
      "description": "Reconstructed tau pT"
    },
    { "name": "eta",
      "type": "real",
      "description": "Reconstructed tau eta"
    },
    { "name": "dm",
      "type": "int",
      "description": "Reconstructed tau decay mode: 0, 1, 2, 10, 11"
    },
    { "name": "genmatch",
      "type": "int",
      "description": "genmatch: 0 or 6 = unmatched or jet, 1 or 3 = electron, 2 or 4 = muon, 5 = real tau"
    },
    { "name": "id",
      "type": "string",
      "description": "Tau ID: DeepTau2017v2p1"
    },
    { "name": "syst",
      "type": "string",
      "description": "Systematic variation: 'nom', 'up', 'down'"
    }
  ],
  "output": {
    "name": "tes",
    "type": "real",
    "description": "tau energy scale"
  },
  "data": {
    "nodetype": "category",
    "input": "id",
    "content": [
      { "key": "DeepTau2017v2p1",
        "value": {
          "nodetype": "transform",
          "input": "genmatch",
          "rule": {
            "nodetype": "category",
            "input": "genmatch",
            "content": [
              { "key": 0, "value": 6.0 },
              { "key": 1, "value": 1.0 },
              { "key": 2, "value": 2.0 },
              { "key": 3, "value": 1.0 },
              { "key": 4, "value": 2.0 },
              { "key": 5, "value": 5.0 },
              { "key": 6, "value": 6.0 }
            ]
          },
          "content": {
            "nodetype": "category",
            "input": "genmatch",
            "content": [
              { "key": 1,
                "value": {
                  "nodetype": "category",
                  "input": "dm",
                  "content": [
                    { "key": 0,
                      "value": {
                        "nodetype": "transform",
                        "input": "eta",
                        "rule": {
                          "nodetype": "formula",
                          "expression": "abs(x)",
                          "parser": "TFormula",
                          "variables": [ "eta" ]
                        },
                        "content": {
                          "nodetype": "binning",
                          "input": "eta",
                          "edges": [ 0.0, 1.5, 2.5 ],
                          "content": [
                            { "nodetype": "category",
                              "input": "syst",
                              "content": [
                                { "key": "nom", "value": 1.00679 },
                                { "key": "up", "value": 1.01485 },
                                { "key": "down", "value": 0.99697 }
                              ]
                            },
                            { "nodetype": "category",
                              "input": "syst",
                              "content": [
                                { "key": "nom", "value": 0.965 },
                                { "key": "up", "value": 0.98308 },
                                { "key": "down", "value": 0.95398 }
                              ]
                            }
                          ],
                          "flow": "clamp"
                        }
                      }
                    },
                    { "key": 1,
                      "value": {
                        "nodetype": "transform",
                        "input": "eta",
                        "rule": {
                          "nodetype": "formula",
                          "expression": "abs(x)",
                          "parser": "TFormula",
                          "variables": [ "eta" ]
                        },
                        "content": {
                          "nodetype": "binning",
                          "input": "eta",
                          "edges": [ 0.0, 1.5, 2.5 ],
                          "content": [
                            { "nodetype": "category",
                              "input": "syst",
                              "content": [
                                { "key": "nom", "value": 1.03389 },
                                { "key": "up", "value": 1.04557 },
                                { "key": "down", "value": 1.00914 }
                              ]
                            },
                            { "nodetype": "category",
                              "input": "syst",
                              "content": [
                                { "key": "nom", "value": 1.05 },
                                { "key": "up", "value": 1.1157 },
                                { "key": "down", "value": 0.99306 }
                              ]
                            }
                          ],
                          "flow": "clamp"
                        }
                      }
                    },
                    { "key": 2,
                      "value": {
                        "nodetype": "transform",
                        "input": "eta",
                        "rule": {
                          "nodetype": "formula",
                          "expression": "abs(x)",
                          "parser": "TFormula",
                          "variables": [ "eta" ]
                        },
                        "content": {
                          "nodetype": "binning",
                          "input": "eta",
                          "edges": [ 0.0, 1.5, 2.5 ],
                          "content": [
                            { "nodetype": "category",
                              "input": "syst",
                              "content": [
                                { "key": "nom", "value": 1.03389 },
                                { "key": "up", "value": 1.04557 },
                                { "key": "down", "value": 1.00914 }
                              ]
                            },
                            { "nodetype": "category",
                              "input": "syst",
                              "content": [
                                { "key": "nom", "value": 1.05 },
                                { "key": "up", "value": 1.1157 },
                                { "key": "down", "value": 0.99306 }
                              ]
                            }
                          ],
                          "flow": "clamp"
                        }
                      }
                    },
                    { "key": 10, "value": 1.0 },
                    { "key": 11, "value": 1.0 }
                  ]
                }
              },
              { "key": 2,
                "value": {
                  "nodetype": "category",
                  "input": "syst",
                  "content": [
                    { "key": "nom", "value": 1.0 },
                    { "key": "up", "value": 1.01 },
                    { "key": "down", "value": 0.99 }
                  ]
                }
              },
              { "key": 5,
                "value": {
                  "nodetype": "category",
                  "input": "dm",
                  "content": [
                    { "key": 0,
                      "value": {
                        "nodetype": "category",
                        "input": "syst",
                        "content": [
                          { "key": "nom", "value": 0.993 },
                          { "key": "up",
                            "value": {
                              "nodetype": "binning",
                              "input": "pt",
                              "edges": [ 0.0, 34.0, 170.0, 1000.0 ],
                              "content": [
                                1.002,
                                { "nodetype": "formula",
                                  "expression": "0.99675+0.000154412*x",
                                  "parser": "TFormula",
                                  "variables": [ "pt" ]
                                },
                                1.023
                              ],
                              "flow": "clamp"
                            }
                          },
                          { "key": "down",
                            "value": {
                              "nodetype": "binning",
                              "input": "pt",
                              "edges": [ 0.0, 34.0, 170.0, 1000.0 ],
                              "content": [
                                0.984,
                                { "nodetype": "formula",
                                  "expression": "0.98925-0.000154412*x",
                                  "parser": "TFormula",
                                  "variables": [ "pt" ]
                                },
                                0.963
                              ],
                              "flow": "clamp"
                            }
                          }
                        ]
                      }
                    },
                    { "key": 1,
                      "value": {
                        "nodetype": "category",
                        "input": "syst",
                        "content": [
                          { "key": "nom", "value": 0.991 },
                          { "key": "up",
                            "value": {
                              "nodetype": "binning",
                              "input": "pt",
                              "edges": [ 0.0, 34.0, 170.0, 1000.0 ],
                              "content": [
                                0.998,
                                { "nodetype": "formula",
                                  "expression": "0.99475+9.55882e-05*x",
                                  "parser": "TFormula",
                                  "variables": [ "pt" ]
                                },
                                1.011
                              ],
                              "flow": "clamp"
                            }
                          },
                          { "key": "down",
                            "value": {
                              "nodetype": "binning",
                              "input": "pt",
                              "edges": [ 0.0, 34.0, 170.0, 1000.0 ],
                              "content": [
                                0.984,
                                { "nodetype": "formula",
                                  "expression": "0.98725-9.55882e-05*x",
                                  "parser": "TFormula",
                                  "variables": [ "pt" ]
                                },
                                0.971
                              ],
                              "flow": "clamp"
                            }
                          }
                        ]
                      }
                    },
                    { "key": 2,
                      "value": {
                        "nodetype": "category",
                        "input": "syst",
                        "content": [
                          { "key": "nom", "value": 0.991 },
                          { "key": "up",
                            "value": {
                              "nodetype": "binning",
                              "input": "pt",
                              "edges": [ 0.0, 34.0, 170.0, 1000.0 ],
                              "content": [
                                0.998,
                                { "nodetype": "formula",
                                  "expression": "0.99475+9.55882e-05*x",
                                  "parser": "TFormula",
                                  "variables": [ "pt" ]
                                },
                                1.011
                              ],
                              "flow": "clamp"
                            }
                          },
                          { "key": "down",
                            "value": {
                              "nodetype": "binning",
                              "input": "pt",
                              "edges": [ 0.0, 34.0, 170.0, 1000.0 ],
                              "content": [
                                0.984,
                                { "nodetype": "formula",
                                  "expression": "0.98725-9.55882e-05*x",
                                  "parser": "TFormula",
                                  "variables": [ "pt" ]
                                },
                                0.971
                              ],
                              "flow": "clamp"
                            }
                          }
                        ]
                      }
                    },
                    { "key": 10,
                      "value": {
                        "nodetype": "category",
                        "input": "syst",
                        "content": [
                          { "key": "nom", "value": 1.001 },
                          { "key": "up",
                            "value": {
                              "nodetype": "binning",
                              "input": "pt",
                              "edges": [ 0.0, 34.0, 170.0, 1000.0 ],
                              "content": [
                                1.008,
                                { "nodetype": "formula",
                                  "expression": "1.00675+3.67647e-05*x",
                                  "parser": "TFormula",
                                  "variables": [ "pt" ]
                                },
                                1.013
                              ],
                              "flow": "clamp"
                            }
                          },
                          { "key": "down",
                            "value": {
                              "nodetype": "binning",
                              "input": "pt",
                              "edges": [ 0.0, 34.0, 170.0, 1000.0 ],
                              "content": [
                                0.994,
                                { "nodetype": "formula",
                                  "expression": "0.99525-3.67647e-05*x",
                                  "parser": "TFormula",
                                  "variables": [ "pt" ]
                                },
                                0.989
                              ],
                              "flow": "clamp"
                            }
                          }
                        ]
                      }
                    },
                    { "key": 11,
                      "value": {
                        "nodetype": "category",
                        "input": "syst",
                        "content": [
                          { "key": "nom", "value": 0.997 },
                          { "key": "up",
                            "value": {
                              "nodetype": "binning",
                              "input": "pt",
                              "edges": [ 0.0, 34.0, 170.0, 1000.0 ],
                              "content": [
                                1.013,
                                { "nodetype": "formula",
                                  "expression": "1.01025+8.08824e-05*x",
                                  "parser": "TFormula",
                                  "variables": [ "pt" ]
                                },
                                1.024
                              ],
                              "flow": "clamp"
                            }
                          },
                          { "key": "down",
                            "value": {
                              "nodetype": "binning",
                              "input": "pt",
                              "edges": [ 0.0, 34.0, 170.0, 1000.0 ],
                              "content": [
                                0.981,
                                { "nodetype": "formula",
                                  "expression": "0.98375-8.08824e-05*x",
                                  "parser": "TFormula",
                                  "variables": [ "pt" ]
                                },
                                0.97
                              ],
                              "flow": "clamp"
                            }
                          }
                        ]
                      }
                    }
                  ]
                }
              },
              { "key": 6, "value": 1.0 }
            ]
          }
        }
      }
    ]
  }
}