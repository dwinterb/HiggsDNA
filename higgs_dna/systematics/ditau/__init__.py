from higgs_dna.systematics.ditau.tau_systematics import Tau_EnergyScale, Tau_EnergyScale_forTauIDSFs

from higgs_dna.systematics.ditau.trigger_systematics import Trigger

from higgs_dna.systematics.ditau.event_weight_systematics import (
    Pileup,
    Zpt_Reweighting,
    Top_pt_Reweighting,
    ggH_QuarkMass_Effects,
    Tau_ID,
    Tau_e_FakeRate,
    Tau_mu_FakeRate,
    Electron_ID,
    Electron_Reco,
    Muon_ID,
    Muon_Isolation,
    Muon_Reco,
)

from higgs_dna.systematics.ditau.electron_systematics import (
    Electron_Scale,
    Electron_Smearing,
)

from higgs_dna.systematics.ditau.jet_systematics_json import jerc_jet

from functools import partial
import logging

logger = logging.getLogger(__name__)

# using add_systematic function of coffea.nanoevents.methods.nanoaod objects as Photon to store object systematics in addition to nominal objects
object_systematics = {
    "Tau_EnergyScale": {
        "object": "Tau",
        "args": {
            "kind": "UpDownSystematic",
            "coffea": False,
            "varying_function": partial(Tau_EnergyScale, is_correction=False)
        }
    },
    "Tau_EnergyScale_forTauIDSFs": {
        "object": "Tau",
        "args": {
            "kind": "UpDownSystematic",
            "coffea": False,
            "varying_function": partial(Tau_EnergyScale_forTauIDSFs, is_correction=False)
        }
    },
    "Electron_Scale": {
        "object": "Electron",
        "args": {
            "kind": "UpDownSystematic",
            "coffea": False,
            "varying_function": partial(Electron_Scale, is_correction=False),
        },
    },
    "Electron_Smearing": {
        "object": "Electron",
        "args": {
            "kind": "UpDownSystematic",
            "coffea": False,
            "varying_function": partial(Electron_Smearing, is_correction=False),
        },
    },
}

# functions correcting nominal object quantities to be placed here
# dict containing "name": varying_function
object_corrections = {
    "Tau_EnergyScale": partial(Tau_EnergyScale, is_correction=True),
    "Tau_EnergyScale_forTauIDSFs": partial(Tau_EnergyScale_forTauIDSFs, is_correction=True),
    "Electron_Scale": partial(Electron_Scale, is_correction=True),
    "Electron_Smearing": partial(Electron_Smearing, is_correction=True),
}

# functions adding systematic variations to event weights to be placed here
# dict containing "name": varying_function
weight_systematics = {
    "Pileup": partial(Pileup, is_correction=False),
    "Tau_ID": partial(Tau_ID, is_correction=False),
    "Tau_e_FakeRate": partial(Tau_e_FakeRate, is_correction=False),
    "Tau_mu_FakeRate": partial(Tau_mu_FakeRate, is_correction=False),
    "Zpt_Reweighting": partial(Zpt_Reweighting, is_correction=False),
    "Top_pt_Reweighting": partial(Top_pt_Reweighting, is_correction=False),
    "ggH_QuarkMass_Effects": partial(ggH_QuarkMass_Effects, is_correction=False),
    "Electron_ID": partial(Electron_ID, is_correction=False),
    "Electron_Reco": partial(Electron_Reco, is_correction=False),
    "Muon_ID": partial(Muon_ID, is_correction=False),
    "Muon_Isolation": partial(Muon_Isolation, is_correction=False),
    "Muon_Reco": partial(Muon_Reco, is_correction=False),
    "Trigger": partial(Trigger, is_correction=False),
}

# functions correcting nominal event weights to be placed here
# dict containing "name": varying_function
weight_corrections = {
    "Pileup": partial(Pileup, is_correction=True),
    "Tau_ID": partial(Tau_ID, is_correction=True),
    "Tau_e_FakeRate": partial(Tau_e_FakeRate, is_correction=True),
    "Tau_mu_FakeRate": partial(Tau_mu_FakeRate, is_correction=True),
    "Zpt_Reweighting": partial(Zpt_Reweighting, is_correction=True),
    "Top_pt_Reweighting": partial(Top_pt_Reweighting, is_correction=True),
    "ggH_QuarkMass_Effects": partial(ggH_QuarkMass_Effects, is_correction=True),
    "Electron_ID": partial(Electron_ID, is_correction=True),
    "Electron_Reco": partial(Electron_Reco, is_correction=True),
    "Muon_ID": partial(Muon_ID, is_correction=True),
    "Muon_Isolation": partial(Muon_Isolation, is_correction=True),
    "Muon_Reco": partial(Muon_Reco, is_correction=True),
    "Trigger": partial(Trigger, is_correction=True),
}


def check_corr_syst(corrections, systematics, logger):
    """
    This function is a sanity check for the choice of systematics and corrections which the user wants to process.
    It ensures that systematic variations of a correction can only be processed when the correction itself is applied.
    """

    for chosen_syst in systematics:
        if (
            chosen_syst in weight_corrections.keys()
            and chosen_syst not in corrections
        ) or (
            chosen_syst in object_corrections.keys()
            and chosen_syst not in corrections
        ):
            # scale unc. will be applied to MC while the correction is applied to data. Exception.
            if "scale" in chosen_syst.lower():
                continue
            logger.info(
                f"Requested to evaluate systematic variation {chosen_syst} without applying the corresponding correction. \nThis is not intended.\nExiting."
            )
            exit()


def add_jme_corr_syst(corrections_dict, systematics_dict, logger):
    corrections_dict.update(
        {
            # jerc for MC
            "Jet_EnergyCorrection": partial(jerc_jet, pt=None, apply_jec=True),
            "Jet_EnergyCorrection_syst": partial(jerc_jet, pt=None, apply_jec=True, jec_syst=True),
            "Jet_EnergyCorrection_regrouped_syst": partial(
                jerc_jet, pt=None, apply_jec=True, jec_syst=True, split_jec_syst=True
            ),
            "Jet_EnergyResolutionCorrection": partial(jerc_jet, pt=None, apply_jec=True, apply_jer=True),
            "Jet_EnergyResolutionCorrection_syst": partial(
                jerc_jet,
                pt=None,
                apply_jec=True,
                jec_syst=True,
                apply_jer=True,
                jer_syst=True,
            ),
            "Jet_EnergyResolutionCorrection_regrouped_syst": partial(
                jerc_jet,
                pt=None,
                apply_jec=True,
                jec_syst=True,
                split_jec_syst=True,
                apply_jer=True,
                jer_syst=True,
            )
        }
    )
    logger.info(
        f"""_summary_

    Available correction keys:
    {corrections_dict.keys()}
    Available systematic keys:
    {systematics_dict.keys()}
    """
    )
    return corrections_dict, systematics_dict


object_corrections, object_systematics = add_jme_corr_syst(
    object_corrections, object_systematics, logger
)
