# Create a json file containing list of files

For samples available via DAS, add the name of the samples to a text file. An example text file can be found in example/dyzll.txt

Create the json file using:
```
fetch.py -i examples/ditau_analysis/dyzll.txt -w Eurasia
``` 

# An example of how to run locally the basic ditau selection

Renew grid certificate:

```
voms-proxy-init --voms cms --valid 96:00 --out ~/cms.proxy
```

Run simple ditau selection:
 
```
run_analysis.py --json-analysis examples/ditau_analysis/simple_ditau_analysis.json --dump output --executor futures --voms ~/cms.proxy --skipbadfiles --max 1 --chunk 50000 --debug
```

# Make a simple plot of the dimuon invariant mass

The following script produces simple 1D plots, e.g to produce a plot of the visible mass of the ditau candidates use:
```
python examples/ditau_analysis/example_plot.py output/mm/DYJetsToLL_M-50/nominal/05a3fe10-f1f8-11ed-90b2-7f0110acbeef_%2FEvents%3B1_0-50089.parquet m_vis 100,50,200
```
