import sys
import pandas
import mplhep as hep
import matplotlib as plt
plt.style.use(hep.style.ROOT)

def plot(df, column, name='plot.pdf', bins=100, x_lo=None, x_hi=None):
  df_1 = df[column]

  fig, ax = plt.pyplot.subplots()

  if x_lo: low = float(x_lo) 
  else: low = df_1.min()
  if x_hi: high = float(x_hi) 
  else: high = df_1.max()

  ax.hist(df_1, bins=int(bins), range=(low,high))
  plt.pyplot.savefig(name)


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage: python script.py <parquet_file> <column>")
        sys.exit(1)

    parquet_file = sys.argv[1]
    column = sys.argv[2]
    df=pandas.read_parquet(parquet_file,engine="pyarrow")

    if len(sys.argv)>=3:
      bins, x_lo, x_hi = sys.argv[3].split(',')     
    else: bins, x_lo, x_hi = 100, None, None

    name=column+'.pdf'
    print (column,name, bins, x_lo, x_hi) 
    plot(df,column,name,bins,x_lo,x_hi)  





















