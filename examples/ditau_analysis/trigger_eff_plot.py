import numpy as np
import sys
import pandas
import mplhep as hep
import matplotlib as plt
plt.style.use(hep.style.ROOT)

def plot(df, column, trigger, name='plot.pdf', bins=100, x_lo=None, x_hi=None):
  df_1 = df[column]
  df_numerator = df_1[df[trigger] == True]

  fig, ax = plt.pyplot.subplots()

  if x_lo: low = float(x_lo) 
  else: low = df_1.min()
  if x_hi: high = float(x_hi) 
  else: high = df_1.max()

  numerator_hist, _ = np.histogram(df_numerator, bins=int(bins), range=(low, high))
  denominator_hist, _ = np.histogram(df_1, bins=int(bins), range=(low, high))

  # Calculate the ratio of histograms
  ratio_hist = numerator_hist / denominator_hist

  bin_centers = 0.5 * (_[:-1] + _[1:])
  ax.plot(bin_centers, ratio_hist, marker='o')
  ax.set_xlabel(column)
  ax.set_ylabel('Efficiency')

  plt.pyplot.savefig(name)


if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("Usage: python script.py <parquet_file> <column> <trigger>")
        sys.exit(1)

    parquet_file = sys.argv[1]
    column = sys.argv[2]
    trigger = sys.argv[3]
    df=pandas.read_parquet(parquet_file,engine="pyarrow")

    if len(sys.argv)>=4:
      bins, x_lo, x_hi = sys.argv[4].split(',')     
    else: bins, x_lo, x_hi = 100, None, None

    name=column+'_trigger_eff.pdf'
    print (column,name, bins, x_lo, x_hi) 
    plot(df,column,trigger,name,bins,x_lo,x_hi)  
