import argparse
import os
import yaml
import fnmatch


def getParams(args,lumi):
    input_xs = os.path.join(os.getcwd(), 'scripts/ditau/config/cross_sections.yaml')
    with open(input_xs, 'r') as f:
        cross_sections = yaml.safe_load(f)

    input_eff = os.path.join(os.getcwd(), f'scripts/ditau/config/{args.year}/effective_events.yaml')
    with open(input_eff, 'r') as f:
        effective_events = yaml.safe_load(f)

    input_filter_efficiencies = os.path.join(os.getcwd(), f'scripts/ditau/config/{args.year}/filter_efficiencies.yaml')
    with open(input_filter_efficiencies, 'r') as f:
        filter_efficiencies = yaml.safe_load(f)

    Run_ID = args.year.split('_')[0]
    kfactors = cross_sections[Run_ID]['kfactors']

    results = {}
    results['lumi'] = lumi
    for process in cross_sections[Run_ID]["processes"]:
        xs = cross_sections[Run_ID]["processes"][process]["cross_sections"]
        kfactor = cross_sections[Run_ID]["processes"][process]["kfactor"]
        if kfactor in kfactors:
            kfactor = kfactors[kfactor]
        kfactor = str(kfactor)
        for sample in list(xs.keys()):
            results[sample] = {}
            results[sample]['xs'] = xs[sample] * eval(kfactor)
            if sample in effective_events:
                results[sample]['eff'] = effective_events[sample]
            else:
                results[sample]['eff'] = 0.0
            if sample in filter_efficiencies:
                print("Filter efficiency for", sample, "is", filter_efficiencies[sample])
                results[sample]['filter_efficiency'] = filter_efficiencies[sample]
            else:
                results[sample]['filter_efficiency'] = 1.0

    for sample in results:
        if "DYto2L" in sample or "WtoLNu" in sample or "_ext" in sample or sample == "lumi":
            continue
        count_extensions = sum(1 for key in results if fnmatch.fnmatch(key, f'{sample}_ext*'))
        total_eff = results[sample]['eff']
        for index in range(count_extensions):
            total_eff += results[f'{sample}_ext{index+1}']['eff']
        results[sample]['eff'] = total_eff
        for index in range(count_extensions):
            results[f'{sample}_ext{index+1}']['eff'] = total_eff

    return results


def reset_params(results,reset_dict):
    for process, processes_list in reset_dict.items():
        inclusive_xs = results[processes_list[0]]['xs']
        count_extensions = sum(1 for key in results if fnmatch.fnmatch(key, f'{processes_list[0]}_ext*'))
        inclusive_eff = results[f'{processes_list[0]}']['eff']
        for index in range(count_extensions):
            inclusive_eff += results[f'{processes_list[0]}_ext{index+1}']['eff']

        for item in processes_list:
            results[item]['xs'] = inclusive_xs
            results[item]['eff'] = inclusive_eff

        for index in range(count_extensions):
            results[f'{processes_list[0]}_ext{index+1}']['xs'] = inclusive_xs
            results[f'{processes_list[0]}_ext{index+1}']['eff'] = inclusive_eff


def dump_params(args,results):
    yaml_output_path = os.path.join(os.getcwd(), f'scripts/ditau/config/{args.year}/params.yaml')
    with open(yaml_output_path, 'w') as yaml_file:
        yaml.dump(results, yaml_file)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--year', type=str, help='The year to process')

    args = parser.parse_args()

    path_to_lumi = os.path.join(os.getcwd(), 'scripts/ditau/config/lumi.yaml')
    with open(path_to_lumi, 'r') as f:
        lumi = yaml.safe_load(f)

    results = getParams(args,lumi[args.year])

    reset_dict = {
        "DY_LO": ["DYto2L_M_50_madgraphMLM","DYto2L_M_50_1J_madgraphMLM","DYto2L_M_50_2J_madgraphMLM","DYto2L_M_50_3J_madgraphMLM","DYto2L_M_50_4J_madgraphMLM"],
        "WJ_LO": ["WtoLNu_madgraphMLM","WtoLNu_1J_madgraphMLM","WtoLNu_2J_madgraphMLM","WtoLNu_3J_madgraphMLM","WtoLNu_4J_madgraphMLM"],
        "DY-NLO": ["DYto2L_M_50_amcatnloFXFX","DYto2L_M_50_amcatnloFXFX_ext1","DYto2L_M_50_0J_amcatnloFXFX","DYto2L_M_50_1J_amcatnloFXFX","DYto2L_M_50_2J_amcatnloFXFX", 'DYto2L_M_50_PTLL_40to100_1J_amcatnloFXFX', 'DYto2L_M_50_PTLL_100to200_1J_amcatnloFXFX', 'DYto2L_M_50_PTLL_200to400_1J_amcatnloFXFX', 'DYto2L_M_50_PTLL_400to600_1J_amcatnloFXFX', 'DYto2L_M_50_PTLL_600_1J_amcatnloFXFX', 'DYto2L_M_50_PTLL_40to100_2J_amcatnloFXFX', 'DYto2L_M_50_PTLL_100to200_2J_amcatnloFXFX', 'DYto2L_M_50_PTLL_200to400_2J_amcatnloFXFX', 'DYto2L_M_50_PTLL_400to600_2J_amcatnloFXFX', 'DYto2L_M_50_PTLL_600_2J_amcatnloFXFX'],
    }
    reset_params(results,reset_dict)
    dump_params(args,results)


if __name__ == "__main__":
    main()
