import os
import glob
import subprocess
import sys
import shutil

# Initialize counters for total and failed job counts
count = 0
total = 0

# Initialize a VOMS proxy for CMS
subprocess.run(["voms-proxy-init", "--voms", "cms", "--valid", "192:00", "--out", os.path.expanduser("~/cms.proxy")])

cwd = os.getcwd()
dir = os.path.join(cwd, sys.argv[1])

# Make directory resubmitted jobs if it does not exist
resub_dir = os.path.join(dir, "resubmitted_jobs")
failed_dir = os.path.join(resub_dir, "failed_jobs")
os.makedirs(failed_dir, exist_ok=True)


# Function to check if the output log contains "Processing 100%"
def check_completion(output_file):
    try:
        with open(output_file, 'r') as f:
            if "Processing 100%" in f.read():
                return True
    except FileNotFoundError:
        pass
    return False


# Function to process a job
def process_job(subfile, job_id, num_jobs):
    global count, total
    base_dir = os.path.dirname(subfile)

    # Loop through all jobs in the .sub file
    print("Checking jobs for", job_id)
    for i in range(num_jobs):
        output_file_pattern = os.path.join(base_dir, f"{job_id}.*.{i}.out")
        error_file_pattern = os.path.join(base_dir, f"{job_id}.*.{i}.err")

        actual_output_files = glob.glob(output_file_pattern)
        actual_error_files = glob.glob(error_file_pattern)

        if not actual_output_files or not actual_error_files:
            print(f"Output or error file for job {i} not found, skipping.")
            continue

        actual_output_file = actual_output_files[0]
        actual_error_file = actual_error_files[0]

        if check_completion(actual_output_file):
            continue
        else:
            count += 1
            # Print the job ID and job number of the failed job
            print(f"Job {job_id} failed: {i}")
            # Move output and error files to failed_jobs directory
            shutil.move(actual_output_file, os.path.join(failed_dir, os.path.basename(actual_output_file)))
            shutil.move(actual_error_file, os.path.join(failed_dir, os.path.basename(actual_error_file)))
            # Create a new submission file for the failed job
            resubmit_filename = os.path.join(resub_dir, f"resubmit_{count}.sub")
            with open(resubmit_filename, 'w') as resubmit_file:
                resubmit_file.write(f"""
executable = {os.path.join(base_dir, f"{job_id}.sh")}
arguments = {i}
output = {os.path.join(base_dir, f"{job_id}.$(ClusterId).{i}.out")}
error = {os.path.join(base_dir, f"{job_id}.$(ClusterId).{i}.err")}
log = {os.path.join(base_dir, f"{job_id}.$(ClusterId).log")}
RequestMemory = 8192
getenv = True
+MaxRuntime = 10800
queue 1
                """.strip())

            subprocess.run(["condor_submit", resubmit_filename])
    total += num_jobs


for subfile in glob.glob(os.path.join(dir, "*.sub")):
    job_id = os.path.basename(subfile).replace('.sub', '')
    # Extract the number of jobs from the queue line in the .sub file
    with open(subfile, 'r') as f:
        for line in f:
            if line.startswith('queue'):
                num_jobs = int(line.split()[1])
                break

    process_job(subfile, job_id, num_jobs)

print(f"{count} jobs out of {total} failed")
