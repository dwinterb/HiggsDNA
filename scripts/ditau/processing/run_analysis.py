#!/usr/bin/env python
import json
from importlib import resources
import os
import sys
import time

import numpy as np
import uproot
from coffea import processor
from coffea.util import save
from coffea.nanoevents import NanoAODSchema, BaseSchema

from higgs_dna.utils.runner_utils import get_main_parser, get_proxy
from higgs_dna.workflows import workflows
from higgs_dna.workflows import taggers as all_taggers
from higgs_dna.metaconditions import metaconditions as all_metaconditions
from higgs_dna.utils.logger_utils import setup_logger
from higgs_dna.systematics.ditau import check_corr_syst


def validate(file):
    try:
        fin = uproot.open(file)
        return fin["Events"].num_entries
    except Exception:
        print(f"Corrupted file: {file}")
        return


def run_executor(_args, executor, sample_dict, processor_instance, EventsNotSelected=False):
    schema_list = {
        "nano": NanoAODSchema,
        "base": BaseSchema,
    }
    run = processor.Runner(
        executor=executor,
        schema=schema_list[_args.schema],
        chunksize=_args.chunk,
        maxchunks=_args.max,
        format=_args.format,
        skipbadfiles=_args.skipbadfiles,
    )

    output = run(
        sample_dict,
        treename="EventsNotSelected" if EventsNotSelected else "Events",
        processor_instance=processor_instance,
    )

    # save event number to json
    if _args.save is not None:
        save(output, _args.save)


if __name__ == "__main__":
    parser = get_main_parser()
    args = parser.parse_args()

    # Setup logger
    if args.debug:
        log_level = "DEBUG"
    else:
        log_level = "INFO"
    logger = setup_logger(level=log_level)
    logger.info("Start production")

    # Here we assume that all the keys are there, otherwise an exception will be raised
    analysis_name = args.json_analysis_file.split("/")[-1].split(".")[0]
    with open(args.json_analysis_file) as f:
        analysis = json.load(f)
    workflow = analysis["workflow"]
    taggers = analysis["taggers"] if analysis["taggers"] else None
    metaconditions = analysis["metaconditions"]
    samplejson = analysis["samplejson"]
    run_effective = analysis["Run_Effective"]
    EventsNotSelected = analysis["EventsNotSelected"]
    systematics = analysis["systematics"]
    corrections = analysis["corrections"]
    corrections_data = analysis["corrections_data"]
    year = analysis["year"]
    channel = args.channel
    logger.info(f"Get effective events: {run_effective}")
    logger.info(f"EventsNotSelected: {EventsNotSelected}")
    logger.info(f"Corrections for simulation: {corrections}")
    logger.info(f"Systematics for simulation: {systematics}")
    logger.info(f"Corrections for data: {corrections_data}")
    logger.info(f"Year: {year}")
    logger.info(f"Channel: {channel}")
    check_corr_syst(corrections, systematics, logger)

    if args.output == parser.get_default("output"):
        args.output = (
            f'hists_{workflow}_{(samplejson).replace("/","_").rstrip(".json")}.coffea'
        )

    # load dataset
    xrootd_pfx = "root://"
    xrd_pfx_len = len(xrootd_pfx)
    with open(samplejson) as f:
        sample_dict = json.load(f)
    for key in sample_dict.keys():
        sample_dict[key] = sample_dict[key][: args.limit]
    if args.executor == "dask/casa":
        for key in sample_dict.keys():
            sample_dict[key] = [
                path.replace(
                    path[xrd_pfx_len : xrd_pfx_len + path[xrd_pfx_len:].find("/")],
                    "xcache",
                )
                for path in sample_dict[key]
            ]

    # For debugging
    if args.only is not None:
        if args.only in sample_dict.keys():  # is dataset
            sample_dict = dict([(args.only, sample_dict[args.only])])
        if "*" in args.only:  # wildcard for datasets
            _new_dict = {}
            logger.debug("Will only process the following datasets:")
            for k, v in sample_dict.items():
                if k.lstrip("/").startswith(args.only.rstrip("*")):
                    logger.debug("    ", k)
                    _new_dict[k] = v
            sample_dict = _new_dict
        else:  # is file
            for key in sample_dict.keys():
                if args.only in sample_dict[key]:
                    sample_dict = dict([(key, [args.only])])

    # Scan if files can be opened
    if args.validate:
        logger.info("Performing sanity check on files")
        start = time.time()
        from p_tqdm import p_map

        all_invalid = []
        for sample in sample_dict.keys():
            _rmap = p_map(
                validate,
                sample_dict[sample],
                num_cpus=args.workers,
                desc=f"Validating {sample[:20]}...",
            )
            _results = list(_rmap)
            counts = np.sum([r for r in _results if r is not None])
            all_invalid += [r for r in _results if type(r) is str]
            logger.debug("Events:", np.sum(counts))
        logger.debug("Bad files:")
        for fi in all_invalid:
            logger.debug(f"  {fi}")
        end = time.time()
        logger.debug("TIME:", time.strftime("%H:%M:%S", time.gmtime(end - start)))
        if input("Remove bad files? (y/n)") == "y":
            logger.debug("Removing:")
            for fi in all_invalid:
                logger.debug(f"Removing: {fi}")
                os.system(f"rm {fi}")
        sys.exit(0)

    # load workflow
    if workflow in workflows:
        wf_taggers = None
        if taggers is not None:
            for tagger in taggers:
                if tagger not in all_taggers.keys():
                    raise NotImplementedError
            wf_taggers = [all_taggers[tagger]() for tagger in taggers]
        with resources.open_text(
            "higgs_dna.metaconditions", all_metaconditions[metaconditions]
        ) as f:
            processor_instance = workflows[workflow](
                json.load(f),
                run_effective,
                systematics,
                corrections,
                corrections_data,
                args.use_trigger,
                args.dump,
                wf_taggers,
                year,
                channel,
            )  # additional args can go here to configure a processor
    else:
        raise NotImplementedError

    if args.executor not in ["futures", "iterative", "dask/lpc", "dask/casa"]:
        """
        dask/parsl needs to export x509 to read over xrootd
        dask/lpc uses custom jobqueue provider that handles x509
        """
        if args.voms is not None:
            _x509_path = args.voms
        else:
            _x509_path = get_proxy()

        env_extra = [
            "export XRD_RUNFORKHANDLER=1",
            f"export X509_USER_PROXY={_x509_path}",
            f"export PYTHONPATH=$PYTHONPATH:{os.getcwd()}",
        ]
        try:
            env_extra.append(f'export X509_CERT_DIR={os.environ["X509_CERT_DIR"]}')
        except KeyError:
            pass
        condor_extra = [
            f'source {os.environ["HOME"]}/.bashrc',
        ]

    # Execute
    if args.executor in ["futures", "iterative"]:
        if args.executor == "iterative":
            executor = processor.IterativeExecutor()
        else:
            executor = processor.FuturesExecutor(workers=args.workers)
        run_executor(args, executor, sample_dict, processor_instance, EventsNotSelected)
    elif args.executor == "imperial_condor":
        from higgs_dna.submission.lxIC_condor import LXICcondorSubmitter

        for cmd_str in env_extra:
            g_var = cmd_str.split()[-1]
            var, var_value = g_var.split("=")
            os.environ[var] = var_value
        args_string = " ".join(sys.argv[1:])

        imperial_submitter = LXICcondorSubmitter(
            args.dump,
            analysis,
            year,
            channel,
            args.json_analysis_file,
            sample_dict,
            args_string,
            treename="EventsNotSelected" if EventsNotSelected else "Events",
            queue=args.queue,
        )
        output = imperial_submitter.submit()
