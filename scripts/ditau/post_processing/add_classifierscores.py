import os
import pyarrow as pa
import pyarrow.parquet as pq
import argparse
import subprocess
import sys
from higgs_dna.utils.logger_utils import setup_logger
from alive_progress import alive_bar
from xgboost import XGBClassifier
import xgboost as xgb
import pandas as pd
import yaml
import numpy as np

logger = setup_logger(level="INFO")


def get_args():
    parser = argparse.ArgumentParser(description="Apply signal background classifier model to the merged.parquet file")
    parser.add_argument('--model_channel', required=False, help='Desired classifier channel')
    parser.add_argument('--parent_dir', type=str, help="The parent directory containing year subdirectories.")
    parser.add_argument('--years', type=str, help="Comma-separated list of years.", required=False)
    parser.add_argument('--channels', type=str, help="Comma-separated list of channels.", required=False)
    parser.add_argument('--use_condor', action='store_true', help="Flag to submit jobs to HTCondor.")
    parser.add_argument('--subdir', type=str, help="Specific subdirectory to process (used by HTCondor jobs).")
    return parser.parse_args()


def create_condor_submission_file(script_path, subdir, log_dir, model_channel='tt'):
    submission_file_content = f"""
executable = {sys.executable}
arguments = {script_path} --subdir {subdir} --model_channel {model_channel}
output = {log_dir}/add_class_{model_channel}.out
error = {log_dir}/add_class_{model_channel}.err
log = {log_dir}/add_class_{model_channel}.log
request_memory = 16000
request_cpus = 2
+MaxRuntime = 9000
queue
"""
    submission_file_path = os.path.join(log_dir, f'condor_submit_{model_channel}.sub')
    with open(submission_file_path, 'w') as f:
        f.write(submission_file_content)

    return submission_file_path


def submit_to_condor(submission_file_path):
    subprocess.run(['condor_submit', submission_file_path])


def apply_classifier_model(subdirectory, parity, model_channel='tt'):
    # Load XGB model
    model = XGBClassifier() # use 2022 model for now
    model.load_model(f'scripts/ditau/config/SignalClassifier/model_{model_channel}/model_{model_channel}_{parity}.json')
    # Load model features
    features = yaml.safe_load(open(f'scripts/ditau/config/SignalClassifier/model_{model_channel}/train_cfg.yaml'))['Features']['train']
    logger.info(f"Model successfully loaded - {parity} events, {model_channel} channel")
    # Find merged parquet file to evaluate
    merged_file = os.path.join(subdirectory, "merged.parquet")
    if not os.path.exists(merged_file):
        logger.warning(f"Merged file does not exist!")
        return
    # Load the relevant (even or odd) events from the file
    df = pd.read_parquet(merged_file, engine='pyarrow')
    if parity == "EVEN":
        df = df[df['event'] % 2 == 0]
    elif parity == "ODD":
        df = df[df['event'] % 2 == 1]
    # Apply the model to the dataframe
    y_pred = model.predict_proba(df[features])
    logger.info(f"Model successfully applied")
    # add classifier scores to the dataframe
    df[f'BDT_pred_score'] = np.max(y_pred, axis=1) # score of highest class
    df[f'BDT_pred_class'] = y_pred.argmax(axis=1) # class with highest score
    # individual scores
    df[f'BDT_raw_score_tau'] = y_pred[:, 0]
    df[f'BDT_raw_score_higgs'] = y_pred[:, 1]
    df[f'BDT_raw_score_fake'] = y_pred[:, 2]
    # write out the file with classifier scores
    table = pa.Table.from_pandas(df)
    pq.write_table(table, os.path.join(subdirectory, f"merged_{parity}_classifier.parquet"))
    logger.info(f"Classifier scores added to temporary file: {subdirectory}/merged_{parity}_classifier.parquet")


def process_directory(directory, model_channel='tt'):
    # Add the model to even events
    apply_classifier_model(directory, "EVEN", model_channel)
    # Add the model to odd events
    apply_classifier_model(directory, "ODD", model_channel)
    # Load df with even model applied
    even_file = os.path.join(directory, "merged_EVEN_classifier.parquet")
    if not os.path.exists(even_file):
        logger.warning(f"EVEN file does not exist!")
        return
    else:
        even_df = pd.read_parquet(even_file, engine='pyarrow')
    # Load df with odd model applied
    odd_file = os.path.join(directory, "merged_ODD_classifier.parquet")
    if not os.path.exists(odd_file):
        logger.warning(f"ODD file does not exist!")
        return
    else:
        odd_df = pd.read_parquet(odd_file, engine='pyarrow')
    # Merge the two dataframes
    df = pd.concat([even_df, odd_df])
    # Write out the merged file
    table = pa.Table.from_pandas(df)
    pq.write_table(table, os.path.join(directory, f"merged.parquet"))
    logger.info(f"Classifier scores added to merged file: {directory}/merged.parquet")
    # Delete the temporary files (safety for future merging)
    try:
        os.remove(even_file)
        os.remove(odd_file)
        logger.info(f"Temporary files deleted: {even_file}, {odd_file}")
    except Exception as e:
        logger.warning(f"Failed to delete temporary files: {e}")


def process_subdirectories(process_dir, script_path, use_condor, model_channel='tt'):
    subdirs = [os.path.join(process_dir, d) for d in os.listdir(process_dir) if os.path.isdir(os.path.join(process_dir, d))]
    logger.info(f"Processing subdirectories in {process_dir}")
    for subdir in subdirs:
        log_dir = os.path.join(subdir, 'classifier_logs')
        os.makedirs(log_dir, exist_ok=True)
        if use_condor:
            submission_file_path = create_condor_submission_file(script_path, subdir, log_dir, model_channel)
            submit_to_condor(submission_file_path)
            logger.info(f"Submitted condor job for {subdir}")
        elif not use_condor:
            process_directory(subdir, model_channel)


def process_directories(parent_dir, years, channels, use_condor):
    for year in years:
        for channel in channels:
            channel_dir = os.path.join(parent_dir, year, channel)
            if not os.path.exists(channel_dir):
                logger.info(f"Directory does not exist: {channel_dir}")
                continue
            if channel == "tt":
                # Apply classifier model (only tt trained for now...)
                processes = [d for d in os.listdir(channel_dir) if os.path.isdir(os.path.join(channel_dir, d))]
                for process in processes:
                    logger.info(f"Applying classifier model for process: {process}")
                    process_dir = os.path.join(channel_dir, process)
                    process_subdirectories(process_dir, __file__, use_condor, channel)
            else:
                logger.info(f"Skipping channel {channel} as no corresponding model is available.")
                continue


def main():
    args = get_args()
    if args.subdir:
        # This mode is for HTCondor execution
        process_directory(args.subdir, args.model_channel)
    else:
        # Interactive or HTCondor submission mode
        parent_dir = args.parent_dir
        years = [year.strip() for year in args.years.split(',')]
        channels = [channel.strip() for channel in args.channels.split(',')]
        process_directories(parent_dir, years, channels, args.use_condor)


if __name__ == "__main__":
    main()
