import os
import pyarrow as pa
import pyarrow.parquet as pq
import argparse
import subprocess
import sys
from higgs_dna.utils.logger_utils import setup_logger
from alive_progress import alive_bar

logger = setup_logger(level="INFO")


def get_args():
    parser = argparse.ArgumentParser(description="Merge Parquet files in specified directories and remove the original files after merging.")
    parser.add_argument('--parent_dir', type=str, help="The parent directory containing year subdirectories.")
    parser.add_argument('--years', type=str, help="Comma-separated list of years.", required=False)
    parser.add_argument('--channels', type=str, help="Comma-separated list of channels.", required=False)
    parser.add_argument('--use_condor', action='store_true', help="Flag to submit jobs to HTCondor.")
    parser.add_argument('--check_logs', action='store_true', help="Flag to check logs for successful completion.")
    parser.add_argument('--subdir', type=str, help="Specific subdirectory to process (used by HTCondor jobs).")
    return parser.parse_args()


def create_condor_submission_file(script_path, subdir, log_dir):
    submission_file_content = f"""
executable = {sys.executable}
arguments = {script_path} --subdir {subdir}
output = {log_dir}/merge.out
error = {log_dir}/merge.err
log = {log_dir}/merge.log
request_memory = 8000
+MaxRuntime = 3000
queue
"""
    submission_file_path = os.path.join(log_dir, 'condor_submit.sub')
    with open(submission_file_path, 'w') as f:
        f.write(submission_file_content)

    return submission_file_path


def submit_to_condor(submission_file_path):
    subprocess.run(['condor_submit', submission_file_path])


def merge_parquet_files(directory):
    parquet_files = [f for f in os.listdir(directory) if f.endswith('.parquet') and f != 'merged.parquet'
                     and f != 'fastmtt.parquet' and f != 'svfit.parquet']
    logger.info(f"Found {len(parquet_files)} parquet files in directory {directory}")
    if len(parquet_files) == 0:
        logger.info(f"No parquet files found in {directory}")
        print("------------------------------------------------------------------")
        return None
    if len(parquet_files) == 1:
        parquet_file = parquet_files[0]
        logger.info(f"Copying single file {parquet_file} to merged.parquet")
        os.system(f"cp {os.path.join(directory, parquet_file)} {os.path.join(directory, 'merged.parquet')}")
        print("------------------------------------------------------------------")
    else:
        logger.info(f"Merging {len(parquet_files)} files into merged.parquet")
        schema = None
        writer = None
        with alive_bar(len(parquet_files)) as bar:
            for f in parquet_files:
                #logger.info(f"Processing file: {f}")
                dataset = pq.ParquetDataset(os.path.join(directory, f))
                table = dataset.read()
                if schema is None:
                    schema = table.schema
                    writer = pq.ParquetWriter(os.path.join(directory, 'merged.parquet'), schema)
                else:
                    aligned_columns = [table[col.name] for col in schema]
                    table = pa.table(aligned_columns, schema=schema)
                writer.write_table(table)
                bar()
        print("------------------------------------------------------------------")

        if writer is not None:
            writer.close()


def process_directory(directory):
    merge_parquet_files(directory)


def process_subdirectories(process_dir, script_path, use_condor):
    subdirs = [os.path.join(process_dir, d) for d in os.listdir(process_dir) if os.path.isdir(os.path.join(process_dir, d))]
    print("------------------------------------------------------------------")
    logger.info(f"Processing subdirectories in {process_dir}")
    for subdir in subdirs:
        log_dir = os.path.join(subdir, 'merge_logs')
        os.makedirs(log_dir, exist_ok=True)
        if use_condor:
            submission_file_path = create_condor_submission_file(script_path, subdir, log_dir)
            submit_to_condor(submission_file_path)
            logger.info(f"Submitted condor job for {subdir}")
            print("------------------------------------------------------------------")
        elif not use_condor:
            process_directory(subdir)


def process_directories(parent_dir, years, channels, use_condor, check_logs=False):
    count_total_merges = 0
    count_failed_merges = 0
    for year in years:
        for channel in channels:
            channel_dir = os.path.join(parent_dir, year, channel)
            if not os.path.exists(channel_dir):
                logger.info(f"Directory does not exist: {channel_dir}")
                continue

            processes = [d for d in os.listdir(channel_dir) if os.path.isdir(os.path.join(channel_dir, d))]
            if not check_logs:
                for process in processes:
                    logger.info(f"Merging parquet files for process: {process}")
                    process_dir = os.path.join(channel_dir, process)
                    process_subdirectories(process_dir, __file__, use_condor)
            elif check_logs:
                for process in processes:
                    process_dir = os.path.join(channel_dir, process)
                    subdirs = [os.path.join(process_dir, d) for d in os.listdir(process_dir) if os.path.isdir(os.path.join(process_dir, d))]
                    for subdir in subdirs:
                        count_total_merges += 1
                        parquet_files = [f for f in os.listdir(subdir) if f.endswith('.parquet')]
                        if len(parquet_files) == 0:
                            logger.info(f"No parquet files found in {subdir}")
                            count_failed_merges += 1
                        elif len(parquet_files) != 1 and 'merged.parquet' not in parquet_files:
                            logger.info(f"Directory {subdir} has failed to merge.")
                            count_failed_merges += 1
    if check_logs:
        logger.info(f"Total Directories to Merge: {count_total_merges}")
        logger.info(f"Directories Failed to Merge: {count_failed_merges}")


def main():
    args = get_args()
    if args.subdir:
        # This mode is for HTCondor execution
        process_directory(args.subdir)
    else:
        # Interactive or HTCondor submission mode
        parent_dir = args.parent_dir
        years = [year.strip() for year in args.years.split(',')]
        channels = [channel.strip() for channel in args.channels.split(',')]
        process_directories(parent_dir, years, channels, args.use_condor, args.check_logs)


if __name__ == "__main__":
    main()
