import os
import argparse
import subprocess
import sys
import pyarrow.dataset as ds
import awkward as ak
import uproot3 as uproot
import numpy as np
from higgs_dna.utils.logger_utils import setup_logger

logger = setup_logger(level="INFO")


def get_args():
    parser = argparse.ArgumentParser(description="Merge Parquet files in specified directories and remove the original files after merging.")
    parser.add_argument('--parent_dir', type=str, help="The parent directory containing year subdirectories.")
    parser.add_argument('--years', type=str, help="Comma-separated list of years.", required=False)
    parser.add_argument('--channels', type=str, help="Comma-separated list of channels.", required=False)
    parser.add_argument('--use_condor', action='store_true', help="Flag to submit jobs to HTCondor.")
    parser.add_argument('--check_logs', action='store_true', help="Flag to check logs for failed jobs.")
    parser.add_argument('--subdir', type=str, help="Subdirectory to process.", required=False)
    return parser.parse_args()


def create_condor_submission_file(script_path, subdir, log_dir, channel):
    if channel in ['ee', 'mm'] and 'DYto2L' in subdir:
        request_cpus = 5
    else:
        request_cpus = 1
    request_memory = request_cpus*8000
    submission_file_content = f"""
executable = {sys.executable}
arguments = {script_path} --subdir {subdir}
output = {log_dir}/create_root.out
error = {log_dir}/create_root.err
log = {log_dir}/create_root.log
request_cpus = {request_cpus}
request_memory = {request_memory}
+MaxRuntime = 9600
queue
"""
    submission_file_path = os.path.join(log_dir, 'condor_submit.sub')
    with open(submission_file_path, 'w') as f:
        f.write(submission_file_content)

    return submission_file_path


def submit_to_condor(submission_file_path):
    subprocess.run(['condor_submit', submission_file_path])


def create_root_file(directory):
    parquet_files = [f for f in os.listdir(directory) if f == 'merged.parquet']
    if len(parquet_files) != 1:
        logger.info(f"Invalid number of parquet files found in {directory}: {len(parquet_files)}")
        return None
    else:
        parquet_file = parquet_files[0]
        parquet_path = os.path.join(directory, parquet_file)

        root_file_path = os.path.join(directory, 'merged.root')

        dataset = ds.dataset(parquet_path, format="parquet")
        chunk_size = 100000
        scanner = dataset.scanner(batch_size=chunk_size)

        total_nan_count = 0
        total_inf_count = 0
        total_nan_count_after = 0

        with uproot.recreate(root_file_path) as root_file:
            for chunk_number,batch in enumerate(scanner.to_batches()):
                events = ak.from_arrow(batch)
                data_dict = {}
                type_dict = {}
                for field in events.fields:
                    data_array = events[field]
                    nan_mask = ak.is_none(data_array)
                    data_array = ak.fill_none(data_array, -9999.0)

                    total_nan_count += ak.sum(nan_mask)
                    np_data_array = ak.to_numpy(data_array)

                    total_inf_count += np.isinf(np_data_array).sum()
                    np_data_array = np.nan_to_num(np_data_array, nan=-9999.0, posinf=-9999.0, neginf=-9999.0)

                    data_dict[field] = np_data_array

                    dtype = np_data_array.dtype
                    if np.issubdtype(dtype, np.unsignedinteger):
                        dtype = 'int64'
                    type_dict[field] = dtype

                    # count nan and inf values
                    total_nan_count_after += np.isnan(np_data_array).sum()

                # Write or extend ROOT file
                if chunk_number == 0:
                    root_file["ntuple"] = uproot.newtree(type_dict)
                root_file["ntuple"].extend(data_dict)

        logger.info(f"Created root file in {directory}")
        logger.info(f"Total NaN count: {total_nan_count}")
        logger.info(f"Total Inf count: {total_inf_count}")
        logger.info(f"Total NaN count after: {total_nan_count_after}")
        print("-------------------------------------------------")


def process_directory(directory):
    logger.info(f"Processing directory: {directory}")
    create_root_file(directory)


def process_subdirectories(process_dir, script_path, use_condor, channel):
    subdirs = [os.path.join(process_dir, d) for d in os.listdir(process_dir) if os.path.isdir(os.path.join(process_dir, d))]
    print("------------------------------------------------------------------")
    logger.info(f"Processing subdirectories in {process_dir}")
    for subdir in subdirs:
        log_dir = os.path.join(subdir, 'create_root_logs')
        os.makedirs(log_dir, exist_ok=True)
        if use_condor:
            submission_file_path = create_condor_submission_file(script_path, subdir, log_dir, channel)
            submit_to_condor(submission_file_path)
            logger.info(f"Submitted condor job for {subdir}")
            print("------------------------------------------------------------------")
        elif not use_condor:
            process_directory(subdir)


def process_directories(parent_dir, years, channels, use_condor, check_logs=False):
    count_total = 0
    count_fails = 0
    for year in years:
        for channel in channels:
            channel_dir = os.path.join(parent_dir, year, channel)
            if not os.path.exists(channel_dir):
                logger.info(f"Directory does not exist: {channel_dir}")
                continue
            processes = [d for d in os.listdir(channel_dir) if os.path.isdir(os.path.join(channel_dir, d))]
            for process in processes:
                process_dir = os.path.join(channel_dir, process)
                if not check_logs:
                    process_subdirectories(process_dir, __file__, use_condor, channel)
                else:
                    subdirs = [os.path.join(process_dir, d) for d in os.listdir(process_dir) if os.path.isdir(os.path.join(process_dir, d))]
                    for subdir in subdirs:
                        count_total += 1
                        root_file = [f for f in os.listdir(subdir) if f.endswith('.root')][0]
                        root_file = os.path.join(subdir, root_file)
                        with uproot.open(root_file) as file:
                            if 'ntuple' not in file:
                                count_fails += 1
                                logger.info(f"Failed to create root file in {subdir}")
                            #else:
                                #logger.info(f"Root file in {subdir} looks ok with {file['ntuple'].numentries} entries.")
    if check_logs:
        print("")
        logger.info(f"Total number of directories processed: {count_total}")
        logger.info(f"Total number of failed directories: {count_fails}")


def main():
    args = get_args()

    if args.subdir:
        # This mode is for HTCondor execution
        process_directory(args.subdir)
    else:
        # Interactive or HTCondor submission mode
        parent_dir = args.parent_dir
        years = [year.strip() for year in args.years.split(',')]
        channels = [channel.strip() for channel in args.channels.split(',')]
        process_directories(parent_dir, years, channels, args.use_condor, args.check_logs)


if __name__ == "__main__":
    main()
