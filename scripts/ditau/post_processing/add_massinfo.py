import os
import pandas as pd
import argparse
import pyarrow as pa
import pyarrow.parquet as pq
from higgs_dna.utils.logger_utils import setup_logger

logger = setup_logger(level="INFO")


def get_args():
    parser = argparse.ArgumentParser(description="Add svfit/fastmtt mass information to the merged.parquet file")
    parser.add_argument('--mass_directory', type=str, help="The directory containing the mass files.")
    parser.add_argument('--parent_dir', type=str, help="The parent directory containing year subdirectories.")
    parser.add_argument('--years', type=str, help="Comma-separated list of years.", required=False)
    parser.add_argument('--channels', type=str, help="Comma-separated list of channels.", required=False)
    parser.add_argument('--add_svfit', action='store_true', help="Add svfit mass to merged file.")
    parser.add_argument('--add_fastmtt', action='store_true', help="Add fastmtt mass to merged file.")
    return parser.parse_args()


def merge_massinfo(mass_directory, directory, add_svfit, add_fastmtt):
    merged_file = os.path.join(directory, "merged.parquet")
    if not os.path.exists(merged_file):
        logger.info("Merged file does not exist!")
        return
    # load merged file with pandas
    df = pd.read_parquet(merged_file, engine='pyarrow')
    # look for svfit or fastmtt mass files
    svfit_file = os.path.join(mass_directory, "svfit.parquet")
    fastmtt_file = os.path.join(mass_directory, "fastmtt.parquet")
    # add mass info if it exists
    if add_svfit:
        if os.path.exists(svfit_file):
            if 'svfitMass' in df.columns:
                logger.info("SVFit mass already in merged file")
                return
            else:
                dfsvfit = pd.read_parquet(svfit_file)
                df = pd.merge(df, dfsvfit[['run', 'lumi', 'event', 'svfitMass', 'svfitMass_err']], on=['run', 'lumi', 'event'], how='left')
                table = pa.Table.from_pandas(df)
                logger.info("SVFit mass added")
        else:
            logger.warning("SVFit mass file does not exist!")
    if add_fastmtt:
        if os.path.exists(fastmtt_file):
            if 'FastMTT_Mass' in df.columns:
                logger.info("FastMTT mass already in merged file")
                return
            else:
                dffastmtt = pd.read_parquet(fastmtt_file)
                df = pd.merge(df, dffastmtt[['run', 'lumi', 'event', 'FastMTT_Mass']], on=['run', 'lumi', 'event'], how='left')
                table = pa.Table.from_pandas(df)
                logger.info("FastMTT mass added")
        else:
            logger.warning("FastMTT mass file does not exist!")

    if add_svfit and not add_fastmtt:
        if not os.path.exists(svfit_file):
            logger.warning("SVFit mass file does not exist!")
            return
    if add_fastmtt and not add_svfit:
        if not os.path.exists(fastmtt_file):
            logger.warning("FastMTT mass file does not exist!")
            return
    if add_fastmtt and add_svfit:
        if not os.path.exists(svfit_file) and not os.path.exists(fastmtt_file):
            logger.warning("No mass file exists for this sample!")
            return

    pq.write_table(table, merged_file)


def process_directories(mass_dir, parent_dir, years, channels, add_svfit=True, add_fastmtt=True):
    for year in years:
        for channel in channels:
            channel_dir = os.path.join(parent_dir, year, channel)
            if not os.path.exists(channel_dir):
                logger.warning(f"Directory does not exist: {channel_dir}")
                continue
            processes = [d for d in os.listdir(channel_dir) if os.path.isdir(os.path.join(channel_dir, d))]
            for process in processes:
                logger.info(f"Attempting to add mass for process: {process}")
                process_dir = os.path.join(channel_dir, process)
                mass_directory = os.path.join(mass_dir, year, channel, process)
                merge_massinfo(os.path.join(mass_directory, 'nominal'), os.path.join(process_dir, 'nominal'), add_svfit, add_fastmtt)
                print("------------------------------------------------------")


def main():
    args = get_args()
    if args.mass_directory is None:
        args.mass_directory = args.parent_dir

    parent_dir = args.parent_dir
    mass_dir = args.mass_directory
    years = [year.strip() for year in args.years.split(',')]
    channels = [channel.strip() for channel in args.channels.split(',')]
    add_svfit = args.add_svfit
    add_fastmtt = args.add_fastmtt
    process_directories(mass_dir, parent_dir, years, channels, add_svfit=add_svfit, add_fastmtt=add_fastmtt)


if __name__ == "__main__":
    main()
