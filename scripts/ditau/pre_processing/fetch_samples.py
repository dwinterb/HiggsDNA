import yaml
import json
import subprocess
import os

# Load the YAML file
with open('scripts/ditau/pre_processing/samples.yaml', 'r') as file:
    data = yaml.safe_load(file)


source_dict = {
    "Run3_2022": "davs://gfe02.grid.hep.ph.ic.ac.uk:2880/pnfs/hep.ph.ic.ac.uk/data/cms/store/user/ksavva/HLepRare/skim_2024_v2",
    "Run3_2022EE": "davs://gfe02.grid.hep.ph.ic.ac.uk:2880/pnfs/hep.ph.ic.ac.uk/data/cms/store/user/irandreo/HLepRare_skim_2024_v2",
    "Run3_2023": "davs://gfe02.grid.hep.ph.ic.ac.uk:2880/pnfs/hep.ph.ic.ac.uk/data/cms/store/user/ksavva/HLepRare/skim_2024_v2",
    "Run3_2023BPix": "davs://gfe02.grid.hep.ph.ic.ac.uk:2880/pnfs/hep.ph.ic.ac.uk/data/cms/store/user/irandreo/HLepRare/skim_2024_v2",
}

source_dict_NLO = "davs://gfe02.grid.hep.ph.ic.ac.uk:2880/pnfs/hep.ph.ic.ac.uk/data/cms/store/user/ksavva/HLepRare/skim_2024_v2/NLO"
source_dict_NLO_pTbinned = "davs://gfe02.grid.hep.ph.ic.ac.uk:2880/pnfs/hep.ph.ic.ac.uk/data/cms/store/user/lrussell/HLepRare/skim_2024_v2"

# Function to get file paths using gfal-ls
def get_file_paths(year,sample_name,source_path):
    command = f"gfal-ls {source_path}/{year}/{sample_name}"
    root_path = source_path.replace("davs://", "root://")
    root_path = root_path.replace(":2880", ":1094")
    try:
        result = subprocess.run(command, shell=True, capture_output=True, text=True, check=True)
        files = result.stdout.splitlines()
        paths = [f"{root_path}/{year}/{sample_name}/{file}" for file in files if file.endswith(".root")]
        return paths
    except subprocess.CalledProcessError as e:
        print(f"An error occurred while processing {sample_name}: {e}")
        return []


# Iterate over the YAML data to populate the JSON structure
for year, categories in data.items():
    source_path = source_dict[year]
    print(f"Processing year {year}...")
    json_structure = {}
    for category, samples in categories.items():
        for sample in samples:
            print(f"Processing {sample}...")
            if sample in ["DYto2L_M_10to50_amcatnloFXFX", "DYto2L_M_50_amcatnloFXFX", "DYto2L_M_50_amcatnloFXFX_ext1", "DYto2L_M_50_0J_amcatnloFXFX", "DYto2L_M_50_1J_amcatnloFXFX", "DYto2L_M_50_2J_amcatnloFXFX"]:
                json_structure[sample] = get_file_paths(year,sample,source_dict_NLO)
            elif sample in ["DYto2L_M_50_PTLL_40to100_1J_amcatnloFXFX", "DYto2L_M_50_PTLL_100to200_1J_amcatnloFXFX", "DYto2L_M_50_PTLL_200to400_1J_amcatnloFXFX", "DYto2L_M_50_PTLL_400to600_1J_amcatnloFXFX", "DYto2L_M_50_PTLL_600_1J_amcatnloFXFX", "DYto2L_M_50_PTLL_40to100_2J_amcatnloFXFX", "DYto2L_M_50_PTLL_100to200_2J_amcatnloFXFX", "DYto2L_M_50_PTLL_200to400_2J_amcatnloFXFX", "DYto2L_M_50_PTLL_400to600_2J_amcatnloFXFX", "DYto2L_M_50_PTLL_600_2J_amcatnloFXFX"
            ]:
                json_structure[sample] = get_file_paths(year, sample, source_dict_NLO_pTbinned)
            else:
                json_structure[sample] = get_file_paths(year,sample,source_path)
    json_output = json.dumps(json_structure, indent=4)
    # Save to a JSON file
    if not os.path.exists(f'scripts/ditau/config/{year}'):
        os.makedirs(f'scripts/ditau/config/{year}')
    with open(f'scripts/ditau/config/{year}/samples.json', 'w') as json_file:
        json_file.write(json_output)
    print(f"JSON file created successfully for {year}.")
